EESchema Schematic File Version 2
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:linear
LIBS:logo
LIBS:maxim
LIBS:mechanical
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:modules
LIBS:motor_drivers
LIBS:motorola
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:zetex
LIBS:Zilog
LIBS:temp-sensor-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "zondag 28 september 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MAX31820 U1
U 1 1 5A727A1D
P 2350 900
F 0 "U1" H 2200 1150 50  0000 C CNN
F 1 "MAX31820" H 2350 650 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 2200 1150 50  0001 C CNN
F 3 "" H 2200 1150 50  0001 C CNN
	1    2350 900 
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 J2
U 1 1 5A727B42
P 2400 1650
F 0 "J2" H 2400 1850 50  0000 C CNN
F 1 "CONN_01X03" V 2500 1650 50  0000 C CNN
F 2 "local:1x04-keyed" H 2400 1650 50  0001 C CNN
F 3 "" H 2400 1650 50  0001 C CNN
	1    2400 1650
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 J1
U 1 1 5A727BD5
P 1450 1650
F 0 "J1" H 1450 1850 50  0000 C CNN
F 1 "CONN_01X03" V 1550 1650 50  0000 C CNN
F 2 "local:3x-drill-holes-70mil" H 1450 1650 50  0001 C CNN
F 3 "" H 1450 1650 50  0001 C CNN
	1    1450 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 1650 2200 1650
Wire Wire Line
	2050 900  1900 900 
Wire Wire Line
	1900 900  1900 1650
Connection ~ 1900 1650
$Comp
L +3V3 #PWR01
U 1 1 5A727CE1
P 2000 750
F 0 "#PWR01" H 2000 600 50  0001 C CNN
F 1 "+3V3" H 2000 890 50  0000 C CNN
F 2 "" H 2000 750 50  0001 C CNN
F 3 "" H 2000 750 50  0001 C CNN
	1    2000 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 750  2000 800 
Wire Wire Line
	2000 800  2050 800 
$Comp
L GND #PWR02
U 1 1 5A727EC5
P 2000 1050
F 0 "#PWR02" H 2000 800 50  0001 C CNN
F 1 "GND" H 2000 900 50  0000 C CNN
F 2 "" H 2000 1050 50  0001 C CNN
F 3 "" H 2000 1050 50  0001 C CNN
	1    2000 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1000 2000 1000
Wire Wire Line
	2000 1000 2000 1050
$Comp
L PWR_FLAG #FLG03
U 1 1 5A727F38
P 600 700
F 0 "#FLG03" H 600 775 50  0001 C CNN
F 1 "PWR_FLAG" H 600 850 50  0000 C CNN
F 2 "" H 600 700 50  0001 C CNN
F 3 "" H 600 700 50  0001 C CNN
	1    600  700 
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 5A727F53
P 900 750
F 0 "#FLG04" H 900 825 50  0001 C CNN
F 1 "PWR_FLAG" H 900 900 50  0000 C CNN
F 2 "" H 900 750 50  0001 C CNN
F 3 "" H 900 750 50  0001 C CNN
	1    900  750 
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR05
U 1 1 5A727F67
P 600 750
F 0 "#PWR05" H 600 500 50  0001 C CNN
F 1 "GND" H 600 600 50  0000 C CNN
F 2 "" H 600 750 50  0001 C CNN
F 3 "" H 600 750 50  0001 C CNN
	1    600  750 
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR06
U 1 1 5A727F7B
P 900 700
F 0 "#PWR06" H 900 550 50  0001 C CNN
F 1 "+3V3" H 900 840 50  0000 C CNN
F 2 "" H 900 700 50  0001 C CNN
F 3 "" H 900 700 50  0001 C CNN
	1    900  700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  700  900  750 
Wire Wire Line
	600  700  600  750 
$Comp
L +3V3 #PWR07
U 1 1 5A728062
P 2150 1500
F 0 "#PWR07" H 2150 1350 50  0001 C CNN
F 1 "+3V3" H 2150 1640 50  0000 C CNN
F 2 "" H 2150 1500 50  0001 C CNN
F 3 "" H 2150 1500 50  0001 C CNN
	1    2150 1500
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR08
U 1 1 5A728076
P 1750 1850
F 0 "#PWR08" H 1750 1700 50  0001 C CNN
F 1 "+3V3" H 1750 1990 50  0000 C CNN
F 2 "" H 1750 1850 50  0001 C CNN
F 3 "" H 1750 1850 50  0001 C CNN
	1    1750 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5A72808A
P 2150 1800
F 0 "#PWR09" H 2150 1550 50  0001 C CNN
F 1 "GND" H 2150 1650 50  0000 C CNN
F 2 "" H 2150 1800 50  0001 C CNN
F 3 "" H 2150 1800 50  0001 C CNN
	1    2150 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5A72809E
P 1750 1450
F 0 "#PWR010" H 1750 1200 50  0001 C CNN
F 1 "GND" H 1750 1300 50  0000 C CNN
F 2 "" H 1750 1450 50  0001 C CNN
F 3 "" H 1750 1450 50  0001 C CNN
	1    1750 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1450 1750 1400
Wire Wire Line
	1750 1400 1650 1400
Wire Wire Line
	1650 1400 1650 1550
Wire Wire Line
	1650 1750 1650 1950
Wire Wire Line
	1650 1950 1750 1950
Wire Wire Line
	1750 1950 1750 1850
Wire Wire Line
	2150 1500 2150 1550
Wire Wire Line
	2150 1550 2200 1550
Wire Wire Line
	2150 1800 2150 1750
Wire Wire Line
	2150 1750 2200 1750
$Comp
L C C1
U 1 1 5A73987A
P 2800 900
F 0 "C1" H 2825 1000 50  0000 L CNN
F 1 "C" H 2825 800 50  0000 L CNN
F 2 "local:C-0306" H 2838 750 50  0001 C CNN
F 3 "" H 2800 900 50  0001 C CNN
	1    2800 900 
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR011
U 1 1 5A73990B
P 2800 700
F 0 "#PWR011" H 2800 550 50  0001 C CNN
F 1 "+3V3" H 2800 840 50  0000 C CNN
F 2 "" H 2800 700 50  0001 C CNN
F 3 "" H 2800 700 50  0001 C CNN
	1    2800 700 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5A739922
P 2800 1100
F 0 "#PWR012" H 2800 850 50  0001 C CNN
F 1 "GND" H 2800 950 50  0000 C CNN
F 2 "" H 2800 1100 50  0001 C CNN
F 3 "" H 2800 1100 50  0001 C CNN
	1    2800 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 1050 2800 1100
Wire Wire Line
	2800 750  2800 700 
$EndSCHEMATC

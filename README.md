# README #

### Flipboard circuit designs ###

* The driver.{sch,brd} files are for a board to drive 12 letters.  They slave to the board below.
* The controller.{sch,brd} files are for the main microcontroller, and line termination.
* The bus needs 100R termination at both ends.  110R across the pair, 1100R to +5, 1100R to GND, works.
* To sense temperatures, hook up a bunch of DS18B20+ TO-92s to pins 1 (Vdd), 2 (GND), and 10 (DQ).
    * Solder some 0.1-inch-header pins into those indentations on the Photon, and use a connector.
* You can also hook up a speaker to pin 6 (tie the other end to ground), to get streaming audio.
    * An amp may be needed.  We don't yet know what max current the Photon can source via its DAC.

### How do I get set up? ###

* Get Eagle
* Set your library path to include the libraries/ directory
* Get the Particle Devices library and unzip its .lbr file, to libraries/particle.lbr
    * https://github.com/spark/hardware-libraries/raw/master/Eagle/Particle-Devices.lbr.zip
* Open driver.sch / driver.brd, or controller.sch / controller.brd

### How do I use this? ###

You will need a copy of the [firmware](https://bitbucket.org/bryankadzban/split-flap-firmware).
See more information on how this whole project works in that repository.

### Who do I talk to? ###

* bkadzban@google.com
* kor@google.com
* (maybe eventually) flipboard-team@google.com

#### This is not an official Google product. ####

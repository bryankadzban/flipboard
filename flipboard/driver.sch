<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="13" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="LOGO" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="motor-driver">
<description>&lt;h3&gt;Toshiba stepper motor driver&lt;/h3&gt;
Contains a TB62212 symbol in an FNG (HTSSOP-48) housing</description>
<packages>
<package name="HTSSOP48DCA">
<description>&lt;b&gt;PowerPAD PLASTIC SMALL-OUTLINE PACKAGE&lt;/b&gt;&lt;p&gt;
DCA (R-PDSO-G**)&lt;br&gt;
Source: http://focus.ti.com/lit/ml/mpds044/mpds044.pdf / slma002.pdf</description>
<wire x1="-6.25" y1="3.05" x2="6.25" y2="3.05" width="0.1016" layer="21"/>
<wire x1="6.25" y1="3.05" x2="6.25" y2="-3.05" width="0.1016" layer="21"/>
<wire x1="6.25" y1="-3.05" x2="-6.25" y2="-3.05" width="0.1016" layer="21"/>
<wire x1="-6.25" y1="-3.05" x2="-6.25" y2="3.05" width="0.1016" layer="21"/>
<circle x="-5.6" y="-2.5" radius="0.2828" width="0.1016" layer="21"/>
<smd name="1" x="-5.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="EXP" x="0" y="0" dx="12.5" dy="5.1" layer="1" stop="no" cream="no"/>
<smd name="2" x="-5.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="3" x="-4.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="4" x="-4.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="5" x="-3.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="6" x="-3.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="7" x="-2.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="8" x="-2.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="9" x="-1.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="10" x="-1.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="11" x="-0.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="12" x="-0.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="13" x="0.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="14" x="0.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="15" x="1.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="16" x="1.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="17" x="2.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="18" x="2.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="19" x="3.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="20" x="3.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="21" x="4.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="22" x="4.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="23" x="5.25" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="24" x="5.75" y="-4.025" dx="0.3" dy="1.65" layer="1" stop="no" cream="no"/>
<smd name="25" x="5.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="26" x="5.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="27" x="4.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="28" x="4.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="29" x="3.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="30" x="3.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="31" x="2.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="32" x="2.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="33" x="1.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="34" x="1.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="35" x="0.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="36" x="0.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="37" x="-0.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="38" x="-0.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="39" x="-1.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="40" x="-1.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="41" x="-2.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="42" x="-2.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="43" x="-3.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="44" x="-3.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="45" x="-4.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="46" x="-4.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="47" x="-5.25" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="48" x="-5.75" y="4.025" dx="0.3" dy="1.65" layer="1" rot="R180" stop="no" cream="no"/>
<text x="-4.61" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.975" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.86" y1="-4.15" x2="-5.63" y2="-3.095" layer="51"/>
<rectangle x1="-5.905" y1="-4.855" x2="-5.595" y2="-3.195" layer="29"/>
<rectangle x1="-5.89" y1="-4.835" x2="-5.61" y2="-3.215" layer="31"/>
<rectangle x1="-5.36" y1="-4.15" x2="-5.13" y2="-3.095" layer="51"/>
<rectangle x1="-5.405" y1="-4.855" x2="-5.095" y2="-3.195" layer="29"/>
<rectangle x1="-5.39" y1="-4.835" x2="-5.11" y2="-3.215" layer="31"/>
<rectangle x1="-4.86" y1="-4.15" x2="-4.63" y2="-3.095" layer="51"/>
<rectangle x1="-4.905" y1="-4.855" x2="-4.595" y2="-3.195" layer="29"/>
<rectangle x1="-4.89" y1="-4.835" x2="-4.61" y2="-3.215" layer="31"/>
<rectangle x1="-4.36" y1="-4.15" x2="-4.13" y2="-3.095" layer="51"/>
<rectangle x1="-4.405" y1="-4.855" x2="-4.095" y2="-3.195" layer="29"/>
<rectangle x1="-4.39" y1="-4.835" x2="-4.11" y2="-3.215" layer="31"/>
<rectangle x1="-3.86" y1="-4.15" x2="-3.63" y2="-3.095" layer="51"/>
<rectangle x1="-3.905" y1="-4.855" x2="-3.595" y2="-3.195" layer="29"/>
<rectangle x1="-3.89" y1="-4.835" x2="-3.61" y2="-3.215" layer="31"/>
<rectangle x1="-3.36" y1="-4.15" x2="-3.13" y2="-3.095" layer="51"/>
<rectangle x1="-3.405" y1="-4.855" x2="-3.095" y2="-3.195" layer="29"/>
<rectangle x1="-3.39" y1="-4.835" x2="-3.11" y2="-3.215" layer="31"/>
<rectangle x1="-2.86" y1="-4.15" x2="-2.63" y2="-3.095" layer="51"/>
<rectangle x1="-2.905" y1="-4.855" x2="-2.595" y2="-3.195" layer="29"/>
<rectangle x1="-2.89" y1="-4.835" x2="-2.61" y2="-3.215" layer="31"/>
<rectangle x1="-2.36" y1="-4.15" x2="-2.13" y2="-3.095" layer="51"/>
<rectangle x1="-2.405" y1="-4.855" x2="-2.095" y2="-3.195" layer="29"/>
<rectangle x1="-2.39" y1="-4.835" x2="-2.11" y2="-3.215" layer="31"/>
<rectangle x1="-1.86" y1="-4.15" x2="-1.63" y2="-3.095" layer="51"/>
<rectangle x1="-1.905" y1="-4.855" x2="-1.595" y2="-3.195" layer="29"/>
<rectangle x1="-1.89" y1="-4.835" x2="-1.61" y2="-3.215" layer="31"/>
<rectangle x1="-1.36" y1="-4.15" x2="-1.13" y2="-3.095" layer="51"/>
<rectangle x1="-1.405" y1="-4.855" x2="-1.095" y2="-3.195" layer="29"/>
<rectangle x1="-1.39" y1="-4.835" x2="-1.11" y2="-3.215" layer="31"/>
<rectangle x1="-0.86" y1="-4.15" x2="-0.63" y2="-3.095" layer="51"/>
<rectangle x1="-0.905" y1="-4.855" x2="-0.595" y2="-3.195" layer="29"/>
<rectangle x1="-0.89" y1="-4.835" x2="-0.61" y2="-3.215" layer="31"/>
<rectangle x1="-0.36" y1="-4.15" x2="-0.13" y2="-3.095" layer="51"/>
<rectangle x1="-0.405" y1="-4.855" x2="-0.095" y2="-3.195" layer="29"/>
<rectangle x1="-0.39" y1="-4.835" x2="-0.11" y2="-3.215" layer="31"/>
<rectangle x1="0.14" y1="-4.15" x2="0.37" y2="-3.095" layer="51"/>
<rectangle x1="0.095" y1="-4.855" x2="0.405" y2="-3.195" layer="29"/>
<rectangle x1="0.11" y1="-4.835" x2="0.39" y2="-3.215" layer="31"/>
<rectangle x1="0.64" y1="-4.15" x2="0.87" y2="-3.095" layer="51"/>
<rectangle x1="0.595" y1="-4.855" x2="0.905" y2="-3.195" layer="29"/>
<rectangle x1="0.61" y1="-4.835" x2="0.89" y2="-3.215" layer="31"/>
<rectangle x1="1.14" y1="-4.15" x2="1.37" y2="-3.095" layer="51"/>
<rectangle x1="1.095" y1="-4.855" x2="1.405" y2="-3.195" layer="29"/>
<rectangle x1="1.11" y1="-4.835" x2="1.39" y2="-3.215" layer="31"/>
<rectangle x1="1.64" y1="-4.15" x2="1.87" y2="-3.095" layer="51"/>
<rectangle x1="1.595" y1="-4.855" x2="1.905" y2="-3.195" layer="29"/>
<rectangle x1="1.61" y1="-4.835" x2="1.89" y2="-3.215" layer="31"/>
<rectangle x1="2.14" y1="-4.15" x2="2.37" y2="-3.095" layer="51"/>
<rectangle x1="2.095" y1="-4.855" x2="2.405" y2="-3.195" layer="29"/>
<rectangle x1="2.11" y1="-4.835" x2="2.39" y2="-3.215" layer="31"/>
<rectangle x1="2.64" y1="-4.15" x2="2.87" y2="-3.095" layer="51"/>
<rectangle x1="2.595" y1="-4.855" x2="2.905" y2="-3.195" layer="29"/>
<rectangle x1="2.61" y1="-4.835" x2="2.89" y2="-3.215" layer="31"/>
<rectangle x1="3.14" y1="-4.15" x2="3.37" y2="-3.095" layer="51"/>
<rectangle x1="3.095" y1="-4.855" x2="3.405" y2="-3.195" layer="29"/>
<rectangle x1="3.11" y1="-4.835" x2="3.39" y2="-3.215" layer="31"/>
<rectangle x1="3.64" y1="-4.15" x2="3.87" y2="-3.095" layer="51"/>
<rectangle x1="3.595" y1="-4.855" x2="3.905" y2="-3.195" layer="29"/>
<rectangle x1="3.61" y1="-4.835" x2="3.89" y2="-3.215" layer="31"/>
<rectangle x1="4.14" y1="-4.15" x2="4.37" y2="-3.095" layer="51"/>
<rectangle x1="4.095" y1="-4.855" x2="4.405" y2="-3.195" layer="29"/>
<rectangle x1="4.11" y1="-4.835" x2="4.39" y2="-3.215" layer="31"/>
<rectangle x1="4.64" y1="-4.15" x2="4.87" y2="-3.095" layer="51"/>
<rectangle x1="4.595" y1="-4.855" x2="4.905" y2="-3.195" layer="29"/>
<rectangle x1="4.61" y1="-4.835" x2="4.89" y2="-3.215" layer="31"/>
<rectangle x1="5.14" y1="-4.15" x2="5.37" y2="-3.095" layer="51"/>
<rectangle x1="5.095" y1="-4.855" x2="5.405" y2="-3.195" layer="29"/>
<rectangle x1="5.11" y1="-4.835" x2="5.39" y2="-3.215" layer="31"/>
<rectangle x1="5.64" y1="-4.15" x2="5.87" y2="-3.095" layer="51"/>
<rectangle x1="5.595" y1="-4.855" x2="5.905" y2="-3.195" layer="29"/>
<rectangle x1="5.61" y1="-4.835" x2="5.89" y2="-3.215" layer="31"/>
<rectangle x1="5.63" y1="3.095" x2="5.86" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-5.905" y1="3.195" x2="-5.595" y2="4.855" layer="29"/>
<rectangle x1="-5.89" y1="3.215" x2="-5.61" y2="4.835" layer="31"/>
<rectangle x1="5.13" y1="3.095" x2="5.36" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-5.405" y1="3.195" x2="-5.095" y2="4.855" layer="29"/>
<rectangle x1="-5.39" y1="3.215" x2="-5.11" y2="4.835" layer="31"/>
<rectangle x1="4.63" y1="3.095" x2="4.86" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-4.905" y1="3.195" x2="-4.595" y2="4.855" layer="29"/>
<rectangle x1="-4.89" y1="3.215" x2="-4.61" y2="4.835" layer="31"/>
<rectangle x1="4.13" y1="3.095" x2="4.36" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-4.405" y1="3.195" x2="-4.095" y2="4.855" layer="29"/>
<rectangle x1="-4.39" y1="3.215" x2="-4.11" y2="4.835" layer="31"/>
<rectangle x1="3.63" y1="3.095" x2="3.86" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-3.905" y1="3.195" x2="-3.595" y2="4.855" layer="29"/>
<rectangle x1="-3.89" y1="3.215" x2="-3.61" y2="4.835" layer="31"/>
<rectangle x1="3.13" y1="3.095" x2="3.36" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-3.405" y1="3.195" x2="-3.095" y2="4.855" layer="29"/>
<rectangle x1="-3.39" y1="3.215" x2="-3.11" y2="4.835" layer="31"/>
<rectangle x1="2.63" y1="3.095" x2="2.86" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-2.905" y1="3.195" x2="-2.595" y2="4.855" layer="29"/>
<rectangle x1="-2.89" y1="3.215" x2="-2.61" y2="4.835" layer="31"/>
<rectangle x1="2.13" y1="3.095" x2="2.36" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-2.405" y1="3.195" x2="-2.095" y2="4.855" layer="29"/>
<rectangle x1="-2.39" y1="3.215" x2="-2.11" y2="4.835" layer="31"/>
<rectangle x1="1.63" y1="3.095" x2="1.86" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-1.905" y1="3.195" x2="-1.595" y2="4.855" layer="29"/>
<rectangle x1="-1.89" y1="3.215" x2="-1.61" y2="4.835" layer="31"/>
<rectangle x1="1.13" y1="3.095" x2="1.36" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-1.405" y1="3.195" x2="-1.095" y2="4.855" layer="29"/>
<rectangle x1="-1.39" y1="3.215" x2="-1.11" y2="4.835" layer="31"/>
<rectangle x1="0.63" y1="3.095" x2="0.86" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-0.905" y1="3.195" x2="-0.595" y2="4.855" layer="29"/>
<rectangle x1="-0.89" y1="3.215" x2="-0.61" y2="4.835" layer="31"/>
<rectangle x1="0.13" y1="3.095" x2="0.36" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="-0.405" y1="3.195" x2="-0.095" y2="4.855" layer="29"/>
<rectangle x1="-0.39" y1="3.215" x2="-0.11" y2="4.835" layer="31"/>
<rectangle x1="-0.37" y1="3.095" x2="-0.14" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="0.095" y1="3.195" x2="0.405" y2="4.855" layer="29"/>
<rectangle x1="0.11" y1="3.215" x2="0.39" y2="4.835" layer="31"/>
<rectangle x1="-0.87" y1="3.095" x2="-0.64" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="0.595" y1="3.195" x2="0.905" y2="4.855" layer="29"/>
<rectangle x1="0.61" y1="3.215" x2="0.89" y2="4.835" layer="31"/>
<rectangle x1="-1.37" y1="3.095" x2="-1.14" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="1.095" y1="3.195" x2="1.405" y2="4.855" layer="29"/>
<rectangle x1="1.11" y1="3.215" x2="1.39" y2="4.835" layer="31"/>
<rectangle x1="-1.87" y1="3.095" x2="-1.64" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="1.595" y1="3.195" x2="1.905" y2="4.855" layer="29"/>
<rectangle x1="1.61" y1="3.215" x2="1.89" y2="4.835" layer="31"/>
<rectangle x1="-2.37" y1="3.095" x2="-2.14" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="2.095" y1="3.195" x2="2.405" y2="4.855" layer="29"/>
<rectangle x1="2.11" y1="3.215" x2="2.39" y2="4.835" layer="31"/>
<rectangle x1="-2.87" y1="3.095" x2="-2.64" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="2.595" y1="3.195" x2="2.905" y2="4.855" layer="29"/>
<rectangle x1="2.61" y1="3.215" x2="2.89" y2="4.835" layer="31"/>
<rectangle x1="-3.37" y1="3.095" x2="-3.14" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="3.095" y1="3.195" x2="3.405" y2="4.855" layer="29"/>
<rectangle x1="3.11" y1="3.215" x2="3.39" y2="4.835" layer="31"/>
<rectangle x1="-3.87" y1="3.095" x2="-3.64" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="3.595" y1="3.195" x2="3.905" y2="4.855" layer="29"/>
<rectangle x1="3.61" y1="3.215" x2="3.89" y2="4.835" layer="31"/>
<rectangle x1="-4.37" y1="3.095" x2="-4.14" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="4.095" y1="3.195" x2="4.405" y2="4.855" layer="29"/>
<rectangle x1="4.11" y1="3.215" x2="4.39" y2="4.835" layer="31"/>
<rectangle x1="-4.87" y1="3.095" x2="-4.64" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="4.595" y1="3.195" x2="4.905" y2="4.855" layer="29"/>
<rectangle x1="4.61" y1="3.215" x2="4.89" y2="4.835" layer="31"/>
<rectangle x1="-5.37" y1="3.095" x2="-5.14" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="5.095" y1="3.195" x2="5.405" y2="4.855" layer="29"/>
<rectangle x1="5.11" y1="3.215" x2="5.39" y2="4.835" layer="31"/>
<rectangle x1="-5.87" y1="3.095" x2="-5.64" y2="4.15" layer="51" rot="R180"/>
<rectangle x1="5.595" y1="3.195" x2="5.905" y2="4.855" layer="29"/>
<rectangle x1="5.61" y1="3.215" x2="5.89" y2="4.835" layer="31"/>
<rectangle x1="-4" y1="-2.2" x2="-3" y2="-1" layer="31"/>
<pad name="GND@1" x="-4.6" y="1.4" drill="0.3302"/>
<pad name="GND@2" x="4.6" y="1.4" drill="0.3302"/>
<pad name="GND@3" x="-4.6" y="-1.4" drill="0.3302"/>
<pad name="GND@4" x="4.6" y="-1.4" drill="0.3302"/>
<rectangle x1="-5.4" y1="-2.2" x2="5.4" y2="2.2" layer="29"/>
<rectangle x1="-4" y1="1" x2="-3" y2="2.2" layer="31"/>
<rectangle x1="-5.4" y1="-0.6" x2="-3" y2="0.6" layer="31"/>
<rectangle x1="3" y1="-2.2" x2="4" y2="-1" layer="31"/>
<rectangle x1="-2.6" y1="-2.2" x2="-0.2" y2="-1" layer="31"/>
<rectangle x1="0.2" y1="-2.2" x2="2.6" y2="-1" layer="31"/>
<rectangle x1="-2.6" y1="1" x2="-0.2" y2="2.2" layer="31"/>
<rectangle x1="0.2" y1="1" x2="2.6" y2="2.2" layer="31"/>
<rectangle x1="3" y1="1" x2="4" y2="2.2" layer="31"/>
<rectangle x1="-2.6" y1="-0.6" x2="-0.2" y2="0.6" layer="31"/>
<rectangle x1="0.2" y1="-0.6" x2="2.6" y2="0.6" layer="31"/>
<rectangle x1="3" y1="-0.6" x2="5.4" y2="0.6" layer="31"/>
</package>
</packages>
<symbols>
<symbol name="TB62212">
<description>4-coil motor driver</description>
<wire x1="-12.7" y1="35.56" x2="12.7" y2="35.56" width="0.254" layer="94"/>
<wire x1="12.7" y1="35.56" x2="12.7" y2="-38.1" width="0.254" layer="94"/>
<wire x1="12.7" y1="-38.1" x2="-12.7" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-38.1" x2="-12.7" y2="35.56" width="0.254" layer="94"/>
<pin name="RS_A" x="-15.24" y="22.86" length="short" direction="in"/>
<pin name="RS_B" x="-15.24" y="10.16" length="short" direction="in"/>
<pin name="RS_C" x="-15.24" y="-2.54" length="short" direction="in"/>
<pin name="RS_D" x="-15.24" y="-15.24" length="short" direction="in"/>
<pin name="IN1_A" x="-15.24" y="20.32" length="short" direction="in"/>
<pin name="IN2_A" x="-15.24" y="17.78" length="short" direction="in"/>
<pin name="IN1_B" x="-15.24" y="7.62" length="short" direction="in"/>
<pin name="IN2_B" x="-15.24" y="5.08" length="short" direction="in"/>
<pin name="IN1_C" x="-15.24" y="-5.08" length="short" direction="in"/>
<pin name="IN2_C" x="-15.24" y="-7.62" length="short" direction="in"/>
<pin name="IN1_D" x="-15.24" y="-17.78" length="short" direction="in"/>
<pin name="IN2_D" x="-15.24" y="-20.32" length="short" direction="in"/>
<pin name="+5V" x="15.24" y="33.02" length="short" direction="out" rot="R180"/>
<pin name="OUT_A-" x="15.24" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="OUT_A+" x="15.24" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="OUT_B-" x="15.24" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="OUT_B+" x="15.24" y="0" length="short" direction="out" rot="R180"/>
<pin name="OUT_C-" x="15.24" y="-5.08" length="short" direction="out" rot="R180"/>
<pin name="OUT_C+" x="15.24" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="OUT_D-" x="15.24" y="-12.7" length="short" direction="out" rot="R180"/>
<pin name="OUT_D+" x="15.24" y="-15.24" length="short" direction="out" rot="R180"/>
<pin name="VREF_A" x="-15.24" y="15.24" length="short"/>
<pin name="VREF_B" x="-15.24" y="2.54" length="short"/>
<pin name="VREF_C" x="-15.24" y="-10.16" length="short"/>
<pin name="VREF_D" x="-15.24" y="-22.86" length="short"/>
<pin name="D_TBLANK_AB" x="-15.24" y="-27.94" length="short" direction="in"/>
<pin name="D_TBLANK_CD" x="-15.24" y="-30.48" length="short" direction="in"/>
<pin name="MODE2" x="-15.24" y="27.94" length="short" direction="in"/>
<pin name="MODE1" x="-15.24" y="30.48" length="short" direction="in"/>
<pin name="MODE0" x="-15.24" y="33.02" length="short" direction="in"/>
<pin name="OSCM" x="-15.24" y="-35.56" length="short"/>
<pin name="VM" x="0" y="38.1" length="short" direction="pwr" rot="R270"/>
</symbol>
<symbol name="NC">
<description>NC pin</description>
<pin name="NC" x="-2.54" y="0" visible="pad" length="short" direction="nc"/>
</symbol>
<symbol name="VM">
<description>Motor drive voltage</description>
<pin name="+12V" x="-2.54" y="0" visible="pad" length="short" direction="pwr"/>
</symbol>
<symbol name="GND">
<description>GND pin</description>
<pin name="GND" x="-2.54" y="0" visible="pad" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TB62212FNG">
<description>HTSSOP48 package</description>
<gates>
<gate name="G$1" symbol="TB62212" x="0" y="0"/>
<gate name="G$2" symbol="NC" x="33.02" y="35.56" addlevel="request"/>
<gate name="G$3" symbol="NC" x="33.02" y="33.02" addlevel="request"/>
<gate name="G$4" symbol="NC" x="33.02" y="30.48" addlevel="request"/>
<gate name="G$5" symbol="NC" x="33.02" y="27.94" addlevel="request"/>
<gate name="G$6" symbol="NC" x="33.02" y="25.4" addlevel="request"/>
<gate name="G$7" symbol="NC" x="33.02" y="22.86" addlevel="request"/>
<gate name="G$8" symbol="NC" x="33.02" y="20.32" addlevel="request"/>
<gate name="G$9" symbol="NC" x="33.02" y="17.78" addlevel="request"/>
<gate name="G$10" symbol="NC" x="33.02" y="15.24" addlevel="request"/>
<gate name="G$12" symbol="GND" x="-33.02" y="22.86" addlevel="request"/>
<gate name="G$13" symbol="GND" x="-33.02" y="20.32" addlevel="request"/>
<gate name="G$14" symbol="GND" x="-33.02" y="17.78" addlevel="request"/>
<gate name="G$15" symbol="GND" x="-33.02" y="15.24" addlevel="request"/>
<gate name="GND$A" symbol="GND" x="-33.02" y="5.08" addlevel="request"/>
<gate name="GND$B" symbol="GND" x="-33.02" y="2.54" addlevel="request"/>
<gate name="GND$C" symbol="GND" x="-33.02" y="0" addlevel="request"/>
<gate name="GND$D" symbol="GND" x="-33.02" y="-2.54" addlevel="request"/>
<gate name="GND$E" symbol="GND" x="-33.02" y="-5.08" addlevel="request"/>
<gate name="GND$H" symbol="GND" x="-33.02" y="-12.7" addlevel="request"/>
<gate name="GND$F" symbol="GND" x="-33.02" y="-7.62" addlevel="request"/>
<gate name="GND$G" symbol="GND" x="-33.02" y="-10.16" addlevel="request"/>
</gates>
<devices>
<device name="" package="HTSSOP48DCA">
<connects>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="D_TBLANK_AB" pad="27"/>
<connect gate="G$1" pin="D_TBLANK_CD" pad="28"/>
<connect gate="G$1" pin="IN1_A" pad="5"/>
<connect gate="G$1" pin="IN1_B" pad="6"/>
<connect gate="G$1" pin="IN1_C" pad="7"/>
<connect gate="G$1" pin="IN1_D" pad="8"/>
<connect gate="G$1" pin="IN2_A" pad="44"/>
<connect gate="G$1" pin="IN2_B" pad="43"/>
<connect gate="G$1" pin="IN2_C" pad="42"/>
<connect gate="G$1" pin="IN2_D" pad="41"/>
<connect gate="G$1" pin="MODE0" pad="32"/>
<connect gate="G$1" pin="MODE1" pad="31"/>
<connect gate="G$1" pin="MODE2" pad="30"/>
<connect gate="G$1" pin="OSCM" pad="48"/>
<connect gate="G$1" pin="OUT_A+" pad="11"/>
<connect gate="G$1" pin="OUT_A-" pad="9"/>
<connect gate="G$1" pin="OUT_B+" pad="13"/>
<connect gate="G$1" pin="OUT_B-" pad="15"/>
<connect gate="G$1" pin="OUT_C+" pad="36"/>
<connect gate="G$1" pin="OUT_C-" pad="34"/>
<connect gate="G$1" pin="OUT_D+" pad="38"/>
<connect gate="G$1" pin="OUT_D-" pad="40"/>
<connect gate="G$1" pin="RS_A" pad="3"/>
<connect gate="G$1" pin="RS_B" pad="23"/>
<connect gate="G$1" pin="RS_C" pad="26"/>
<connect gate="G$1" pin="RS_D" pad="46"/>
<connect gate="G$1" pin="VM" pad="25"/>
<connect gate="G$1" pin="VREF_A" pad="17"/>
<connect gate="G$1" pin="VREF_B" pad="18"/>
<connect gate="G$1" pin="VREF_C" pad="19"/>
<connect gate="G$1" pin="VREF_D" pad="21"/>
<connect gate="G$10" pin="NC" pad="47"/>
<connect gate="G$12" pin="GND" pad="12"/>
<connect gate="G$13" pin="GND" pad="24"/>
<connect gate="G$14" pin="GND" pad="37"/>
<connect gate="G$15" pin="GND" pad="EXP"/>
<connect gate="G$2" pin="NC" pad="2"/>
<connect gate="G$3" pin="NC" pad="4"/>
<connect gate="G$4" pin="NC" pad="16"/>
<connect gate="G$5" pin="NC" pad="20"/>
<connect gate="G$6" pin="NC" pad="22"/>
<connect gate="G$7" pin="NC" pad="29"/>
<connect gate="G$8" pin="NC" pad="33"/>
<connect gate="G$9" pin="NC" pad="45"/>
<connect gate="GND$A" pin="GND" pad="10"/>
<connect gate="GND$B" pin="GND" pad="14"/>
<connect gate="GND$C" pin="GND" pad="35"/>
<connect gate="GND$D" pin="GND" pad="39"/>
<connect gate="GND$E" pin="GND" pad="GND@1"/>
<connect gate="GND$F" pin="GND" pad="GND@2"/>
<connect gate="GND$G" pin="GND" pad="GND@3"/>
<connect gate="GND$H" pin="GND" pad="GND@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+12V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+12V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="C01005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-US" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="1v">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+1V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+1V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+1V" prefix="+1V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+1V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<description>&lt;b&gt;GIC connector&lt;/b&gt;&lt;p&gt;
 &lt;author&gt;Created by bryan@kadzban.net&lt;/author&gt;</description>
<packages>
<package name="GULL-WING">
<description>3x DIP switches</description>
<smd name="IN2" x="0" y="4.3" dx="2.44" dy="1.13" layer="1" rot="R90"/>
<smd name="IN3" x="2.54" y="4.3" dx="2.44" dy="1.13" layer="1" rot="R90"/>
<smd name="IN1" x="-2.54" y="4.3" dx="2.44" dy="1.13" layer="1" rot="R90"/>
<smd name="OUT1" x="-2.54" y="-4.3" dx="2.44" dy="1.13" layer="1" rot="R90"/>
<smd name="OUT2" x="0" y="-4.3" dx="2.44" dy="1.13" layer="1" rot="R90"/>
<smd name="OUT3" x="2.54" y="-4.3" dx="2.44" dy="1.13" layer="1" rot="R90"/>
<wire x1="-0.75" y1="3.35" x2="-0.75" y2="4" width="0.127" layer="51"/>
<wire x1="-0.75" y1="4" x2="-0.275" y2="4" width="0.127" layer="51"/>
<wire x1="-0.275" y1="4" x2="-0.275" y2="4.955" width="0.127" layer="51"/>
<wire x1="-0.275" y1="4.955" x2="0.275" y2="4.955" width="0.127" layer="51"/>
<wire x1="0.275" y1="4.955" x2="0.275" y2="4" width="0.127" layer="51"/>
<wire x1="0.275" y1="4" x2="0.75" y2="4" width="0.127" layer="51"/>
<wire x1="0.75" y1="4" x2="0.75" y2="3.35" width="0.127" layer="51"/>
<wire x1="-3.29" y1="3.35" x2="-3.29" y2="4" width="0.127" layer="51"/>
<wire x1="-1.79" y1="4" x2="-1.79" y2="3.35" width="0.127" layer="51"/>
<wire x1="-3.29" y1="4" x2="-2.815" y2="4" width="0.127" layer="51"/>
<wire x1="-2.265" y1="4" x2="-1.79" y2="4" width="0.127" layer="51"/>
<wire x1="-2.815" y1="4" x2="-2.815" y2="4.955" width="0.127" layer="51"/>
<wire x1="-2.265" y1="4.955" x2="-2.265" y2="4" width="0.127" layer="51"/>
<wire x1="-2.815" y1="4.955" x2="-2.265" y2="4.955" width="0.127" layer="51"/>
<wire x1="-4.545" y1="3.35" x2="-4.545" y2="-3.35" width="0.127" layer="21"/>
<wire x1="4.545" y1="-3.35" x2="4.545" y2="3.35" width="0.127" layer="21"/>
<wire x1="4.545" y1="3.35" x2="3.29" y2="3.35" width="0.127" layer="21"/>
<wire x1="1.79" y1="3.35" x2="0.75" y2="3.35" width="0.127" layer="21"/>
<wire x1="-0.75" y1="3.35" x2="-1.79" y2="3.35" width="0.127" layer="21"/>
<wire x1="-3.29" y1="3.35" x2="-4.545" y2="3.35" width="0.127" layer="21"/>
<wire x1="1.79" y1="3.35" x2="1.79" y2="4" width="0.127" layer="51"/>
<wire x1="3.29" y1="4" x2="3.29" y2="3.35" width="0.127" layer="51"/>
<wire x1="1.79" y1="4" x2="2.265" y2="4" width="0.127" layer="51"/>
<wire x1="2.815" y1="4" x2="3.29" y2="4" width="0.127" layer="51"/>
<wire x1="2.265" y1="4" x2="2.265" y2="4.955" width="0.127" layer="51"/>
<wire x1="2.815" y1="4.955" x2="2.815" y2="4" width="0.127" layer="51"/>
<wire x1="2.265" y1="4.955" x2="2.815" y2="4.955" width="0.127" layer="51"/>
<wire x1="-3.29" y1="-4" x2="-3.29" y2="-3.35" width="0.127" layer="51"/>
<wire x1="-1.79" y1="-3.35" x2="-1.79" y2="-4" width="0.127" layer="51"/>
<wire x1="-2.265" y1="-4" x2="-1.79" y2="-4" width="0.127" layer="51"/>
<wire x1="-3.29" y1="-4" x2="-2.815" y2="-4" width="0.127" layer="51"/>
<wire x1="-2.815" y1="-4.955" x2="-2.815" y2="-4" width="0.127" layer="51"/>
<wire x1="-2.265" y1="-4" x2="-2.265" y2="-4.955" width="0.127" layer="51"/>
<wire x1="-2.815" y1="-4.955" x2="-2.265" y2="-4.955" width="0.127" layer="51"/>
<wire x1="-0.275" y1="-4.955" x2="0.275" y2="-4.955" width="0.127" layer="51"/>
<wire x1="-0.275" y1="-4.955" x2="-0.275" y2="-4" width="0.127" layer="51"/>
<wire x1="0.275" y1="-4" x2="0.275" y2="-4.955" width="0.127" layer="51"/>
<wire x1="-0.75" y1="-4" x2="-0.275" y2="-4" width="0.127" layer="51"/>
<wire x1="0.275" y1="-4" x2="0.75" y2="-4" width="0.127" layer="51"/>
<wire x1="-0.75" y1="-4" x2="-0.75" y2="-3.35" width="0.127" layer="51"/>
<wire x1="0.75" y1="-3.35" x2="0.75" y2="-4" width="0.127" layer="51"/>
<wire x1="1.79" y1="-4" x2="1.79" y2="-3.35" width="0.127" layer="51"/>
<wire x1="3.29" y1="-3.35" x2="3.29" y2="-4" width="0.127" layer="51"/>
<wire x1="1.79" y1="-4" x2="2.265" y2="-4" width="0.127" layer="51"/>
<wire x1="2.815" y1="-4" x2="3.29" y2="-4" width="0.127" layer="51"/>
<wire x1="2.265" y1="-4.955" x2="2.265" y2="-4" width="0.127" layer="51"/>
<wire x1="2.815" y1="-4" x2="2.815" y2="-4.955" width="0.127" layer="51"/>
<wire x1="2.265" y1="-4.955" x2="2.815" y2="-4.955" width="0.127" layer="51"/>
<wire x1="-4.6" y1="3.4" x2="4.6" y2="3.4" width="0.127" layer="39"/>
<wire x1="4.6" y1="3.4" x2="4.6" y2="-3.4" width="0.127" layer="39"/>
<wire x1="4.6" y1="-3.4" x2="-4.6" y2="-3.4" width="0.127" layer="39"/>
<wire x1="-4.6" y1="-3.4" x2="-4.6" y2="3.4" width="0.127" layer="39"/>
<wire x1="-3.29" y1="-3.35" x2="-4.545" y2="-3.35" width="0.127" layer="21"/>
<wire x1="-0.75" y1="-3.35" x2="-1.79" y2="-3.35" width="0.127" layer="21"/>
<wire x1="1.79" y1="-3.35" x2="0.75" y2="-3.35" width="0.127" layer="21"/>
<wire x1="4.545" y1="-3.35" x2="3.29" y2="-3.35" width="0.127" layer="21"/>
</package>
<package name="RJ45-V-SHIELD*2">
<description>2x RJ-45 connectors, vertical orientation, shielded</description>
<wire x1="-9.05" y1="-14.15" x2="9.05" y2="-14.15" width="0.127" layer="51"/>
<wire x1="9.05" y1="-14.15" x2="9.05" y2="-11.35" width="0.127" layer="51"/>
<wire x1="9.05" y1="-11.35" x2="9.05" y2="-9.2" width="0.127" layer="51"/>
<wire x1="9.05" y1="-9.2" x2="9.05" y2="-6.7" width="0.127" layer="51"/>
<wire x1="9.05" y1="-6.7" x2="9.05" y2="2.1" width="0.127" layer="21"/>
<wire x1="9.05" y1="2.1" x2="9.05" y2="2.9" width="0.127" layer="51"/>
<wire x1="9.05" y1="2.9" x2="9.05" y2="6.8" width="0.127" layer="21"/>
<wire x1="9.05" y1="6.8" x2="9.05" y2="9.35" width="0.127" layer="51"/>
<wire x1="9.05" y1="9.35" x2="9.05" y2="14.15" width="0.127" layer="21"/>
<wire x1="9.05" y1="14.15" x2="-9.05" y2="14.15" width="0.127" layer="21"/>
<wire x1="-9.05" y1="14.15" x2="-9.05" y2="13.4" width="0.127" layer="21"/>
<hole x="-5.715" y="-3.35" drill="3.2"/>
<hole x="5.715" y="-3.35" drill="3.2"/>
<pad name="S1" x="-8.515" y="-7.92" drill="1.6"/>
<pad name="S2" x="-7.755" y="5.8" drill="1.6"/>
<pad name="S3" x="-8.515" y="12.15" drill="1.6"/>
<pad name="S4" x="8.515" y="-7.92" drill="1.6"/>
<pad name="S5" x="7.755" y="2.5" drill="1.6"/>
<pad name="S6" x="8.515" y="8.08" drill="1.6"/>
<pad name="1" x="-4.445" y="3" drill="0.9"/>
<pad name="3" x="-1.905" y="3" drill="0.9"/>
<pad name="5" x="0.635" y="3" drill="0.9"/>
<pad name="7" x="3.175" y="3" drill="0.9"/>
<pad name="2" x="-3.175" y="5.54" drill="0.9"/>
<pad name="4" x="-0.635" y="5.54" drill="0.9"/>
<pad name="6" x="1.905" y="5.54" drill="0.9"/>
<pad name="8" x="4.445" y="5.54" drill="0.9"/>
<pad name="16" x="-4.445" y="9.6" drill="0.9"/>
<pad name="14" x="-1.905" y="9.6" drill="0.9"/>
<pad name="12" x="0.635" y="9.6" drill="0.9"/>
<pad name="10" x="3.175" y="9.6" drill="0.9"/>
<pad name="15" x="-3.175" y="12.14" drill="0.9"/>
<pad name="13" x="-0.635" y="12.14" drill="0.9"/>
<pad name="11" x="1.905" y="12.14" drill="0.9"/>
<pad name="9" x="4.445" y="12.14" drill="0.9"/>
<wire x1="-9.05" y1="13.4" x2="-9.05" y2="10.9" width="0.127" layer="51"/>
<wire x1="-9.05" y1="10.9" x2="-9.05" y2="6.2" width="0.127" layer="21"/>
<wire x1="-9.05" y1="6.2" x2="-9.05" y2="5.4" width="0.127" layer="51"/>
<wire x1="-9.05" y1="5.4" x2="-9.05" y2="-6.7" width="0.127" layer="21"/>
<wire x1="-9.05" y1="-6.7" x2="-9.05" y2="-9.2" width="0.127" layer="51"/>
<wire x1="-9.05" y1="-9.2" x2="-9.05" y2="-11.35" width="0.127" layer="51"/>
<wire x1="-9.05" y1="-11.35" x2="-9.05" y2="-14.15" width="0.127" layer="51"/>
<wire x1="-9.05" y1="-11.35" x2="9.05" y2="-11.35" width="0.127" layer="51"/>
</package>
<package name="SWITCH">
<description>SPST pushbutton switch</description>
<wire x1="3" y1="-3" x2="3" y2="-2.7" width="0.127" layer="51"/>
<wire x1="3" y1="-2.7" x2="3" y2="2.7" width="0.127" layer="21"/>
<wire x1="3" y1="2.7" x2="3" y2="3" width="0.127" layer="51"/>
<wire x1="3" y1="3" x2="1.35" y2="3" width="0.127" layer="51"/>
<wire x1="1.35" y1="3" x2="-1.35" y2="3" width="0.127" layer="21"/>
<wire x1="-1.35" y1="3" x2="-3" y2="3" width="0.127" layer="51"/>
<wire x1="-3" y1="3" x2="-3" y2="2.7" width="0.127" layer="51"/>
<wire x1="-3" y1="2.7" x2="-3" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-3" y1="-2.7" x2="-3" y2="-3" width="0.127" layer="51"/>
<wire x1="-3" y1="-3" x2="-1.35" y2="-3" width="0.127" layer="51"/>
<pad name="1" x="-2.25" y="3.25" drill="1" rot="R90"/>
<pad name="3" x="2.25" y="3.25" drill="1" rot="R90"/>
<text x="3.175" y="6.35" size="1.778" layer="25" rot="R180">&gt;NAME</text>
<circle x="0" y="0" radius="1.755" width="0.127" layer="21"/>
<pad name="2" x="-2.25" y="-3.25" drill="1" rot="R90"/>
<pad name="4" x="2.25" y="-3.25" drill="1" rot="R90"/>
<wire x1="-1.35" y1="-3" x2="1.35" y2="-3" width="0.127" layer="21"/>
<wire x1="1.35" y1="-3" x2="3" y2="-3" width="0.127" layer="51"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.4" dx="0.8" dy="1.7" layer="1"/>
<smd name="2" x="0.95" y="-1.4" dx="0.8" dy="1.7" layer="1"/>
<smd name="1" x="-0.95" y="-1.4" dx="0.8" dy="1.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="C0306">
<description>&lt;b&gt;Ceramic Chip Capacitor 0306&lt;/b&gt;&lt;p&gt;
Metric Code Size 0816</description>
<wire x1="-0.4064" y1="0.8001" x2="0.4064" y2="0.8001" width="0.1016" layer="51"/>
<wire x1="0.4064" y1="-0.8001" x2="-0.4064" y2="-0.8001" width="0.1016" layer="51"/>
<smd name="1" x="-0.675" y="0" dx="1.05" dy="1.6" layer="1"/>
<smd name="2" x="0.675" y="0" dx="1.05" dy="1.6" layer="1"/>
<text x="-1.4" y="1.05" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.4" y="-2.05" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4572" y1="-0.8001" x2="-0.2794" y2="0.8001" layer="51"/>
<rectangle x1="0.2794" y1="-0.8001" x2="0.4572" y2="0.8001" layer="51"/>
<wire x1="-0.4" y1="-0.8" x2="0.4" y2="-0.8" width="0.127" layer="39"/>
<wire x1="0.4" y1="-0.8" x2="0.4" y2="0.8" width="0.127" layer="39"/>
<wire x1="0.4" y1="0.8" x2="-0.4" y2="0.8" width="0.127" layer="39"/>
<wire x1="-0.4" y1="0.8" x2="-0.4" y2="-0.8" width="0.127" layer="39"/>
</package>
<package name="SHERLOCK-6">
<description>Molex Sherlock family 6-pin board connector</description>
<pad name="1" x="5" y="0" drill="0.8"/>
<pad name="2" x="3" y="0" drill="0.8"/>
<pad name="3" x="1" y="0" drill="0.8"/>
<pad name="4" x="-1" y="0" drill="0.8"/>
<pad name="5" x="-3" y="0" drill="0.8"/>
<pad name="6" x="-5" y="0" drill="0.8"/>
<wire x1="-7.15" y1="2.95" x2="7.15" y2="2.95" width="0.127" layer="21"/>
<wire x1="-7.15" y1="2.95" x2="-7.15" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-7.15" y1="-2.55" x2="-5.35" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-5.35" y1="-2.55" x2="-5.35" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-5.35" y1="-1.8" x2="5.35" y2="-1.8" width="0.127" layer="21"/>
<wire x1="5.35" y1="-1.8" x2="5.35" y2="-2.55" width="0.127" layer="21"/>
<wire x1="5.35" y1="-2.55" x2="7.15" y2="-2.55" width="0.127" layer="21"/>
<wire x1="7.15" y1="2.95" x2="7.15" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-7.15" y1="2.95" x2="7.15" y2="2.95" width="0.127" layer="39"/>
<wire x1="7.15" y1="2.95" x2="7.15" y2="-4.15" width="0.127" layer="39"/>
<wire x1="7.15" y1="-4.15" x2="-7.15" y2="-4.15" width="0.127" layer="39"/>
<wire x1="-7.15" y1="-4.15" x2="-7.15" y2="2.95" width="0.127" layer="39"/>
<wire x1="6" y1="-1.5" x2="6.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="6.6" y1="-1.5" x2="6.3" y2="-2" width="0.127" layer="21"/>
<wire x1="6.3" y1="-2" x2="6" y2="-1.5" width="0.127" layer="21"/>
</package>
<package name="A0R-JMP">
<description>&lt;b&gt;0R Jumper&lt;/b&gt;&lt;p&gt;
chip 0603&lt;p&gt;
http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_04.pdf</description>
<wire x1="0.4" y1="1.6" x2="0.4" y2="0.4" width="0.1" layer="51"/>
<wire x1="-0.4" y1="1.6" x2="-0.4" y2="0.4" width="0.1" layer="51"/>
<smd name="1" x="0" y="2" dx="1.2" dy="0.9" layer="1" rot="R270"/>
<smd name="2" x="0" y="0" dx="1.2" dy="0.9" layer="1" rot="R270"/>
<smd name="3" x="0" y="-2" dx="1.2" dy="0.9" layer="1" rot="R270"/>
<text x="-0.762" y="2.716" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-3.986" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.2" x2="0.4" y2="0.45" layer="51"/>
<rectangle x1="-0.4" y1="1.55" x2="0.4" y2="1.8" layer="51"/>
</package>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.8104" x2="1.4224" y2="-0.8104" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="-0.8104" x2="-1.4224" y2="-0.8104" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.8104" x2="-1.4224" y2="0.8104" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.8104" x2="1.4224" y2="0.8104" width="0.1524" layer="51"/>
<wire x1="-0.5224" y1="0.8104" x2="0.5224" y2="0.8104" width="0.1524" layer="21"/>
<wire x1="-0.4276" y1="-0.8104" x2="-0.5224" y2="-0.8104" width="0.1524" layer="21"/>
<wire x1="0.5224" y1="-0.8104" x2="0.4276" y2="-0.8104" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.4001" dx="0.55" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.4001" dx="0.55" dy="1.4" layer="1"/>
<smd name="3" x="0.95" y="-1.4001" dx="0.55" dy="1.4" layer="1"/>
<smd name="4" x="0.95" y="1.4001" dx="0.55" dy="1.4" layer="1"/>
<smd name="5" x="-0.95" y="1.4001" dx="0.55" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<wire x1="-1.425" y1="0.825" x2="-1.4224" y2="0.8104" width="0.127" layer="39"/>
<wire x1="-1.4224" y1="0.8104" x2="-1.4224" y2="-0.8104" width="0.127" layer="39"/>
<wire x1="-1.4224" y1="-0.8104" x2="1.4224" y2="-0.8104" width="0.127" layer="39"/>
<wire x1="1.4224" y1="-0.8104" x2="1.4224" y2="0.8104" width="0.127" layer="39"/>
<wire x1="1.4224" y1="0.8104" x2="-1.4224" y2="0.8104" width="0.127" layer="39"/>
<wire x1="1.2" y1="0.85" x2="1.2" y2="1.5" width="0.127" layer="39"/>
<wire x1="1.2" y1="1.5" x2="0.7" y2="1.5" width="0.127" layer="39"/>
<wire x1="0.7" y1="1.5" x2="0.7" y2="0.85" width="0.127" layer="39"/>
<wire x1="0.7" y1="0.85" x2="1.2" y2="0.85" width="0.127" layer="39"/>
<wire x1="-0.7" y1="0.85" x2="-0.7" y2="1.5" width="0.127" layer="39"/>
<wire x1="-1.2" y1="0.85" x2="-1.2" y2="1.5" width="0.127" layer="39"/>
<wire x1="-0.7" y1="1.5" x2="-1.2" y2="1.5" width="0.127" layer="39"/>
<wire x1="-1.2" y1="0.85" x2="-0.7" y2="0.85" width="0.127" layer="39"/>
<wire x1="-1.2" y1="-0.85" x2="-0.7" y2="-0.85" width="0.127" layer="39"/>
<wire x1="-0.7" y1="-1.5" x2="-1.2" y2="-1.5" width="0.127" layer="39"/>
<wire x1="-0.7" y1="-1.5" x2="-0.7" y2="-0.85" width="0.127" layer="39"/>
<wire x1="-1.2" y1="-1.5" x2="-1.2" y2="-0.85" width="0.127" layer="39"/>
<wire x1="-0.25" y1="-1.5" x2="-0.25" y2="-0.85" width="0.127" layer="39"/>
<wire x1="0.25" y1="-1.5" x2="0.25" y2="-0.85" width="0.127" layer="39"/>
<wire x1="0.25" y1="-1.5" x2="-0.25" y2="-1.5" width="0.127" layer="39"/>
<wire x1="0.25" y1="-0.85" x2="-0.25" y2="-0.85" width="0.127" layer="39"/>
<wire x1="0.7" y1="-1.5" x2="0.7" y2="-0.85" width="0.127" layer="39"/>
<wire x1="1.2" y1="-1.5" x2="1.2" y2="-0.85" width="0.127" layer="39"/>
<wire x1="1.2" y1="-1.5" x2="0.7" y2="-1.5" width="0.127" layer="39"/>
<wire x1="1.2" y1="-0.85" x2="0.7" y2="-0.85" width="0.127" layer="39"/>
</package>
<package name="L3X3MM">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;</description>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.0508" layer="39"/>
<wire x1="1.55" y1="-1.55" x2="-1.55" y2="-1.55" width="0.0508" layer="39"/>
<wire x1="-1.55" y1="-1.55" x2="-1.55" y2="1.55" width="0.0508" layer="39"/>
<wire x1="-1.464" y1="1.34" x2="1.464" y2="1.34" width="0.1016" layer="51"/>
<wire x1="-1.464" y1="-1.353" x2="1.464" y2="-1.353" width="0.1016" layer="51"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.55" width="0.0508" layer="39"/>
<smd name="1" x="-1.5" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.67" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.94" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.543" y1="-1.35" x2="-0.7" y2="1.35" layer="51"/>
<rectangle x1="0.7" y1="-1.35" x2="1.5382" y2="1.35" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C0508">
<description>&lt;b&gt;Ceramic Chip Capacitor 0306&lt;/b&gt;&lt;p&gt;
Metric Code Size 0816</description>
<wire x1="-0.5742" y1="1" x2="0.5742" y2="1" width="0.1016" layer="51"/>
<wire x1="0.5742" y1="-1" x2="-0.5742" y2="-1" width="0.1016" layer="51"/>
<smd name="1" x="-0.8875" y="0" dx="1.075" dy="1.8" layer="1"/>
<smd name="2" x="0.8875" y="0" dx="1.075" dy="1.8" layer="1"/>
<text x="-1.4" y="1.05" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.4" y="-2.05" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.625" y1="-1" x2="-0.4472" y2="1" layer="51"/>
<rectangle x1="0.4472" y1="-1" x2="0.625" y2="1" layer="51"/>
</package>
<package name="F0805">
<description>&lt;b&gt;0ZCK Series - PTC Resettable Fuses&lt;/b&gt;&lt;p&gt;
Source: http://www.belfuse.com/pdfs/0ZCK.pdf</description>
<smd name="1" x="-1.3" y="0" dx="1.4" dy="1.5" layer="1"/>
<smd name="2" x="1.3" y="0" dx="1.4" dy="1.5" layer="1"/>
<wire x1="-1.15" y1="-0.75" x2="-1.15" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="-1.15" y1="-0.45" x2="-1.15" y2="0.45" width="0.1016" layer="51" curve="126.343451"/>
<wire x1="-1.15" y1="0.45" x2="-1.15" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.15" y1="0.75" x2="-0.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.75" x2="0.45" y2="0.75" width="0.1016" layer="21"/>
<wire x1="0.55" y1="0.75" x2="1.15" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.15" y1="0.75" x2="1.15" y2="0.45" width="0.1016" layer="51"/>
<wire x1="1.15" y1="-0.45" x2="1.15" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.75" x2="-0.45" y2="-0.75" width="0.1016" layer="21"/>
<wire x1="-0.55" y1="-0.75" x2="-0.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.55" y1="0.75" x2="0.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1.15" y1="0.45" x2="1.15" y2="-0.45" width="0.1016" layer="51" curve="126.343451"/>
<text x="-1.1" y="1" size="0.5" layer="25">&gt;NAME</text>
<text x="-1.325" y="-1.45" size="0.5" layer="27">&gt;VALUE</text>
<wire x1="-1.15" y1="-0.75" x2="-0.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1.15" y1="-0.75" x2="0.55" y2="-0.75" width="0.1016" layer="51"/>
</package>
<package name="F1812">
<description>&lt;b&gt;0ZCG Series - PTC Resettable Fuses&lt;/b&gt;&lt;p&gt;
Source: http://www.belfuse.com/pdfs/0ZCG.pdf</description>
<smd name="1" x="-2.615" y="0" dx="1.78" dy="3.5" layer="1"/>
<smd name="2" x="2.615" y="0" dx="1.78" dy="3.5" layer="1"/>
<wire x1="-2.365" y1="-1.705" x2="-2.365" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-2.365" y1="-0.8" x2="-2.365" y2="0.7875" width="0.1016" layer="51" curve="126.343451"/>
<wire x1="-2.365" y1="0.7875" x2="-2.365" y2="1.705" width="0.1016" layer="51"/>
<wire x1="-2.365" y1="1.705" x2="-1.565" y2="1.705" width="0.1016" layer="51"/>
<wire x1="-1.565" y1="1.705" x2="1.565" y2="1.705" width="0.1016" layer="21"/>
<wire x1="1.565" y1="1.705" x2="2.365" y2="1.705" width="0.1016" layer="51"/>
<wire x1="2.365" y1="1.705" x2="2.365" y2="0.8" width="0.1016" layer="51"/>
<wire x1="2.365" y1="-0.7875" x2="2.365" y2="-1.705" width="0.1016" layer="51"/>
<wire x1="2.365" y1="-1.705" x2="1.565" y2="-1.705" width="0.1016" layer="51"/>
<wire x1="1.565" y1="-1.705" x2="-1.565" y2="-1.705" width="0.1016" layer="21"/>
<wire x1="-1.565" y1="-1.705" x2="-2.365" y2="-1.705" width="0.1016" layer="51"/>
<wire x1="-1.565" y1="-1.705" x2="-1.565" y2="1.705" width="0.1016" layer="51"/>
<wire x1="1.565" y1="1.705" x2="1.565" y2="-1.705" width="0.1016" layer="51"/>
<wire x1="2.365" y1="0.8" x2="2.365" y2="-0.7875" width="0.1016" layer="51" curve="126.343451"/>
<text x="-1.3" y="2" size="0.6" layer="25">&gt;NAME</text>
<text x="-1.625" y="-2.55" size="0.6" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<description>&lt;b&gt;Ceramic Chip Capacitor 0805&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.575" x2="0.925" y2="0.575" width="0.1" layer="51"/>
<wire x1="0.925" y1="-0.575" x2="-0.925" y2="-0.575" width="0.1016" layer="51"/>
<smd name="1" x="-1.15" y="0" dx="1.3" dy="1.25" layer="1"/>
<smd name="2" x="1.15" y="0" dx="1.3" dy="1.25" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.625" x2="-0.5" y2="0.625" layer="51"/>
<rectangle x1="0.5" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<wire x1="-0.95" y1="0.575" x2="0.95" y2="0.575" width="0.127" layer="39"/>
<wire x1="0.95" y1="0.575" x2="0.95" y2="-0.575" width="0.127" layer="39"/>
<wire x1="0.95" y1="-0.575" x2="-0.95" y2="-0.575" width="0.127" layer="39"/>
<wire x1="-0.95" y1="-0.575" x2="-0.95" y2="0.575" width="0.127" layer="39"/>
</package>
<package name="C-UCM">
<description>&lt;b&gt;UCM series electrolytic capacitor 5x5.8mm&lt;/b&gt;&lt;p&gt;
Source: 
http://nichicon-us.com/english/products/pdfs/e-ucm.pdf / http://products.nichicon.co.jp/en/pdf/XJA043/e-ch_ref.pdf</description>
<smd name="NEG" x="-2.2" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="POS" x="2.2" y="0" dx="3" dy="1.6" layer="1"/>
<text x="-2.5" y="2.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3.9" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-2.75" y1="2.75" x2="2.15" y2="2.75" width="0.127" layer="21"/>
<wire x1="2.15" y1="2.75" x2="2.75" y2="2.15" width="0.127" layer="21"/>
<wire x1="2.75" y1="2.15" x2="2.75" y2="1" width="0.127" layer="21"/>
<wire x1="2.75" y1="-1" x2="2.75" y2="-2.15" width="0.127" layer="21"/>
<wire x1="2.75" y1="-2.15" x2="2.15" y2="-2.75" width="0.127" layer="21"/>
<wire x1="2.15" y1="-2.75" x2="-2.75" y2="-2.75" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-2.75" x2="-2.75" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.75" y1="2.75" x2="-2.75" y2="1" width="0.127" layer="21"/>
</package>
<package name="C0603">
<description>&lt;b&gt;Ceramic Chip Capacitor 0603&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.975" y="0" dx="1.25" dy="1.08" layer="1"/>
<smd name="2" x="0.975" y="0" dx="1.25" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="39"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="39"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="39"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="39"/>
</package>
<package name="C-UCM-6MM">
<description>&lt;b&gt;UCM series electrolytic capacitor 6.3x7.7mm&lt;/b&gt;&lt;p&gt;
Source: 
http://nichicon-us.com/english/products/pdfs/e-ucm.pdf / http://products.nichicon.co.jp/en/pdf/XJA043/e-ch_ref.pdf</description>
<smd name="NEG" x="-2.7" y="0" dx="3.8" dy="1.6" layer="1"/>
<smd name="POS" x="2.7" y="0" dx="3.8" dy="1.6" layer="1"/>
<text x="-2.5" y="3.575" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.5" y="-4.6" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-3.4" y1="3.4" x2="2.35" y2="3.4" width="0.127" layer="21"/>
<wire x1="2.35" y1="3.4" x2="3.4" y2="2.35" width="0.127" layer="21"/>
<wire x1="3.4" y1="2.35" x2="3.4" y2="1" width="0.127" layer="21"/>
<wire x1="3.4" y1="-1" x2="3.4" y2="-2.35" width="0.127" layer="21"/>
<wire x1="3.4" y1="-2.35" x2="2.35" y2="-3.4" width="0.127" layer="21"/>
<wire x1="2.35" y1="-3.4" x2="-3.4" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="C-UCM-8MM">
<description>&lt;b&gt;UCM series electrolytic capacitor 8x6.2mm&lt;/b&gt;&lt;p&gt;
Source: 
http://nichicon-us.com/english/products/pdfs/e-ucm.pdf / http://products.nichicon.co.jp/en/pdf/XJA043/e-ch_ref.pdf</description>
<smd name="NEG" x="-2.2" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="POS" x="2.2" y="0" dx="3" dy="1.6" layer="1"/>
<text x="-2.5" y="2.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3.9" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-4.25" y1="4.25" x2="2.15" y2="4.25" width="0.127" layer="21"/>
<wire x1="2.15" y1="4.25" x2="4.25" y2="2.15" width="0.127" layer="21"/>
<wire x1="4.25" y1="2.15" x2="4.25" y2="1" width="0.127" layer="21"/>
<wire x1="4.25" y1="-1" x2="4.25" y2="-2.15" width="0.127" layer="21"/>
<wire x1="4.25" y1="-2.15" x2="2.15" y2="-4.25" width="0.127" layer="21"/>
<wire x1="2.15" y1="-4.25" x2="-4.25" y2="-4.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-4.25" x2="-4.25" y2="-1" width="0.127" layer="21"/>
<wire x1="-4.25" y1="4.25" x2="-4.25" y2="1" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SHERLOCK-6">
<pin name="1" x="0" y="7.62" visible="pad" length="point" direction="pas"/>
<pin name="2" x="0" y="5.08" visible="pad" length="point" direction="pas"/>
<pin name="3" x="0" y="2.54" visible="pad" length="point" direction="pas"/>
<pin name="4" x="0" y="0" visible="pad" length="point" direction="pas"/>
<pin name="5" x="0" y="-2.54" visible="pad" length="point" direction="pas"/>
<pin name="6" x="0" y="-5.08" visible="pad" length="point" direction="pas"/>
<wire x1="-2.54" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="DIP-3">
<description>3-pin DIP switch</description>
<pin name="IN1" x="-5.08" y="2.54" length="short" direction="pas"/>
<pin name="IN2" x="-5.08" y="0" length="short" direction="pas"/>
<pin name="IN3" x="-5.08" y="-2.54" length="short" direction="pas"/>
<pin name="OUT1" x="5.08" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="OUT2" x="5.08" y="0" length="short" direction="pas" rot="R180"/>
<pin name="OUT3" x="5.08" y="-2.54" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="RJ-45*2-6G">
<description>Dual RJ-45 shielded connector</description>
<wire x1="-5.461" y1="-22.86" x2="8.001" y2="-22.86" width="0.127" layer="94" style="shortdash"/>
<wire x1="8.001" y1="-22.86" x2="8.001" y2="21.59" width="0.127" layer="94" style="shortdash"/>
<wire x1="8.001" y1="21.59" x2="-5.461" y2="21.59" width="0.127" layer="94" style="shortdash"/>
<wire x1="-5.461" y1="21.59" x2="-5.461" y2="20.828" width="0.127" layer="94"/>
<wire x1="-5.461" y1="19.812" x2="-5.461" y2="18.288" width="0.127" layer="94"/>
<wire x1="-5.461" y1="17.272" x2="-5.461" y2="15.748" width="0.127" layer="94"/>
<wire x1="-5.461" y1="14.732" x2="-5.461" y2="13.208" width="0.127" layer="94"/>
<wire x1="-5.461" y1="12.192" x2="-5.461" y2="10.668" width="0.127" layer="94"/>
<wire x1="-5.461" y1="9.652" x2="-5.461" y2="8.128" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-20.828" x2="-5.461" y2="-22.86" width="0.127" layer="94"/>
<wire x1="-3.556" y1="20.828" x2="-5.08" y2="20.828" width="0.254" layer="94"/>
<wire x1="-5.08" y1="20.828" x2="-5.08" y2="19.812" width="0.254" layer="94"/>
<wire x1="-5.08" y1="19.812" x2="-3.556" y2="19.812" width="0.254" layer="94"/>
<wire x1="-3.556" y1="18.288" x2="-5.08" y2="18.288" width="0.254" layer="94"/>
<wire x1="-5.08" y1="18.288" x2="-5.08" y2="17.272" width="0.254" layer="94"/>
<wire x1="-5.08" y1="17.272" x2="-3.556" y2="17.272" width="0.254" layer="94"/>
<wire x1="-3.556" y1="15.748" x2="-5.08" y2="15.748" width="0.254" layer="94"/>
<wire x1="-5.08" y1="15.748" x2="-5.08" y2="14.732" width="0.254" layer="94"/>
<wire x1="-5.08" y1="14.732" x2="-3.556" y2="14.732" width="0.254" layer="94"/>
<wire x1="-3.556" y1="13.208" x2="-5.08" y2="13.208" width="0.254" layer="94"/>
<wire x1="-5.08" y1="13.208" x2="-5.08" y2="12.192" width="0.254" layer="94"/>
<wire x1="-5.08" y1="12.192" x2="-3.556" y2="12.192" width="0.254" layer="94"/>
<wire x1="-3.556" y1="10.668" x2="-5.08" y2="10.668" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.668" x2="-5.08" y2="9.652" width="0.254" layer="94"/>
<wire x1="-5.08" y1="9.652" x2="-3.556" y2="9.652" width="0.254" layer="94"/>
<wire x1="-3.556" y1="8.128" x2="-5.08" y2="8.128" width="0.254" layer="94"/>
<wire x1="-5.08" y1="8.128" x2="-5.08" y2="7.112" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.112" x2="-3.556" y2="7.112" width="0.254" layer="94"/>
<wire x1="-1.651" y1="8.636" x2="0.889" y2="8.636" width="0.1998" layer="94"/>
<wire x1="0.889" y1="8.636" x2="0.889" y2="10.414" width="0.1998" layer="94"/>
<wire x1="0.889" y1="10.414" x2="1.905" y2="10.414" width="0.1998" layer="94"/>
<wire x1="1.905" y1="10.414" x2="1.905" y2="12.446" width="0.1998" layer="94"/>
<wire x1="1.905" y1="12.446" x2="0.889" y2="12.446" width="0.1998" layer="94"/>
<wire x1="0.889" y1="12.446" x2="0.889" y2="14.224" width="0.1998" layer="94"/>
<wire x1="0.889" y1="14.224" x2="-1.651" y2="14.224" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="14.224" x2="-1.651" y2="13.208" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="13.208" x2="-1.651" y2="12.7" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="12.7" x2="-1.651" y2="12.192" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="12.192" x2="-1.651" y2="11.684" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="11.684" x2="-1.651" y2="11.176" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="11.176" x2="-1.651" y2="10.668" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="10.668" x2="-1.651" y2="10.16" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="10.16" x2="-1.651" y2="9.652" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="9.652" x2="-1.651" y2="8.636" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="13.208" x2="-0.889" y2="13.208" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="12.7" x2="-0.889" y2="12.7" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="12.192" x2="-0.889" y2="12.192" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="11.684" x2="-0.889" y2="11.684" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="11.176" x2="-0.889" y2="11.176" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="10.668" x2="-0.889" y2="10.668" width="0.1998" layer="94"/>
<wire x1="-3.556" y1="5.588" x2="-5.08" y2="5.588" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.588" x2="-5.08" y2="4.572" width="0.254" layer="94"/>
<wire x1="-5.08" y1="4.572" x2="-3.556" y2="4.572" width="0.254" layer="94"/>
<wire x1="-3.556" y1="3.048" x2="-5.08" y2="3.048" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.048" x2="-5.08" y2="2.032" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.032" x2="-3.556" y2="2.032" width="0.254" layer="94"/>
<wire x1="-1.651" y1="10.16" x2="-0.889" y2="10.16" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="9.652" x2="-0.889" y2="9.652" width="0.1998" layer="94"/>
<wire x1="-5.461" y1="7.112" x2="-5.461" y2="5.588" width="0.127" layer="94"/>
<wire x1="-5.461" y1="4.572" x2="-5.461" y2="3.048" width="0.127" layer="94"/>
<text x="-5.08" y="21.844" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="20.32" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="17.78" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="15.24" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="12.7" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="10.16" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="7" x="-7.62" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="11" x="-7.62" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="12" x="-7.62" y="-10.16" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="13" x="-7.62" y="-12.7" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="14" x="-7.62" y="-15.24" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="15" x="-7.62" y="-17.78" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="16" x="-7.62" y="-20.32" visible="pad" length="short" direction="pas" swaplevel="1"/>
<wire x1="-3.556" y1="-2.032" x2="-5.08" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.032" x2="-5.08" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-3.048" x2="-3.556" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-4.572" x2="-5.08" y2="-4.572" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-4.572" x2="-5.08" y2="-5.588" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.588" x2="-3.556" y2="-5.588" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-7.112" x2="-5.08" y2="-7.112" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.112" x2="-5.08" y2="-8.128" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-8.128" x2="-3.556" y2="-8.128" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-9.652" x2="-5.08" y2="-9.652" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-9.652" x2="-5.08" y2="-10.668" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.668" x2="-3.556" y2="-10.668" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-12.192" x2="-5.08" y2="-12.192" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-12.192" x2="-5.08" y2="-13.208" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-13.208" x2="-3.556" y2="-13.208" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-14.732" x2="-5.08" y2="-14.732" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-14.732" x2="-5.08" y2="-15.748" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-15.748" x2="-3.556" y2="-15.748" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-17.272" x2="-5.08" y2="-17.272" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-17.272" x2="-5.08" y2="-18.288" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-18.288" x2="-3.556" y2="-18.288" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-19.812" x2="-5.08" y2="-19.812" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-19.812" x2="-5.08" y2="-20.828" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-20.828" x2="-3.556" y2="-20.828" width="0.254" layer="94"/>
<wire x1="-1.651" y1="-14.224" x2="0.889" y2="-14.224" width="0.1998" layer="94"/>
<wire x1="0.889" y1="-14.224" x2="0.889" y2="-12.446" width="0.1998" layer="94"/>
<wire x1="0.889" y1="-12.446" x2="1.905" y2="-12.446" width="0.1998" layer="94"/>
<wire x1="1.905" y1="-12.446" x2="1.905" y2="-10.414" width="0.1998" layer="94"/>
<wire x1="1.905" y1="-10.414" x2="0.889" y2="-10.414" width="0.1998" layer="94"/>
<wire x1="0.889" y1="-10.414" x2="0.889" y2="-8.636" width="0.1998" layer="94"/>
<wire x1="0.889" y1="-8.636" x2="-1.651" y2="-8.636" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-8.636" x2="-1.651" y2="-9.652" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-9.652" x2="-1.651" y2="-10.16" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-10.16" x2="-1.651" y2="-10.668" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-10.668" x2="-1.651" y2="-11.176" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-11.176" x2="-1.651" y2="-11.684" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-11.684" x2="-1.651" y2="-12.192" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-12.192" x2="-1.651" y2="-12.7" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-12.7" x2="-1.651" y2="-13.208" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-13.208" x2="-1.651" y2="-14.224" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-9.652" x2="-0.889" y2="-9.652" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-10.16" x2="-0.889" y2="-10.16" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-10.668" x2="-0.889" y2="-10.668" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-11.176" x2="-0.889" y2="-11.176" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-11.684" x2="-0.889" y2="-11.684" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-12.192" x2="-0.889" y2="-12.192" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-12.7" x2="-0.889" y2="-12.7" width="0.1998" layer="94"/>
<wire x1="-1.651" y1="-13.208" x2="-0.889" y2="-13.208" width="0.1998" layer="94"/>
<wire x1="-5.461" y1="-19.812" x2="-5.461" y2="-18.288" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-17.272" x2="-5.461" y2="-15.748" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-14.732" x2="-5.461" y2="-13.208" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-12.192" x2="-5.461" y2="-10.668" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-9.652" x2="-5.461" y2="-8.128" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-7.112" x2="-5.461" y2="-5.588" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-4.572" x2="-5.461" y2="-3.048" width="0.127" layer="94"/>
<wire x1="-5.461" y1="-2.032" x2="-5.461" y2="2.032" width="0.127" layer="94"/>
<pin name="S@1" x="-5.08" y="-25.4" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S@2" x="-2.54" y="-25.4" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S@3" x="0" y="-25.4" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S@4" x="2.54" y="-25.4" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S@5" x="5.08" y="-25.4" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S@6" x="7.62" y="-25.4" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="SPST">
<pin name="1@1" x="-7.62" y="2.54" visible="pad" length="short"/>
<pin name="2@1" x="7.62" y="2.54" visible="pad" length="short" rot="R180"/>
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-5.08" y="4.064" size="1.778" layer="95">&gt;NAME</text>
<circle x="-2.54" y="0" radius="0.254" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.254" width="0.254" layer="94"/>
<pin name="1@2" x="-7.62" y="0" visible="pad" length="short"/>
<pin name="2@2" x="7.62" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="P-MOS">
<description>P-channel MOSFET</description>
<wire x1="0" y1="0" x2="-1.016" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.381" x2="-1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.381" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.381" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0.762" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="-0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-0.127" x2="-0.889" y2="0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0.127" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="1.524" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.635" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.016" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="in"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="out" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="in" rot="R270"/>
</symbol>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="0RJM">
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="1.524" x2="0.254" y2="1.524" width="0.508" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.508" layer="94" curve="-180" cap="flat"/>
<wire x1="0.254" y1="1.016" x2="-0.254" y2="1.016" width="0.508" layer="94" curve="-180" cap="flat"/>
<wire x1="0.254" y1="-1.524" x2="-0.254" y2="-1.524" width="0.508" layer="94" curve="-180" cap="flat"/>
<text x="2.54" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
</symbol>
<symbol name="PAM2305">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VIN" x="-10.16" y="5.08" length="short" direction="pwr"/>
<pin name="EN" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="VOUT" x="10.16" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="SW" x="10.16" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="0" y="-7.62" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="L-US">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
<symbol name="FUSE">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.397" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="6-1971800">
<description>6-pin Sherlock connector</description>
<gates>
<gate name="C" symbol="SHERLOCK-6" x="0" y="0"/>
</gates>
<devices>
<device name="SH" package="SHERLOCK-6">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
<connect gate="C" pin="3" pad="3"/>
<connect gate="C" pin="4" pad="4"/>
<connect gate="C" pin="5" pad="5"/>
<connect gate="C" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIP-3-219">
<gates>
<gate name="A" symbol="DIP-3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GULL-WING">
<connects>
<connect gate="A" pin="IN1" pad="IN1"/>
<connect gate="A" pin="IN2" pad="IN2"/>
<connect gate="A" pin="IN3" pad="IN3"/>
<connect gate="A" pin="OUT1" pad="OUT1"/>
<connect gate="A" pin="OUT2" pad="OUT2"/>
<connect gate="A" pin="OUT3" pad="OUT3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RJ45*2-V" prefix="I2C">
<description>&lt;p&gt;2x RJ-45 connectors, vertical orientation, shielded&lt;/p&gt;

Source: http://productfinder.pulseeng.com/products/datasheets/E5908-4V0S54-L.pdf</description>
<gates>
<gate name="A" symbol="RJ-45*2-6G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RJ45-V-SHIELD*2">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
<connect gate="A" pin="S@1" pad="S1"/>
<connect gate="A" pin="S@2" pad="S2"/>
<connect gate="A" pin="S@3" pad="S3"/>
<connect gate="A" pin="S@4" pad="S4"/>
<connect gate="A" pin="S@5" pad="S5"/>
<connect gate="A" pin="S@6" pad="S6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW-1825910-7">
<description>SPST pushbutton switch</description>
<gates>
<gate name="G$1" symbol="SPST" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWITCH">
<connects>
<connect gate="G$1" pin="1@1" pad="1"/>
<connect gate="G$1" pin="1@2" pad="2"/>
<connect gate="G$1" pin="2@1" pad="3"/>
<connect gate="G$1" pin="2@2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DMG2305UX" prefix="Q">
<description>&lt;b&gt;P-CHANNEL MOSFET&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="A" pin="D" pad="3"/>
<connect gate="A" pin="G" pad="1"/>
<connect gate="A" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-US" prefix="C" uservalue="yes">
<description>Capacitor, US symbol</description>
<gates>
<gate name="C" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="0306" package="C0306">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="C0508">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-UCM" package="C-UCM">
<connects>
<connect gate="C" pin="1" pad="POS"/>
<connect gate="C" pin="2" pad="NEG"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-UCM-6MM" package="C-UCM-6MM">
<connects>
<connect gate="C" pin="1" pad="POS"/>
<connect gate="C" pin="2" pad="NEG"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-UCM-8MM" package="C-UCM-8MM">
<connects>
<connect gate="C" pin="1" pad="POS"/>
<connect gate="C" pin="2" pad="NEG"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0R-JUMP" prefix="J">
<description>0R jumper, SMD</description>
<gates>
<gate name="A" symbol="0RJM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="A0R-JMP">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAM2305" prefix="U">
<description>&lt;b&gt;3.3V VOLTAGE REGULATOR&lt;/b&gt;</description>
<gates>
<gate name="G1" symbol="PAM2305" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G1" pin="EN" pad="3"/>
<connect gate="G1" pin="GND" pad="2"/>
<connect gate="G1" pin="SW" pad="5"/>
<connect gate="G1" pin="VIN" pad="1"/>
<connect gate="G1" pin="VOUT" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L-US" prefix="L" uservalue="yes">
<description>&lt;B&gt;INDUCTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="L" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="L2012C" package="L3X3MM">
<connects>
<connect gate="L" pin="1" pad="1"/>
<connect gate="L" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE" prefix="F" uservalue="yes">
<description>&lt;b&gt;Belfuse PTC Resettable Fuses&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="F0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="F1812">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="i2c-driver">
<description>&lt;b&gt;I2C bus buffer and CAN transceiver&lt;/b&gt;&lt;p&gt;
 &lt;author&gt;Created by bryan@kadzban.net&lt;/author&gt;</description>
<packages>
<package name="TSSOP56-6">
<description>&lt;b&gt;TSSOP56&lt;/b&gt; Plastic thin shrink small outline package; 56 leads; body width 6.1mm AKA SOT364-1&lt;p&gt;
Source: http://www.nxp.com/documents/data_sheet/PCA9698.pdf</description>
<wire x1="7" y1="-3" x2="7" y2="3" width="0.2032" layer="21"/>
<wire x1="7" y1="3" x2="-7" y2="3" width="0.2032" layer="21"/>
<wire x1="-7" y1="3" x2="-7" y2="-3" width="0.2032" layer="21"/>
<wire x1="-7" y1="-3" x2="7" y2="-3" width="0.2032" layer="21"/>
<circle x="-6.05" y="-2.07" radius="0.5" width="0.2032" layer="21"/>
<smd name="1" x="-6.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="2" x="-6.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="3" x="-5.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="4" x="-5.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="5" x="-4.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="6" x="-4.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="7" x="-3.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="8" x="-3.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="9" x="-2.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="10" x="-2.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="11" x="-1.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="12" x="-1.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="13" x="-0.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="14" x="-0.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="15" x="0.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="16" x="0.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="17" x="1.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="18" x="1.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="19" x="2.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="20" x="2.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="21" x="3.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="22" x="3.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="23" x="4.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="24" x="4.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="25" x="5.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="26" x="5.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="27" x="6.25" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="28" x="6.75" y="-4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="29" x="6.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="30" x="6.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="31" x="5.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="32" x="5.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="33" x="4.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="34" x="4.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="35" x="3.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="36" x="3.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="37" x="2.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="38" x="2.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="39" x="1.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="40" x="1.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="41" x="0.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="42" x="0.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="43" x="-0.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="44" x="-0.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="45" x="-1.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="46" x="-1.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="47" x="-2.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="48" x="-2.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="49" x="-3.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="50" x="-3.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="51" x="-4.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="52" x="-4.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="53" x="-5.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="54" x="-5.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="55" x="-6.25" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<smd name="56" x="-6.75" y="4.05" dx="1.7" dy="0.2" layer="1" rot="R90"/>
<text x="-7.62" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-7.15" y1="-3.55" x2="-6.35" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-6.65" y1="-3.55" x2="-5.85" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-6.15" y1="-3.55" x2="-5.35" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-5.65" y1="-3.55" x2="-4.85" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-5.15" y1="-3.55" x2="-4.35" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-4.65" y1="-3.55" x2="-3.85" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-4.15" y1="-3.55" x2="-3.35" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-3.65" y1="-3.55" x2="-2.85" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-3.15" y1="-3.55" x2="-2.35" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-2.65" y1="-3.55" x2="-1.85" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-2.15" y1="-3.55" x2="-1.35" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-1.65" y1="-3.55" x2="-0.85" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-1.15" y1="-3.55" x2="-0.35" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-0.65" y1="-3.55" x2="0.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="-0.15" y1="-3.55" x2="0.65" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="0.35" y1="-3.55" x2="1.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="0.85" y1="-3.55" x2="1.65" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="1.35" y1="-3.55" x2="2.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="1.85" y1="-3.55" x2="2.65" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="2.35" y1="-3.55" x2="3.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="2.85" y1="-3.55" x2="3.65" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="3.35" y1="-3.55" x2="4.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="3.85" y1="-3.55" x2="4.65" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="4.35" y1="-3.55" x2="5.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="4.85" y1="-3.55" x2="5.65" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="5.35" y1="-3.55" x2="6.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="5.85" y1="-3.55" x2="6.65" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="6.35" y1="-3.55" x2="7.15" y2="-3.35" layer="51" rot="R90"/>
<rectangle x1="6.35" y1="3.41" x2="7.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="5.85" y1="3.41" x2="6.65" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="5.35" y1="3.41" x2="6.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="4.85" y1="3.41" x2="5.65" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="4.35" y1="3.41" x2="5.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="3.85" y1="3.41" x2="4.65" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="3.35" y1="3.41" x2="4.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="2.85" y1="3.41" x2="3.65" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="2.35" y1="3.41" x2="3.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="1.85" y1="3.41" x2="2.65" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="1.35" y1="3.41" x2="2.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="0.85" y1="3.41" x2="1.65" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="0.35" y1="3.41" x2="1.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-0.15" y1="3.41" x2="0.65" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-0.65" y1="3.41" x2="0.15" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-1.15" y1="3.41" x2="-0.35" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-1.65" y1="3.41" x2="-0.85" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-2.15" y1="3.41" x2="-1.35" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-2.65" y1="3.41" x2="-1.85" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-3.15" y1="3.41" x2="-2.35" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-3.65" y1="3.41" x2="-2.85" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-4.15" y1="3.41" x2="-3.35" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-4.65" y1="3.41" x2="-3.85" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-5.15" y1="3.41" x2="-4.35" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-5.65" y1="3.41" x2="-4.85" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-6.15" y1="3.41" x2="-5.35" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-6.65" y1="3.41" x2="-5.85" y2="3.61" layer="51" rot="R90"/>
<rectangle x1="-7.15" y1="3.41" x2="-6.35" y2="3.61" layer="51" rot="R90"/>
</package>
<package name="TSSOP16">
<description>&lt;b&gt;TSSOP16: plastic thin shrink small outline package; 16 leads; body width 4.4 mm&lt;/b&gt; SOT403-1&lt;p&gt;
Source: http://www.nxp.com/documents/data_sheet/PCA9616.pdf</description>
<wire x1="-2.5" y1="-2.2" x2="2.5" y2="-2.2" width="0.1524" layer="21"/>
<wire x1="2.5" y1="2.2" x2="2.5" y2="-2.2" width="0.1524" layer="21"/>
<wire x1="2.5" y1="2.2" x2="-2.5" y2="2.2" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="-2.2" x2="-2.5" y2="2.2" width="0.1524" layer="21"/>
<circle x="-1.65" y="-1.25" radius="0.3" width="0.1" layer="21"/>
<smd name="1" x="-2.275" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<smd name="2" x="-1.625" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<smd name="3" x="-0.975" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<smd name="4" x="-0.325" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<smd name="5" x="0.325" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<text x="-2.6956" y="-2.2828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.7862" y="-2.2828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.3875" y1="-3.2" x2="-2.1625" y2="-2.2" layer="51"/>
<rectangle x1="-1.7375" y1="-3.2" x2="-1.5125" y2="-2.2" layer="51"/>
<rectangle x1="-1.0875" y1="-3.2" x2="-0.8625" y2="-2.2" layer="51"/>
<rectangle x1="-0.4375" y1="-3.2" x2="-0.2125" y2="-2.2" layer="51"/>
<rectangle x1="0.2125" y1="-3.2" x2="0.4375" y2="-2.2" layer="51"/>
<rectangle x1="-2.425" y1="-4" x2="-2.125" y2="-2.45" layer="29"/>
<rectangle x1="-1.775" y1="-4" x2="-1.475" y2="-2.45" layer="29"/>
<rectangle x1="-1.125" y1="-4" x2="-0.825" y2="-2.45" layer="29"/>
<rectangle x1="-0.475" y1="-4" x2="-0.175" y2="-2.45" layer="29"/>
<rectangle x1="0.175" y1="-4" x2="0.475" y2="-2.45" layer="29"/>
<smd name="6" x="0.975" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<smd name="7" x="1.625" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<smd name="8" x="2.275" y="-3.225" dx="0.3" dy="1.55" layer="1" stop="no"/>
<rectangle x1="0.8625" y1="-3.2" x2="1.0875" y2="-2.2" layer="51"/>
<rectangle x1="1.5125" y1="-3.2" x2="1.7375" y2="-2.2" layer="51"/>
<rectangle x1="2.1625" y1="-3.2" x2="2.3875" y2="-2.2" layer="51"/>
<rectangle x1="0.825" y1="-4" x2="1.125" y2="-2.45" layer="29"/>
<rectangle x1="1.475" y1="-4" x2="1.775" y2="-2.45" layer="29"/>
<rectangle x1="2.125" y1="-4" x2="2.425" y2="-2.45" layer="29"/>
<smd name="9" x="2.275" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<smd name="10" x="1.625" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<smd name="11" x="0.975" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<smd name="12" x="0.325" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<smd name="13" x="-0.325" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.1625" y1="2.2" x2="2.3875" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="1.5125" y1="2.2" x2="1.7375" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="0.8625" y1="2.2" x2="1.0875" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="0.2125" y1="2.2" x2="0.4375" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-0.4375" y1="2.2" x2="-0.2125" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-2.425" y1="2.45" x2="-2.125" y2="4" layer="29"/>
<rectangle x1="-1.775" y1="2.45" x2="-1.475" y2="4" layer="29"/>
<rectangle x1="-1.125" y1="2.45" x2="-0.825" y2="4" layer="29"/>
<rectangle x1="-0.475" y1="2.45" x2="-0.175" y2="4" layer="29"/>
<rectangle x1="0.175" y1="2.45" x2="0.475" y2="4" layer="29"/>
<smd name="14" x="-0.975" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<smd name="15" x="-1.625" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<smd name="16" x="-2.275" y="3.225" dx="0.3" dy="1.55" layer="1" rot="R180" stop="no"/>
<rectangle x1="-1.0875" y1="2.2" x2="-0.8625" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-1.7375" y1="2.2" x2="-1.5125" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="-2.3875" y1="2.2" x2="-2.1625" y2="3.2" layer="51" rot="R180"/>
<rectangle x1="0.825" y1="2.45" x2="1.125" y2="4" layer="29"/>
<rectangle x1="1.475" y1="2.45" x2="1.775" y2="4" layer="29"/>
<rectangle x1="2.125" y1="2.45" x2="2.425" y2="4" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="PCA9698">
<wire x1="-27.94" y1="-66.04" x2="27.94" y2="-66.04" width="0.254" layer="94"/>
<wire x1="27.94" y1="-66.04" x2="27.94" y2="66.04" width="0.254" layer="94"/>
<wire x1="27.94" y1="66.04" x2="-27.94" y2="66.04" width="0.254" layer="94"/>
<wire x1="-27.94" y1="66.04" x2="-27.94" y2="-66.04" width="0.254" layer="94"/>
<wire x1="20.32" y1="60.96" x2="12.7" y2="60.96" width="0.1524" layer="94"/>
<wire x1="12.7" y1="60.96" x2="12.7" y2="58.42" width="0.1524" layer="94"/>
<wire x1="12.7" y1="58.42" x2="5.08" y2="58.42" width="0.1524" layer="94"/>
<wire x1="5.08" y1="22.86" x2="5.08" y2="-45.212" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-45.212" x2="10.16" y2="-45.212" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-45.212" x2="12.7" y2="-45.212" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-45.212" x2="17.78" y2="-45.212" width="0.1524" layer="94"/>
<wire x1="17.78" y1="-45.212" x2="17.78" y2="58.42" width="0.1524" layer="94"/>
<wire x1="17.78" y1="58.42" x2="12.7" y2="58.42" width="0.1524" layer="94"/>
<wire x1="0" y1="58.42" x2="-10.16" y2="58.42" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="58.42" x2="-10.16" y2="53.34" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="53.34" x2="-10.16" y2="48.26" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="43.18" x2="-10.16" y2="30.48" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="30.48" x2="-10.16" y2="25.4" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="25.4" x2="-10.16" y2="12.192" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="12.192" x2="-10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="-6.35" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-12.7" x2="-3.81" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-12.7" x2="0" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="-12.7" x2="0" y2="20.32" width="0.1524" layer="94"/>
<wire x1="0" y1="20.32" x2="0" y2="25.4" width="0.1524" layer="94"/>
<wire x1="0" y1="25.4" x2="0" y2="58.42" width="0.1524" layer="94"/>
<wire x1="-19.304" y1="30.48" x2="-10.16" y2="30.48" width="0.1524" layer="94"/>
<wire x1="-19.304" y1="25.4" x2="-10.16" y2="25.4" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="12.192" x2="-20.32" y2="12.192" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="12.192" x2="-20.32" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="20.32" x2="-21.844" y2="19.304" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="19.304" x2="-21.844" y2="18.288" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="17.272" x2="-21.844" y2="16.256" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="16.256" x2="-21.844" y2="15.24" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="14.224" x2="-21.844" y2="13.208" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="16.256" x2="-23.876" y2="16.256" width="0.1524" layer="94"/>
<wire x1="-23.876" y1="16.256" x2="-23.876" y2="14.732" width="0.1524" layer="94"/>
<wire x1="-23.876" y1="14.732" x2="-23.876" y2="13.208" width="0.1524" layer="94"/>
<wire x1="-23.876" y1="13.208" x2="-21.844" y2="13.208" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="13.208" x2="-21.844" y2="12.192" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="19.304" x2="-26.416" y2="19.304" width="0.1524" layer="94"/>
<wire x1="-26.416" y1="19.304" x2="-26.416" y2="24.892" width="0.1524" layer="94"/>
<wire x1="-23.876" y1="14.732" x2="-25.4" y2="14.732" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="14.732" x2="-25.4" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="10.16" x2="-23.876" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-23.876" y1="10.16" x2="-23.876" y2="9.144" width="0.1524" layer="94"/>
<wire x1="-23.876" y1="9.144" x2="-26.924" y2="9.144" width="0.1524" layer="94"/>
<wire x1="-26.924" y1="9.144" x2="-26.924" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-26.924" y1="10.16" x2="-25.4" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="0" x2="-15.24" y2="0" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="0" x2="-15.24" y2="-26.416" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-26.416" x2="-17.78" y2="-26.416" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-26.416" x2="-15.24" y2="-26.416" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-26.416" x2="-14.224" y2="-26.416" width="0.1524" layer="94"/>
<wire x1="-14.224" y1="-26.416" x2="-14.224" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="-14.224" y1="-33.02" x2="-14.224" y2="-54.356" width="0.1524" layer="94"/>
<wire x1="-14.224" y1="-54.356" x2="-25.4" y2="-54.356" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-54.356" x2="-25.4" y2="-26.416" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-22.86" x2="-17.78" y2="-26.416" width="0.1524" layer="94"/>
<wire x1="-14.224" y1="-33.02" x2="-12.7" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-33.02" x2="-12.7" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="58.42" x2="5.08" y2="22.86" width="0.1524" layer="94"/>
<wire x1="13.716" y1="-49.276" x2="13.716" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="13.716" y1="-53.34" x2="0" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="0" y1="-53.34" x2="-10.16" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-53.34" x2="-10.16" y2="-49.276" width="0.1524" layer="94"/>
<wire x1="-5.334" y1="-49.276" x2="-4.826" y2="-49.276" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-49.276" x2="-5.334" y2="-49.276" width="0.1524" layer="94"/>
<wire x1="-4.826" y1="-49.276" x2="13.716" y2="-49.276" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-57.912" x2="-5.08" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-63.5" x2="5.588" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="5.588" y1="-63.5" x2="5.588" y2="-60.452" width="0.1524" layer="94"/>
<wire x1="5.588" y1="-60.452" x2="5.588" y2="-57.912" width="0.1524" layer="94"/>
<wire x1="5.588" y1="-57.912" x2="0" y2="-57.912" width="0.1524" layer="94"/>
<wire x1="0" y1="-57.912" x2="-5.08" y2="-57.912" width="0.1524" layer="94"/>
<wire x1="0" y1="-53.34" x2="0" y2="-57.912" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="16.256" x2="-22.86" y2="16.764" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="16.256" x2="-22.86" y2="15.748" width="0.1524" layer="94"/>
<wire x1="5.588" y1="-60.452" x2="15.748" y2="-60.452" width="0.1524" layer="94"/>
<wire x1="15.748" y1="-60.452" x2="15.748" y2="-52.324" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-52.324" x2="17.272" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-53.34" x2="17.272" y2="-54.356" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-55.372" x2="17.272" y2="-56.388" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-56.388" x2="17.272" y2="-57.404" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-58.42" x2="17.272" y2="-59.436" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-56.388" x2="19.304" y2="-56.388" width="0.1524" layer="94"/>
<wire x1="19.304" y1="-56.388" x2="19.304" y2="-57.912" width="0.1524" layer="94"/>
<wire x1="19.304" y1="-57.912" x2="19.304" y2="-59.436" width="0.1524" layer="94"/>
<wire x1="19.304" y1="-59.436" x2="17.272" y2="-59.436" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-59.436" x2="17.272" y2="-60.452" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-53.34" x2="24.384" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="19.304" y1="-57.912" x2="20.828" y2="-57.912" width="0.1524" layer="94"/>
<wire x1="20.828" y1="-57.912" x2="20.828" y2="-62.484" width="0.1524" layer="94"/>
<wire x1="20.828" y1="-62.484" x2="19.304" y2="-62.484" width="0.1524" layer="94"/>
<wire x1="19.304" y1="-62.484" x2="19.304" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="19.304" y1="-63.5" x2="22.352" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="22.352" y1="-63.5" x2="22.352" y2="-62.484" width="0.1524" layer="94"/>
<wire x1="22.352" y1="-62.484" x2="20.828" y2="-62.484" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-56.388" x2="18.288" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="17.272" y1="-56.388" x2="18.288" y2="-56.896" width="0.1524" layer="94"/>
<wire x1="-5.334" y1="-49.276" x2="-3.81" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-48.26" x2="-4.826" y2="-49.276" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-12.7" x2="-6.35" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-12.7" x2="-3.81" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="11.176" y1="-49.276" x2="12.7" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-48.26" x2="11.684" y2="-49.276" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-45.212" x2="10.16" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-45.212" x2="12.7" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="-10.668" y1="25.908" x2="-10.16" y2="25.4" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="25.4" x2="-10.668" y2="24.892" width="0.1524" layer="94"/>
<wire x1="-10.668" y1="-4.572" x2="-10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.668" y2="-5.588" width="0.1524" layer="94"/>
<wire x1="-10.668" y1="30.988" x2="-10.16" y2="30.48" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="30.48" x2="-10.668" y2="29.972" width="0.1524" layer="94"/>
<wire x1="13.208" y1="58.928" x2="12.7" y2="58.42" width="0.1524" layer="94"/>
<wire x1="12.7" y1="58.42" x2="12.192" y2="58.928" width="0.1524" layer="94"/>
<wire x1="2.54" y1="25.4" x2="5.08" y2="22.86" width="0.1524" layer="94"/>
<wire x1="5.08" y1="22.86" x2="2.54" y2="20.32" width="0.1524" layer="94"/>
<wire x1="0" y1="25.4" x2="2.54" y2="25.4" width="0.1524" layer="94"/>
<wire x1="0" y1="20.32" x2="2.54" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-10.668" y1="43.688" x2="-10.16" y2="43.18" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="43.18" x2="-10.668" y2="42.672" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="43.18" x2="-10.16" y2="43.18" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="43.18" x2="-10.16" y2="48.26" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="48.26" x2="-21.844" y2="48.26" width="0.1524" layer="94"/>
<wire x1="-21.844" y1="53.34" x2="-10.16" y2="53.34" width="0.1524" layer="94"/>
<wire x1="-10.668" y1="48.768" x2="-10.16" y2="48.26" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="48.26" x2="-10.668" y2="47.752" width="0.1524" layer="94"/>
<wire x1="-10.668" y1="53.848" x2="-10.16" y2="53.34" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="53.34" x2="-10.668" y2="52.832" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="2.54" x2="-17.78" y2="0" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-19.304" x2="-16.256" y2="-19.304" width="0.1524" layer="94"/>
<wire x1="-16.256" y1="-19.304" x2="-16.256" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="-16.256" y1="-20.32" x2="-19.304" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="-19.304" y1="-20.32" x2="-19.304" y2="-19.304" width="0.1524" layer="94"/>
<wire x1="-19.304" y1="-19.304" x2="-17.78" y2="-19.304" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-19.304" x2="-17.78" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-17.78" x2="-17.78" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-15.24" x2="-17.78" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-12.7" x2="-17.78" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="-17.78" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-7.62" x2="-17.78" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-5.08" x2="-19.812" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-7.62" x2="-19.812" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="-19.812" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-12.7" x2="-19.812" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-15.24" x2="-19.812" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-17.78" x2="-19.812" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="0" x2="-19.304" y2="0" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="2.54" x2="-19.304" y2="2.54" width="0.1524" layer="94"/>
<text x="-9.144" y="26.416" size="1.27" layer="94">I2C-BUS</text>
<text x="-9.652" y="23.876" size="1.27" layer="94">CONTROL</text>
<text x="8.636" y="52.324" size="1.27" layer="94">INPUT/</text>
<text x="7.62" y="49.276" size="1.27" layer="94">OUTPUT</text>
<text x="8.128" y="46.228" size="1.27" layer="94">PORTS</text>
<text x="8.636" y="40.132" size="1.27" layer="94">BANK 0</text>
<text x="8.636" y="20.32" size="1.27" layer="94">BANK 1</text>
<text x="8.636" y="0" size="1.27" layer="94">BANK 2</text>
<text x="8.636" y="-20.32" size="1.27" layer="94">BANK 3</text>
<text x="8.636" y="-40.64" size="1.27" layer="94">BANK 4</text>
<text x="-24.892" y="-32.004" size="1.27" layer="94">POWER-ON</text>
<text x="-22.352" y="-34.544" size="1.27" layer="94">RESET</text>
<text x="-9.652" y="-51.816" size="1.27" layer="94">INTERRUPT MANAGEMENT</text>
<text x="-4.064" y="-60.452" size="1.27" layer="94">LOW PASS</text>
<text x="-3.048" y="-62.484" size="1.27" layer="94">FILTER</text>
<text x="-27.94" y="67.31" size="1.778" layer="95">&gt;NAME</text>
<text x="-27.94" y="-68.58" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A0" x="-30.48" y="53.34" length="short" direction="in"/>
<pin name="!INT" x="30.48" y="-50.8" length="short" direction="out" rot="R180"/>
<pin name="!RESET" x="-30.48" y="-22.86" length="short" direction="in"/>
<pin name="VDD@1" x="-30.48" y="2.54" length="short" direction="pwr"/>
<pin name="SDA" x="-30.48" y="25.4" length="short"/>
<pin name="SCL" x="-30.48" y="30.48" length="short" direction="in"/>
<pin name="IO0_0" x="30.48" y="55.88" length="short" rot="R180"/>
<pin name="!OE" x="30.48" y="60.96" length="short" direction="in" rot="R180"/>
<pin name="IO0_1" x="30.48" y="53.34" length="short" rot="R180"/>
<pin name="IO0_2" x="30.48" y="50.8" length="short" rot="R180"/>
<pin name="IO0_3" x="30.48" y="48.26" length="short" rot="R180"/>
<pin name="IO0_4" x="30.48" y="45.72" length="short" rot="R180"/>
<pin name="IO0_5" x="30.48" y="43.18" length="short" rot="R180"/>
<pin name="IO0_6" x="30.48" y="40.64" length="short" rot="R180"/>
<pin name="IO0_7" x="30.48" y="38.1" length="short" rot="R180"/>
<pin name="VSS@1" x="-30.48" y="-5.08" length="short" direction="pwr"/>
<pin name="IO1_0" x="30.48" y="35.56" length="short" rot="R180"/>
<pin name="IO1_1" x="30.48" y="33.02" length="short" rot="R180"/>
<pin name="IO1_2" x="30.48" y="30.48" length="short" rot="R180"/>
<pin name="IO1_3" x="30.48" y="27.94" length="short" rot="R180"/>
<pin name="IO1_4" x="30.48" y="25.4" length="short" rot="R180"/>
<pin name="IO1_5" x="30.48" y="22.86" length="short" rot="R180"/>
<pin name="IO1_6" x="30.48" y="20.32" length="short" rot="R180"/>
<pin name="IO1_7" x="30.48" y="17.78" length="short" rot="R180"/>
<pin name="IO2_0" x="30.48" y="15.24" length="short" rot="R180"/>
<pin name="IO2_1" x="30.48" y="12.7" length="short" rot="R180"/>
<pin name="A1" x="-30.48" y="48.26" length="short" direction="in"/>
<pin name="A2" x="-30.48" y="43.18" length="short" direction="in"/>
<pin name="IO2_2" x="30.48" y="10.16" length="short" rot="R180"/>
<pin name="IO2_3" x="30.48" y="7.62" length="short" rot="R180"/>
<pin name="IO2_4" x="30.48" y="5.08" length="short" rot="R180"/>
<pin name="IO2_5" x="30.48" y="2.54" length="short" rot="R180"/>
<pin name="IO2_6" x="30.48" y="0" length="short" rot="R180"/>
<pin name="IO2_7" x="30.48" y="-2.54" length="short" rot="R180"/>
<pin name="IO3_0" x="30.48" y="-5.08" length="short" rot="R180"/>
<pin name="IO3_1" x="30.48" y="-7.62" length="short" rot="R180"/>
<pin name="IO3_2" x="30.48" y="-10.16" length="short" rot="R180"/>
<pin name="IO3_3" x="30.48" y="-12.7" length="short" rot="R180"/>
<pin name="IO3_4" x="30.48" y="-15.24" length="short" rot="R180"/>
<pin name="IO3_5" x="30.48" y="-17.78" length="short" rot="R180"/>
<pin name="IO3_6" x="30.48" y="-20.32" length="short" rot="R180"/>
<pin name="IO3_7" x="30.48" y="-22.86" length="short" rot="R180"/>
<pin name="IO4_0" x="30.48" y="-25.4" length="short" rot="R180"/>
<pin name="IO4_1" x="30.48" y="-27.94" length="short" rot="R180"/>
<pin name="IO4_2" x="30.48" y="-30.48" length="short" rot="R180"/>
<pin name="IO4_3" x="30.48" y="-33.02" length="short" rot="R180"/>
<pin name="IO4_4" x="30.48" y="-35.56" length="short" rot="R180"/>
<pin name="IO4_5" x="30.48" y="-38.1" length="short" rot="R180"/>
<pin name="IO4_6" x="30.48" y="-40.64" length="short" rot="R180"/>
<pin name="IO4_7" x="30.48" y="-43.18" length="short" rot="R180"/>
<pin name="VDD@2" x="-30.48" y="0" length="short" direction="pwr"/>
<pin name="VSS@2" x="-30.48" y="-7.62" length="short" direction="pwr"/>
<pin name="VSS@3" x="-30.48" y="-10.16" length="short" direction="pwr"/>
<pin name="VSS@4" x="-30.48" y="-12.7" length="short" direction="pwr"/>
<pin name="VSS@5" x="-30.48" y="-15.24" length="short" direction="pwr"/>
<pin name="VSS@6" x="-30.48" y="-17.78" length="short" direction="pwr"/>
</symbol>
<symbol name="PCA9616">
<description>PCA9616 - Triple bidirectional bus buffer</description>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD(A)" x="-12.7" y="12.7" length="short" direction="pwr"/>
<pin name="SDA" x="-12.7" y="2.54" length="short"/>
<pin name="DSDAP" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="VSS" x="-12.7" y="-15.24" length="short" direction="pwr"/>
<pin name="SCL" x="-12.7" y="0" length="short" function="clk"/>
<pin name="DSDAM" x="12.7" y="0" length="short" rot="R180"/>
<pin name="DSCLP" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="DSCLM" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="EN" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="VDD(B)" x="12.7" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="!PIDET" x="-12.7" y="-7.62" length="short" direction="out"/>
<pin name="!READY" x="-12.7" y="-10.16" length="short" direction="out"/>
<pin name="VDDA_SEL" x="12.7" y="7.62" length="short" direction="in" rot="R180"/>
<pin name="INT" x="-12.7" y="-2.54" length="short"/>
<pin name="DINTP" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="DINTM" x="12.7" y="-15.24" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCA9698" prefix="IC">
<description>&lt;b&gt;40-bit I2C-bus I/O port with /RESET, /OE and /INT&lt;/b&gt;&lt;p&gt;
Source: http://www.nxp.com/documents/data_sheet/PCA9698.pdf</description>
<gates>
<gate name="G$1" symbol="PCA9698" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP56-6">
<connects>
<connect gate="G$1" pin="!INT" pad="55"/>
<connect gate="G$1" pin="!OE" pad="30"/>
<connect gate="G$1" pin="!RESET" pad="56"/>
<connect gate="G$1" pin="A0" pad="27"/>
<connect gate="G$1" pin="A1" pad="28"/>
<connect gate="G$1" pin="A2" pad="29"/>
<connect gate="G$1" pin="IO0_0" pad="3"/>
<connect gate="G$1" pin="IO0_1" pad="4"/>
<connect gate="G$1" pin="IO0_2" pad="5"/>
<connect gate="G$1" pin="IO0_3" pad="7"/>
<connect gate="G$1" pin="IO0_4" pad="8"/>
<connect gate="G$1" pin="IO0_5" pad="9"/>
<connect gate="G$1" pin="IO0_6" pad="10"/>
<connect gate="G$1" pin="IO0_7" pad="12"/>
<connect gate="G$1" pin="IO1_0" pad="13"/>
<connect gate="G$1" pin="IO1_1" pad="14"/>
<connect gate="G$1" pin="IO1_2" pad="15"/>
<connect gate="G$1" pin="IO1_3" pad="16"/>
<connect gate="G$1" pin="IO1_4" pad="17"/>
<connect gate="G$1" pin="IO1_5" pad="19"/>
<connect gate="G$1" pin="IO1_6" pad="20"/>
<connect gate="G$1" pin="IO1_7" pad="21"/>
<connect gate="G$1" pin="IO2_0" pad="22"/>
<connect gate="G$1" pin="IO2_1" pad="24"/>
<connect gate="G$1" pin="IO2_2" pad="25"/>
<connect gate="G$1" pin="IO2_3" pad="26"/>
<connect gate="G$1" pin="IO2_4" pad="31"/>
<connect gate="G$1" pin="IO2_5" pad="32"/>
<connect gate="G$1" pin="IO2_6" pad="33"/>
<connect gate="G$1" pin="IO2_7" pad="35"/>
<connect gate="G$1" pin="IO3_0" pad="36"/>
<connect gate="G$1" pin="IO3_1" pad="37"/>
<connect gate="G$1" pin="IO3_2" pad="38"/>
<connect gate="G$1" pin="IO3_3" pad="40"/>
<connect gate="G$1" pin="IO3_4" pad="41"/>
<connect gate="G$1" pin="IO3_5" pad="42"/>
<connect gate="G$1" pin="IO3_6" pad="43"/>
<connect gate="G$1" pin="IO3_7" pad="44"/>
<connect gate="G$1" pin="IO4_0" pad="45"/>
<connect gate="G$1" pin="IO4_1" pad="47"/>
<connect gate="G$1" pin="IO4_2" pad="48"/>
<connect gate="G$1" pin="IO4_3" pad="49"/>
<connect gate="G$1" pin="IO4_4" pad="50"/>
<connect gate="G$1" pin="IO4_5" pad="52"/>
<connect gate="G$1" pin="IO4_6" pad="53"/>
<connect gate="G$1" pin="IO4_7" pad="54"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="VDD@1" pad="18"/>
<connect gate="G$1" pin="VDD@2" pad="46"/>
<connect gate="G$1" pin="VSS@1" pad="6"/>
<connect gate="G$1" pin="VSS@2" pad="11"/>
<connect gate="G$1" pin="VSS@3" pad="23"/>
<connect gate="G$1" pin="VSS@4" pad="34"/>
<connect gate="G$1" pin="VSS@5" pad="39"/>
<connect gate="G$1" pin="VSS@6" pad="51"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="NXP" constant="no"/>
<attribute name="MPN" value="PCA9698DGG" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PCA9616" prefix="U">
<description>PCA9616 - 3-channel differential bus buffer</description>
<gates>
<gate name="A" symbol="PCA9616" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP16">
<connects>
<connect gate="A" pin="!PIDET" pad="5"/>
<connect gate="A" pin="!READY" pad="6"/>
<connect gate="A" pin="DINTM" pad="11"/>
<connect gate="A" pin="DINTP" pad="10"/>
<connect gate="A" pin="DSCLM" pad="12"/>
<connect gate="A" pin="DSCLP" pad="13"/>
<connect gate="A" pin="DSDAM" pad="15"/>
<connect gate="A" pin="DSDAP" pad="14"/>
<connect gate="A" pin="EN" pad="3"/>
<connect gate="A" pin="INT" pad="7"/>
<connect gate="A" pin="SCL" pad="4"/>
<connect gate="A" pin="SDA" pad="2"/>
<connect gate="A" pin="VDD(A)" pad="1"/>
<connect gate="A" pin="VDD(B)" pad="16"/>
<connect gate="A" pin="VDDA_SEL" pad="9"/>
<connect gate="A" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="5566-4">
<description>&lt;b&gt;Mini FIT connector 4 pol&lt;/b&gt;&lt;p&gt;
Source: http://www.molex.com</description>
<wire x1="-4.7" y1="4.5" x2="4.7" y2="4.5" width="0.254" layer="21"/>
<wire x1="4.7" y1="4.5" x2="4.7" y2="-2.2" width="0.254" layer="21"/>
<wire x1="4.7" y1="-3.3" x2="4.7" y2="-4.9" width="0.254" layer="21"/>
<wire x1="4.7" y1="-4.9" x2="-4.7" y2="-4.9" width="0.254" layer="21"/>
<wire x1="-4.7" y1="-4.9" x2="-4.7" y2="4.5" width="0.254" layer="21"/>
<wire x1="-8.2" y1="3.2" x2="-5.4" y2="3.2" width="0.254" layer="21" curve="-114.529547"/>
<wire x1="-8.2" y1="1.4" x2="-5.4" y2="1.4" width="0.254" layer="21" curve="114.529547"/>
<wire x1="-8.2" y1="3.2" x2="-8.2" y2="1.4" width="0.254" layer="21"/>
<wire x1="-5.5" y1="3.3" x2="-4.8" y2="3.3" width="0.254" layer="21"/>
<wire x1="-5.5" y1="1.3" x2="-4.8" y2="1.3" width="0.254" layer="21"/>
<wire x1="5.4" y1="1.4" x2="8.2" y2="1.4" width="0.254" layer="21" curve="114.529547"/>
<wire x1="5.4" y1="3.2" x2="8.2" y2="3.2" width="0.254" layer="21" curve="-114.529547"/>
<wire x1="8.2" y1="1.4" x2="8.2" y2="3.2" width="0.254" layer="21"/>
<wire x1="5.5" y1="1.3" x2="4.8" y2="1.3" width="0.254" layer="21"/>
<wire x1="5.5" y1="3.3" x2="4.8" y2="3.3" width="0.254" layer="21"/>
<wire x1="4.7" y1="-2.2" x2="4.7" y2="-3.3" width="0.254" layer="21" curve="-180"/>
<wire x1="-2.1" y1="6.3" x2="2.1" y2="6.3" width="0.254" layer="27"/>
<wire x1="-2.1" y1="6.3" x2="-2.1" y2="4.6" width="0.254" layer="27"/>
<wire x1="2.1" y1="6.3" x2="2.1" y2="4.6" width="0.254" layer="27"/>
<pad name="1" x="2.1" y="-2.75" drill="1.4" shape="square"/>
<pad name="2" x="-2.1" y="-2.75" drill="1.4" shape="square"/>
<pad name="4" x="-2.1" y="2.75" drill="1.4" shape="square"/>
<pad name="3" x="2.1" y="2.75" drill="1.4" shape="square"/>
<text x="-1.27" y="-6.985" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="6.8" y="2.29" drill="3"/>
<hole x="-6.8" y="2.29" drill="3"/>
</package>
</packages>
<symbols>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5566-4" prefix="X">
<description>&lt;b&gt;Mini FIT connector 4 pol&lt;/b&gt;&lt;p&gt;
Source: http://www.molex.com</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="M" x="0" y="2.54" addlevel="always"/>
<gate name="-3" symbol="M" x="0" y="0" addlevel="always"/>
<gate name="-4" symbol="M" x="0" y="-2.54" addlevel="always"/>
</gates>
<devices>
<device name="" package="5566-4">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U3" library="i2c-driver" deviceset="PCA9698" device=""/>
<part name="U4" library="motor-driver" deviceset="TB62212FNG" device=""/>
<part name="U5" library="motor-driver" deviceset="TB62212FNG" device=""/>
<part name="U6" library="motor-driver" deviceset="TB62212FNG" device=""/>
<part name="I2C" library="connector" deviceset="RJ45*2-V" device=""/>
<part name="+3V3@4" library="supply1" deviceset="+3V3" device=""/>
<part name="GND@4" library="supply1" deviceset="GND" device=""/>
<part name="+3V3@6" library="supply1" deviceset="+3V3" device=""/>
<part name="GND@6" library="supply1" deviceset="GND" device=""/>
<part name="GND@5" library="supply1" deviceset="GND" device=""/>
<part name="GND@8" library="supply1" deviceset="GND" device=""/>
<part name="GND@9" library="supply1" deviceset="GND" device=""/>
<part name="R14" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R15" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R16" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R17" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R18" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R19" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R20" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R21" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R25" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R24" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R23" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="R22" library="rcl" deviceset="R-US_" device="R1206" value="1"/>
<part name="+1V@1" library="1v" deviceset="+1V" device=""/>
<part name="+1V@2" library="1v" deviceset="+1V" device=""/>
<part name="+1V@3" library="1v" deviceset="+1V" device=""/>
<part name="GND@10" library="supply1" deviceset="GND" device=""/>
<part name="GND@11" library="supply1" deviceset="GND" device=""/>
<part name="GND@12" library="supply1" deviceset="GND" device=""/>
<part name="+1V@4" library="1v" deviceset="+1V" device=""/>
<part name="+3V3@8" library="supply1" deviceset="+3V3" device=""/>
<part name="GND@13" library="supply1" deviceset="GND" device=""/>
<part name="R8" library="rcl" deviceset="R-US_" device="R0805" value="22k"/>
<part name="R9" library="rcl" deviceset="R-US_" device="R0805" value="1k"/>
<part name="R10" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="C8" library="rcl" deviceset="C-US" device="C0603" value="220pF"/>
<part name="C11" library="rcl" deviceset="C-US" device="C0603" value="220pF"/>
<part name="C14" library="rcl" deviceset="C-US" device="C0603" value="220pF"/>
<part name="IO1" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO2" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO3" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO4" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO5" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO6" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO7" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO8" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO9" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO10" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO11" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="IO12" library="connector" deviceset="6-1971800" device="SH" value="6-1971800SH"/>
<part name="SW1" library="connector" deviceset="DIP-3-219" device=""/>
<part name="R5" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R6" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R7" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="GND@1" library="supply1" deviceset="GND" device=""/>
<part name="R2" library="rcl" deviceset="R-US_" device="R0805" value="4.7k"/>
<part name="R1" library="rcl" deviceset="R-US_" device="R0805" value="4.7k"/>
<part name="+3V3@2" library="supply1" deviceset="+3V3" device=""/>
<part name="RST" library="connector" deviceset="SW-1825910-7" device=""/>
<part name="GND@14" library="supply1" deviceset="GND" device=""/>
<part name="R3" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="+3V3@10" library="supply1" deviceset="+3V3" device=""/>
<part name="GND@15" library="supply1" deviceset="GND" device=""/>
<part name="Q1" library="connector" deviceset="DMG2305UX" device=""/>
<part name="Q2" library="connector" deviceset="DMG2305UX" device=""/>
<part name="Q3" library="connector" deviceset="DMG2305UX" device=""/>
<part name="GND@16" library="supply1" deviceset="GND" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="PWR" library="con-molex" deviceset="5566-4" device=""/>
<part name="C4" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="C5" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="C7" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="C10" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="C13" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="+3V3@1" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V3@5" library="supply1" deviceset="+3V3" device=""/>
<part name="C3" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="GND@3" library="supply1" deviceset="GND" device=""/>
<part name="GND@17" library="supply1" deviceset="GND" device=""/>
<part name="GND@18" library="supply1" deviceset="GND" device=""/>
<part name="GND@19" library="supply1" deviceset="GND" device=""/>
<part name="GND@20" library="supply1" deviceset="GND" device=""/>
<part name="C9" library="connector" deviceset="C-US" device="0508" value="0.1uF"/>
<part name="C12" library="connector" deviceset="C-US" device="0508" value="0.1uF"/>
<part name="C15" library="connector" deviceset="C-US" device="0508" value="0.1uF"/>
<part name="GND@21" library="supply1" deviceset="GND" device=""/>
<part name="GND@22" library="supply1" deviceset="GND" device=""/>
<part name="GND@23" library="supply1" deviceset="GND" device=""/>
<part name="C6" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="+3V3@7" library="supply1" deviceset="+3V3" device=""/>
<part name="GND@24" library="supply1" deviceset="GND" device=""/>
<part name="VM@11" library="supply1" deviceset="+12V" device=""/>
<part name="GND@7" library="supply1" deviceset="GND" device=""/>
<part name="U2" library="i2c-driver" deviceset="PCA9616" device=""/>
<part name="J1" library="connector" deviceset="0R-JUMP" device=""/>
<part name="R4" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="+3V3@9" library="supply1" deviceset="+3V3" device=""/>
<part name="U1" library="connector" deviceset="PAM2305" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="C1" library="connector" deviceset="C-US" device="0805" value="10uF"/>
<part name="GND@2" library="supply1" deviceset="GND" device=""/>
<part name="L1" library="connector" deviceset="L-US" device="L2012C" value="2.2 uH"/>
<part name="C2" library="connector" deviceset="C-US" device="0805" value="10uF"/>
<part name="+3V3@11" library="supply1" deviceset="+3V3" device=""/>
<part name="VM@1" library="supply1" deviceset="+12V" device=""/>
<part name="VM@2" library="supply1" deviceset="+12V" device=""/>
<part name="R11" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R12" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R13" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="VM@3" library="supply1" deviceset="+12V" device=""/>
<part name="VM@4" library="supply1" deviceset="+12V" device=""/>
<part name="VM@5" library="supply1" deviceset="+12V" device=""/>
<part name="F1" library="connector" deviceset="FUSE" device="1812" value="1.5A"/>
<part name="VM@6" library="supply1" deviceset="+12V" device=""/>
<part name="F2" library="connector" deviceset="FUSE" device="0805" value="500mA"/>
<part name="R26" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R27" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R28" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R29" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R30" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R31" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R32" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R33" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R34" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R35" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R36" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="R37" library="rcl" deviceset="R-US_" device="R0805" value="10k"/>
<part name="C16" library="connector" deviceset="C-US" device="-UCM" value="100uF"/>
<part name="GND@25" library="supply1" deviceset="GND" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="VM@7" library="supply1" deviceset="+12V" device=""/>
<part name="GND@26" library="supply1" deviceset="GND" device=""/>
<part name="C17" library="connector" deviceset="C-US" device="-UCM-6MM" value="100uF"/>
<part name="C18" library="connector" deviceset="C-US" device="0805" value="3.3uF"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="86.36" y="137.16" size="1.778" layer="91">Vdd = 3.3V
Vil(max) = 0.3*Vdd = 0.99V
 = 0.4V @buffer, 0.3*Vdd @micro
Iol(min) = 20mA = 0.02A
 = 0.080A @buffer, 0.008A @micro
tr = 1000ns @ 100kHz
tr = 300ns @ 400kHz
tr = 120ns @ 1MHz
Cb = 400pF @ 100kHz
Cb = 400pF @ 400kHz
Cb = 550pF @ 1MHz

Rp(min) = (Vdd - Vil(max)) / Iol(min)
 = (3.3 - 0.99) / 0.02
 = 2.31 / 0.02
 = 231 / 2
 = 115.5 ohms @gpio
= (3.3 - 0.4) / 0.08
 = 2900 / 80 = 36.25 ohms @buffer
= (3.3 - 0.99) / 0.008
 = 2310 / 8 = 288.75 ohms @micro

Rp(max) = tr / (0.8473 * Cb)
 = 300ns / (0.8473 * 400pF (17) )
 = 300 / (0.8473 * .4 (.02) )
 = 300 / .3389 (.0169)
 = 885 ohms (17.7k)

 = 120ns / (0.8473 * 550pF (17) )
 = 120 / (0.8473 * .55 (.02) )
 = 120 / .466 (.0169)
 = 257.5 ohms (7k)</text>
<text x="12.7" y="137.16" size="1.778" layer="91">Motors sink 80mA, call it 100
  = 1.2A
30mA for driver chips
  = 1.23A

Each sensor board sinks 10-15mA
  = 360mA with all of them on</text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="111.76" y="66.04"/>
<instance part="U4" gate="G$1" x="200.66" y="175.26"/>
<instance part="U5" gate="G$1" x="200.66" y="93.98"/>
<instance part="U6" gate="G$1" x="200.66" y="12.7"/>
<instance part="I2C" gate="A" x="27.94" y="63.5"/>
<instance part="+3V3@4" gate="G$1" x="76.2" y="73.66"/>
<instance part="GND@4" gate="1" x="76.2" y="43.18"/>
<instance part="+3V3@6" gate="G$1" x="78.74" y="152.4"/>
<instance part="GND@6" gate="1" x="58.42" y="124.46"/>
<instance part="GND@5" gate="1" x="177.8" y="205.74"/>
<instance part="GND@8" gate="1" x="177.8" y="124.46"/>
<instance part="GND@9" gate="1" x="177.8" y="43.18"/>
<instance part="R14" gate="G$1" x="175.26" y="198.12"/>
<instance part="R15" gate="G$1" x="175.26" y="185.42"/>
<instance part="R16" gate="G$1" x="175.26" y="172.72"/>
<instance part="R17" gate="G$1" x="175.26" y="160.02"/>
<instance part="R18" gate="G$1" x="175.26" y="116.84"/>
<instance part="R19" gate="G$1" x="175.26" y="104.14"/>
<instance part="R20" gate="G$1" x="175.26" y="91.44"/>
<instance part="R21" gate="G$1" x="175.26" y="78.74"/>
<instance part="R25" gate="G$1" x="175.26" y="-2.54"/>
<instance part="R24" gate="G$1" x="175.26" y="10.16"/>
<instance part="R23" gate="G$1" x="175.26" y="22.86"/>
<instance part="R22" gate="G$1" x="175.26" y="35.56"/>
<instance part="+1V@1" gate="G$1" x="180.34" y="190.5"/>
<instance part="+1V@2" gate="G$1" x="180.34" y="109.22"/>
<instance part="+1V@3" gate="G$1" x="180.34" y="27.94"/>
<instance part="GND@10" gate="1" x="180.34" y="142.24"/>
<instance part="GND@11" gate="1" x="180.34" y="60.96"/>
<instance part="GND@12" gate="1" x="180.34" y="-20.32"/>
<instance part="+1V@4" gate="G$1" x="73.66" y="193.04"/>
<instance part="+3V3@8" gate="G$1" x="60.96" y="200.66"/>
<instance part="GND@13" gate="1" x="60.96" y="152.4"/>
<instance part="R8" gate="G$1" x="60.96" y="190.5" rot="R90"/>
<instance part="R9" gate="G$1" x="60.96" y="177.8" rot="R90"/>
<instance part="R10" gate="G$1" x="60.96" y="162.56" rot="R90"/>
<instance part="C8" gate="G$1" x="167.64" y="144.78"/>
<instance part="C11" gate="G$1" x="167.64" y="63.5"/>
<instance part="C14" gate="G$1" x="167.64" y="-17.78"/>
<instance part="IO1" gate="C" x="243.84" y="200.66"/>
<instance part="IO2" gate="C" x="243.84" y="180.34"/>
<instance part="IO3" gate="C" x="243.84" y="160.02"/>
<instance part="IO4" gate="C" x="243.84" y="139.7"/>
<instance part="IO5" gate="C" x="243.84" y="119.38"/>
<instance part="IO6" gate="C" x="243.84" y="99.06"/>
<instance part="IO7" gate="C" x="243.84" y="78.74"/>
<instance part="IO8" gate="C" x="243.84" y="58.42"/>
<instance part="IO9" gate="C" x="243.84" y="38.1"/>
<instance part="IO10" gate="C" x="243.84" y="17.78"/>
<instance part="IO11" gate="C" x="243.84" y="-2.54"/>
<instance part="IO12" gate="C" x="243.84" y="-22.86"/>
<instance part="SW1" gate="A" x="76.2" y="142.24" rot="R90"/>
<instance part="R5" gate="G$1" x="66.04" y="124.46"/>
<instance part="R6" gate="G$1" x="66.04" y="129.54"/>
<instance part="R7" gate="G$1" x="66.04" y="134.62"/>
<instance part="GND@1" gate="1" x="68.58" y="35.56"/>
<instance part="R2" gate="G$1" x="63.5" y="83.82"/>
<instance part="R1" gate="G$1" x="63.5" y="78.74"/>
<instance part="+3V3@2" gate="G$1" x="58.42" y="88.9"/>
<instance part="RST" gate="G$1" x="63.5" y="10.16"/>
<instance part="GND@14" gate="1" x="55.88" y="5.08"/>
<instance part="R3" gate="G$1" x="73.66" y="15.24" rot="R90"/>
<instance part="+3V3@10" gate="G$1" x="71.12" y="22.86"/>
<instance part="GND@15" gate="1" x="27.94" y="33.02"/>
<instance part="Q1" gate="A" x="223.52" y="203.2"/>
<instance part="Q2" gate="A" x="223.52" y="121.92"/>
<instance part="Q3" gate="A" x="223.52" y="40.64"/>
<instance part="GND@16" gate="1" x="134.62" y="154.94"/>
<instance part="P+1" gate="1" x="43.18" y="73.66"/>
<instance part="PWR" gate="-1" x="139.7" y="172.72"/>
<instance part="PWR" gate="-2" x="139.7" y="170.18"/>
<instance part="PWR" gate="-3" x="139.7" y="167.64"/>
<instance part="PWR" gate="-4" x="139.7" y="165.1"/>
<instance part="C4" gate="C" x="58.42" y="27.94"/>
<instance part="C5" gate="C" x="109.22" y="-5.08"/>
<instance part="C7" gate="C" x="215.9" y="203.2"/>
<instance part="C10" gate="C" x="215.9" y="121.92"/>
<instance part="C13" gate="C" x="215.9" y="40.64"/>
<instance part="+3V3@1" gate="G$1" x="58.42" y="35.56"/>
<instance part="+3V3@5" gate="G$1" x="109.22" y="0"/>
<instance part="C3" gate="C" x="53.34" y="27.94"/>
<instance part="P+4" gate="1" x="53.34" y="35.56"/>
<instance part="GND@3" gate="1" x="55.88" y="20.32"/>
<instance part="GND@17" gate="1" x="109.22" y="-12.7"/>
<instance part="GND@18" gate="1" x="215.9" y="193.04"/>
<instance part="GND@19" gate="1" x="215.9" y="111.76"/>
<instance part="GND@20" gate="1" x="215.9" y="30.48"/>
<instance part="C9" gate="C" x="215.9" y="152.4"/>
<instance part="C12" gate="C" x="215.9" y="71.12"/>
<instance part="C15" gate="C" x="215.9" y="-10.16"/>
<instance part="GND@21" gate="1" x="215.9" y="144.78"/>
<instance part="GND@22" gate="1" x="215.9" y="63.5"/>
<instance part="GND@23" gate="1" x="215.9" y="-17.78"/>
<instance part="C6" gate="C" x="116.84" y="-5.08"/>
<instance part="+3V3@7" gate="G$1" x="116.84" y="0"/>
<instance part="GND@24" gate="1" x="116.84" y="-12.7"/>
<instance part="VM@11" gate="1" x="167.64" y="215.9"/>
<instance part="GND@7" gate="1" x="144.78" y="124.46"/>
<instance part="U2" gate="A" x="55.88" y="55.88" rot="MR0"/>
<instance part="J1" gate="A" x="73.66" y="27.94" rot="MR0"/>
<instance part="R4" gate="G$1" x="134.62" y="-12.7" rot="R90"/>
<instance part="+3V3@9" gate="G$1" x="132.08" y="-5.08"/>
<instance part="U1" gate="G1" x="35.56" y="104.14"/>
<instance part="P+2" gate="1" x="15.24" y="116.84"/>
<instance part="C1" gate="C" x="17.78" y="106.68"/>
<instance part="GND@2" gate="1" x="35.56" y="93.98"/>
<instance part="L1" gate="L" x="55.88" y="109.22" rot="R90"/>
<instance part="C2" gate="C" x="66.04" y="106.68"/>
<instance part="+3V3@11" gate="G$1" x="68.58" y="116.84"/>
<instance part="VM@1" gate="1" x="167.64" y="134.62"/>
<instance part="VM@2" gate="1" x="167.64" y="53.34"/>
<instance part="R11" gate="G$1" x="223.52" y="215.9"/>
<instance part="R12" gate="G$1" x="223.52" y="134.62"/>
<instance part="R13" gate="G$1" x="223.52" y="53.34"/>
<instance part="VM@3" gate="1" x="215.9" y="157.48"/>
<instance part="VM@4" gate="1" x="215.9" y="76.2"/>
<instance part="VM@5" gate="1" x="215.9" y="-5.08"/>
<instance part="F1" gate="A" x="139.7" y="180.34"/>
<instance part="VM@6" gate="1" x="144.78" y="185.42"/>
<instance part="F2" gate="A" x="137.16" y="195.58"/>
<instance part="R26" gate="G$1" x="243.84" y="223.52" rot="R90"/>
<instance part="R27" gate="G$1" x="248.92" y="223.52" rot="R90"/>
<instance part="R28" gate="G$1" x="254" y="223.52" rot="R90"/>
<instance part="R29" gate="G$1" x="259.08" y="223.52" rot="R90"/>
<instance part="R30" gate="G$1" x="264.16" y="223.52" rot="R90"/>
<instance part="R31" gate="G$1" x="269.24" y="223.52" rot="R90"/>
<instance part="R32" gate="G$1" x="274.32" y="223.52" rot="R90"/>
<instance part="R33" gate="G$1" x="279.4" y="223.52" rot="R90"/>
<instance part="R34" gate="G$1" x="284.48" y="223.52" rot="R90"/>
<instance part="R35" gate="G$1" x="289.56" y="223.52" rot="R90"/>
<instance part="R36" gate="G$1" x="294.64" y="223.52" rot="R90"/>
<instance part="R37" gate="G$1" x="299.72" y="223.52" rot="R90"/>
<instance part="C16" gate="C" x="45.72" y="86.36"/>
<instance part="GND@25" gate="1" x="45.72" y="78.74"/>
<instance part="P+3" gate="1" x="45.72" y="93.98"/>
<instance part="VM@7" gate="1" x="152.4" y="226.06"/>
<instance part="GND@26" gate="1" x="152.4" y="205.74"/>
<instance part="C17" gate="C" x="154.94" y="215.9"/>
<instance part="C18" gate="C" x="147.32" y="215.9"/>
</instances>
<busses>
<bus name="OUT_A1-,OUT_A1+,OUT_B1-,OUT_B1+,OUT_C1-,OUT_C1+,OUT_D1-,OUT_D1+">
<segment>
<wire x1="231.14" y1="210.82" x2="231.14" y2="142.24" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="OUT_A2-,OUT_A2+,OUT_B2-,OUT_B2+,OUT_C2-,OUT_C2+,OUT_D2-,OUT_D2+">
<segment>
<wire x1="231.14" y1="129.54" x2="231.14" y2="60.96" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="OUT_A3-,OUT_A3+,OUT_B3-,OUT_B3+,OUT_C3-,OUT_C3+,OUT_D3-,OUT_D3+">
<segment>
<wire x1="231.14" y1="48.26" x2="231.14" y2="-20.32" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="IO0_[0..7],IO1_[0..7],IO2_[0..7],IO3_[0..7],IO4_[0..7],GND,+5V_ATX">
<segment>
<wire x1="236.22" y1="-30.48" x2="236.22" y2="215.9" width="0.762" layer="92"/>
<wire x1="152.4" y1="198.12" x2="152.4" y2="-30.48" width="0.762" layer="92"/>
<wire x1="236.22" y1="-30.48" x2="152.4" y2="-30.48" width="0.762" layer="92"/>
<wire x1="236.22" y1="215.9" x2="302.26" y2="215.9" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="DSDAP,DSDAM,DSCLP,DSCLM,DINTP,DINTM,+5V,GND">
<segment>
<wire x1="38.1" y1="38.1" x2="38.1" y2="88.9" width="0.762" layer="92"/>
<wire x1="38.1" y1="88.9" x2="7.62" y2="88.9" width="0.762" layer="92"/>
<wire x1="7.62" y1="88.9" x2="7.62" y2="40.64" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VDD@1"/>
<pinref part="+3V3@4" gate="G$1" pin="+3V3"/>
<wire x1="81.28" y1="68.58" x2="76.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="76.2" y1="68.58" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="76.2" y1="68.58" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<junction x="76.2" y="68.58"/>
<pinref part="U3" gate="G$1" pin="VDD@2"/>
<wire x1="76.2" y1="66.04" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3@8" gate="G$1" pin="+3V3"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="60.96" y1="198.12" x2="60.96" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="+3V3@2" gate="G$1" pin="+3V3"/>
<wire x1="58.42" y1="83.82" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="58.42" y1="78.74" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
<junction x="58.42" y="83.82"/>
<wire x1="68.58" y1="73.66" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<junction x="58.42" y="78.74"/>
<wire x1="68.58" y1="63.5" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VDD(A)"/>
<wire x1="68.58" y1="68.58" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<junction x="68.58" y="68.58"/>
<pinref part="U2" gate="A" pin="EN"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="+3V3@10" gate="G$1" pin="+3V3"/>
<wire x1="73.66" y1="20.32" x2="71.12" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="C" pin="1"/>
<pinref part="+3V3@1" gate="G$1" pin="+3V3"/>
<wire x1="58.42" y1="30.48" x2="58.42" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="C" pin="1"/>
<pinref part="+3V3@5" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="SW1" gate="A" pin="OUT3"/>
<pinref part="SW1" gate="A" pin="OUT2"/>
<wire x1="78.74" y1="147.32" x2="76.2" y2="147.32" width="0.1524" layer="91"/>
<pinref part="SW1" gate="A" pin="OUT1"/>
<wire x1="76.2" y1="147.32" x2="73.66" y2="147.32" width="0.1524" layer="91"/>
<junction x="76.2" y="147.32"/>
<pinref part="+3V3@6" gate="G$1" pin="+3V3"/>
<wire x1="78.74" y1="147.32" x2="78.74" y2="149.86" width="0.1524" layer="91"/>
<junction x="78.74" y="147.32"/>
</segment>
<segment>
<pinref part="C6" gate="C" pin="1"/>
<pinref part="+3V3@7" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="+3V3@9" gate="G$1" pin="+3V3"/>
<wire x1="132.08" y1="-7.62" x2="134.62" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L1" gate="L" pin="2"/>
<pinref part="U1" gate="G1" pin="VOUT"/>
<wire x1="63.5" y1="109.22" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="63.5" y1="101.6" x2="45.72" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C2" gate="C" pin="1"/>
<wire x1="63.5" y1="109.22" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<junction x="63.5" y="109.22"/>
<wire x1="66.04" y1="109.22" x2="68.58" y2="109.22" width="0.1524" layer="91"/>
<wire x1="68.58" y1="109.22" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<junction x="66.04" y="109.22"/>
<pinref part="+3V3@11" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VSS@6"/>
<pinref part="GND@4" gate="1" pin="GND"/>
<wire x1="81.28" y1="48.26" x2="76.2" y2="48.26" width="0.1524" layer="91"/>
<wire x1="76.2" y1="48.26" x2="76.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="76.2" y1="48.26" x2="76.2" y2="50.8" width="0.1524" layer="91"/>
<junction x="76.2" y="48.26"/>
<pinref part="U3" gate="G$1" pin="VSS@5"/>
<wire x1="76.2" y1="50.8" x2="81.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="76.2" y1="50.8" x2="76.2" y2="53.34" width="0.1524" layer="91"/>
<junction x="76.2" y="50.8"/>
<pinref part="U3" gate="G$1" pin="VSS@4"/>
<wire x1="76.2" y1="53.34" x2="81.28" y2="53.34" width="0.1524" layer="91"/>
<wire x1="76.2" y1="53.34" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<junction x="76.2" y="53.34"/>
<pinref part="U3" gate="G$1" pin="VSS@3"/>
<wire x1="76.2" y1="55.88" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<wire x1="76.2" y1="55.88" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<junction x="76.2" y="55.88"/>
<pinref part="U3" gate="G$1" pin="VSS@2"/>
<wire x1="76.2" y1="58.42" x2="81.28" y2="58.42" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<junction x="76.2" y="58.42"/>
<pinref part="U3" gate="G$1" pin="VSS@1"/>
<wire x1="76.2" y1="60.96" x2="81.28" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="MODE0"/>
<pinref part="GND@5" gate="1" pin="GND"/>
<wire x1="185.42" y1="208.28" x2="180.34" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="MODE1"/>
<wire x1="180.34" y1="208.28" x2="177.8" y2="208.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="205.74" x2="180.34" y2="205.74" width="0.1524" layer="91"/>
<wire x1="180.34" y1="205.74" x2="180.34" y2="208.28" width="0.1524" layer="91"/>
<junction x="180.34" y="208.28"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="MODE0"/>
<pinref part="GND@8" gate="1" pin="GND"/>
<wire x1="185.42" y1="127" x2="180.34" y2="127" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="MODE1"/>
<wire x1="180.34" y1="127" x2="177.8" y2="127" width="0.1524" layer="91"/>
<wire x1="185.42" y1="124.46" x2="180.34" y2="124.46" width="0.1524" layer="91"/>
<wire x1="180.34" y1="124.46" x2="180.34" y2="127" width="0.1524" layer="91"/>
<junction x="180.34" y="127"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="MODE0"/>
<pinref part="GND@9" gate="1" pin="GND"/>
<wire x1="185.42" y1="45.72" x2="180.34" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="MODE1"/>
<wire x1="180.34" y1="45.72" x2="177.8" y2="45.72" width="0.1524" layer="91"/>
<wire x1="185.42" y1="43.18" x2="180.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="43.18" x2="180.34" y2="45.72" width="0.1524" layer="91"/>
<junction x="180.34" y="45.72"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="D_TBLANK_AB"/>
<pinref part="GND@12" gate="1" pin="GND"/>
<wire x1="185.42" y1="-15.24" x2="180.34" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-15.24" x2="180.34" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="D_TBLANK_CD"/>
<wire x1="185.42" y1="-17.78" x2="180.34" y2="-17.78" width="0.1524" layer="91"/>
<junction x="180.34" y="-17.78"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="167.64" y1="-15.24" x2="180.34" y2="-15.24" width="0.1524" layer="91"/>
<junction x="180.34" y="-15.24"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="D_TBLANK_CD"/>
<pinref part="GND@11" gate="1" pin="GND"/>
<wire x1="185.42" y1="63.5" x2="180.34" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="D_TBLANK_AB"/>
<wire x1="185.42" y1="66.04" x2="180.34" y2="66.04" width="0.1524" layer="91"/>
<wire x1="180.34" y1="66.04" x2="180.34" y2="63.5" width="0.1524" layer="91"/>
<junction x="180.34" y="63.5"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="167.64" y1="66.04" x2="180.34" y2="66.04" width="0.1524" layer="91"/>
<junction x="180.34" y="66.04"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="D_TBLANK_CD"/>
<pinref part="GND@10" gate="1" pin="GND"/>
<wire x1="185.42" y1="144.78" x2="180.34" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="D_TBLANK_AB"/>
<wire x1="185.42" y1="147.32" x2="180.34" y2="147.32" width="0.1524" layer="91"/>
<wire x1="180.34" y1="147.32" x2="180.34" y2="144.78" width="0.1524" layer="91"/>
<junction x="180.34" y="144.78"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="167.64" y1="147.32" x2="180.34" y2="147.32" width="0.1524" layer="91"/>
<junction x="180.34" y="147.32"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="GND@13" gate="1" pin="GND"/>
<wire x1="60.96" y1="157.48" x2="60.96" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND@1" gate="1" pin="GND"/>
<wire x1="68.58" y1="40.64" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VSS"/>
</segment>
<segment>
<pinref part="IO1" gate="C" pin="3"/>
<wire x1="243.84" y1="203.2" x2="236.22" y2="203.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO2" gate="C" pin="3"/>
<wire x1="243.84" y1="182.88" x2="236.22" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO3" gate="C" pin="3"/>
<wire x1="243.84" y1="162.56" x2="236.22" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO4" gate="C" pin="3"/>
<wire x1="243.84" y1="142.24" x2="236.22" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO5" gate="C" pin="3"/>
<wire x1="243.84" y1="121.92" x2="236.22" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO6" gate="C" pin="3"/>
<wire x1="243.84" y1="101.6" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO7" gate="C" pin="3"/>
<wire x1="243.84" y1="81.28" x2="236.22" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO8" gate="C" pin="3"/>
<wire x1="243.84" y1="60.96" x2="236.22" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO9" gate="C" pin="3"/>
<wire x1="243.84" y1="40.64" x2="236.22" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO10" gate="C" pin="3"/>
<wire x1="243.84" y1="20.32" x2="236.22" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO11" gate="C" pin="3"/>
<wire x1="243.84" y1="0" x2="236.22" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO12" gate="C" pin="3"/>
<wire x1="243.84" y1="-20.32" x2="236.22" y2="-20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RST" gate="G$1" pin="1@2"/>
<pinref part="GND@14" gate="1" pin="GND"/>
<wire x1="55.88" y1="10.16" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<pinref part="RST" gate="G$1" pin="1@1"/>
<wire x1="55.88" y1="10.16" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<junction x="55.88" y="10.16"/>
</segment>
<segment>
<pinref part="GND@16" gate="1" pin="GND"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="157.48" width="0.1524" layer="91"/>
<pinref part="PWR" gate="-4" pin="S"/>
<wire x1="137.16" y1="165.1" x2="134.62" y2="165.1" width="0.1524" layer="91"/>
<pinref part="PWR" gate="-3" pin="S"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="167.64" width="0.1524" layer="91"/>
<wire x1="134.62" y1="167.64" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
<junction x="134.62" y="165.1"/>
</segment>
<segment>
<pinref part="C4" gate="C" pin="2"/>
<pinref part="C3" gate="C" pin="2"/>
<wire x1="58.42" y1="22.86" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<pinref part="GND@3" gate="1" pin="GND"/>
<wire x1="55.88" y1="22.86" x2="53.34" y2="22.86" width="0.1524" layer="91"/>
<junction x="55.88" y="22.86"/>
</segment>
<segment>
<pinref part="C5" gate="C" pin="2"/>
<pinref part="GND@17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="C" pin="2"/>
<pinref part="GND@18" gate="1" pin="GND"/>
<wire x1="215.9" y1="198.12" x2="215.9" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="C" pin="2"/>
<pinref part="GND@19" gate="1" pin="GND"/>
<wire x1="215.9" y1="116.84" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="C" pin="2"/>
<pinref part="GND@20" gate="1" pin="GND"/>
<wire x1="215.9" y1="35.56" x2="215.9" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="C" pin="2"/>
<pinref part="GND@21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="C" pin="2"/>
<pinref part="GND@22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="C" pin="2"/>
<pinref part="GND@23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="C" pin="2"/>
<pinref part="GND@24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="60.96" y1="124.46" x2="60.96" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="60.96" y1="129.54" x2="60.96" y2="134.62" width="0.1524" layer="91"/>
<junction x="60.96" y="129.54"/>
<pinref part="GND@6" gate="1" pin="GND"/>
<wire x1="60.96" y1="129.54" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<wire x1="58.42" y1="129.54" x2="58.42" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="!OE"/>
<pinref part="GND@7" gate="1" pin="GND"/>
<wire x1="144.78" y1="127" x2="142.24" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G1" pin="GND"/>
<pinref part="GND@2" gate="1" pin="GND"/>
<pinref part="C1" gate="C" pin="2"/>
<wire x1="35.56" y1="96.52" x2="17.78" y2="96.52" width="0.1524" layer="91"/>
<wire x1="17.78" y1="96.52" x2="17.78" y2="101.6" width="0.1524" layer="91"/>
<junction x="35.56" y="96.52"/>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="66.04" y1="101.6" x2="66.04" y2="96.52" width="0.1524" layer="91"/>
<wire x1="66.04" y1="96.52" x2="35.56" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="5"/>
<wire x1="7.62" y1="73.66" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<label x="12.7" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="13"/>
<wire x1="7.62" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<label x="12.7" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GND@15" gate="1" pin="GND"/>
<pinref part="I2C" gate="A" pin="S@3"/>
<wire x1="27.94" y1="35.56" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<pinref part="I2C" gate="A" pin="S@4"/>
<wire x1="27.94" y1="38.1" x2="30.48" y2="38.1" width="0.1524" layer="91"/>
<junction x="27.94" y="38.1"/>
<pinref part="I2C" gate="A" pin="S@5"/>
<wire x1="30.48" y1="38.1" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<junction x="30.48" y="38.1"/>
<pinref part="I2C" gate="A" pin="S@6"/>
<wire x1="33.02" y1="38.1" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
<junction x="33.02" y="38.1"/>
<pinref part="I2C" gate="A" pin="S@2"/>
<wire x1="27.94" y1="38.1" x2="25.4" y2="38.1" width="0.1524" layer="91"/>
<pinref part="I2C" gate="A" pin="S@1"/>
<wire x1="25.4" y1="38.1" x2="22.86" y2="38.1" width="0.1524" layer="91"/>
<junction x="25.4" y="38.1"/>
</segment>
<segment>
<pinref part="C16" gate="C" pin="2"/>
<pinref part="GND@25" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="C" pin="2"/>
<pinref part="GND@26" gate="1" pin="GND"/>
<wire x1="154.94" y1="210.82" x2="152.4" y2="210.82" width="0.1524" layer="91"/>
<wire x1="152.4" y1="210.82" x2="152.4" y2="208.28" width="0.1524" layer="91"/>
<pinref part="C18" gate="C" pin="2"/>
<wire x1="147.32" y1="210.82" x2="152.4" y2="210.82" width="0.1524" layer="91"/>
<junction x="152.4" y="210.82"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="VM"/>
<wire x1="200.66" y1="213.36" x2="167.64" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="170.18" y1="185.42" x2="167.64" y2="185.42" width="0.1524" layer="91"/>
<wire x1="167.64" y1="185.42" x2="167.64" y2="198.12" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="170.18" y1="172.72" x2="167.64" y2="172.72" width="0.1524" layer="91"/>
<wire x1="167.64" y1="172.72" x2="167.64" y2="185.42" width="0.1524" layer="91"/>
<junction x="167.64" y="185.42"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="170.18" y1="160.02" x2="167.64" y2="160.02" width="0.1524" layer="91"/>
<wire x1="167.64" y1="160.02" x2="167.64" y2="172.72" width="0.1524" layer="91"/>
<junction x="167.64" y="172.72"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="170.18" y1="198.12" x2="167.64" y2="198.12" width="0.1524" layer="91"/>
<junction x="167.64" y="198.12"/>
<wire x1="167.64" y1="213.36" x2="167.64" y2="198.12" width="0.1524" layer="91"/>
<pinref part="VM@11" gate="1" pin="+12V"/>
<junction x="167.64" y="213.36"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VM"/>
<wire x1="200.66" y1="132.08" x2="167.64" y2="132.08" width="0.1524" layer="91"/>
<wire x1="167.64" y1="78.74" x2="167.64" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="167.64" y1="91.44" x2="167.64" y2="104.14" width="0.1524" layer="91"/>
<wire x1="167.64" y1="104.14" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
<wire x1="170.18" y1="78.74" x2="167.64" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="170.18" y1="91.44" x2="167.64" y2="91.44" width="0.1524" layer="91"/>
<junction x="167.64" y="91.44"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="170.18" y1="104.14" x2="167.64" y2="104.14" width="0.1524" layer="91"/>
<junction x="167.64" y="104.14"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="170.18" y1="116.84" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
<junction x="167.64" y="116.84"/>
<wire x1="167.64" y1="132.08" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="VM@1" gate="1" pin="+12V"/>
<junction x="167.64" y="132.08"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VM"/>
<wire x1="200.66" y1="50.8" x2="167.64" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="170.18" y1="35.56" x2="167.64" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="170.18" y1="22.86" x2="167.64" y2="22.86" width="0.1524" layer="91"/>
<wire x1="167.64" y1="22.86" x2="167.64" y2="35.56" width="0.1524" layer="91"/>
<junction x="167.64" y="35.56"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="170.18" y1="10.16" x2="167.64" y2="10.16" width="0.1524" layer="91"/>
<wire x1="167.64" y1="10.16" x2="167.64" y2="22.86" width="0.1524" layer="91"/>
<junction x="167.64" y="22.86"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="170.18" y1="-2.54" x2="167.64" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-2.54" x2="167.64" y2="10.16" width="0.1524" layer="91"/>
<junction x="167.64" y="10.16"/>
<wire x1="167.64" y1="50.8" x2="167.64" y2="35.56" width="0.1524" layer="91"/>
<pinref part="VM@2" gate="1" pin="+12V"/>
<junction x="167.64" y="50.8"/>
</segment>
<segment>
<pinref part="C9" gate="C" pin="1"/>
<pinref part="VM@3" gate="1" pin="+12V"/>
</segment>
<segment>
<pinref part="C12" gate="C" pin="1"/>
<pinref part="VM@4" gate="1" pin="+12V"/>
</segment>
<segment>
<pinref part="C15" gate="C" pin="1"/>
<pinref part="VM@5" gate="1" pin="+12V"/>
</segment>
<segment>
<pinref part="F1" gate="A" pin="2"/>
<pinref part="VM@6" gate="1" pin="+12V"/>
<wire x1="144.78" y1="180.34" x2="144.78" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="C" pin="1"/>
<pinref part="VM@7" gate="1" pin="+12V"/>
<wire x1="154.94" y1="218.44" x2="152.4" y2="218.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="218.44" x2="152.4" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C18" gate="C" pin="1"/>
<wire x1="147.32" y1="218.44" x2="152.4" y2="218.44" width="0.1524" layer="91"/>
<junction x="152.4" y="218.44"/>
</segment>
</net>
<net name="RS_D1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="RS_D"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="185.42" y1="160.02" x2="180.34" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_C1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="RS_C"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="185.42" y1="172.72" x2="180.34" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_B1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="RS_B"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="185.42" y1="185.42" x2="180.34" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_A1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="RS_A"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="185.42" y1="198.12" x2="180.34" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_B3" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="RS_B"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="185.42" y1="22.86" x2="180.34" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_C3" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="RS_C"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="185.42" y1="10.16" x2="180.34" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_D3" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="RS_D"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-2.54" x2="180.34" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_A3" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="RS_A"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="185.42" y1="35.56" x2="180.34" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_0"/>
<wire x1="142.24" y1="121.92" x2="152.4" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN1_D"/>
<wire x1="185.42" y1="157.48" x2="152.4" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_1"/>
<wire x1="142.24" y1="119.38" x2="152.4" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN1_C"/>
<wire x1="185.42" y1="170.18" x2="152.4" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_2"/>
<wire x1="142.24" y1="116.84" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN1_B"/>
<wire x1="185.42" y1="182.88" x2="152.4" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_3"/>
<wire x1="142.24" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN1_A"/>
<wire x1="185.42" y1="195.58" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_4"/>
<wire x1="142.24" y1="111.76" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN2_A"/>
<wire x1="185.42" y1="193.04" x2="152.4" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_5"/>
<wire x1="142.24" y1="109.22" x2="152.4" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN2_B"/>
<wire x1="185.42" y1="180.34" x2="152.4" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_6"/>
<wire x1="142.24" y1="106.68" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN2_C"/>
<wire x1="185.42" y1="167.64" x2="152.4" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO0_7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO0_7"/>
<wire x1="142.24" y1="104.14" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN2_D"/>
<wire x1="185.42" y1="154.94" x2="152.4" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_0"/>
<wire x1="142.24" y1="101.6" x2="152.4" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO3" gate="C" pin="4"/>
<wire x1="243.84" y1="160.02" x2="236.22" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="254" y1="218.44" x2="254" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_0"/>
<wire x1="142.24" y1="81.28" x2="152.4" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN1_D"/>
<wire x1="185.42" y1="76.2" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_1"/>
<wire x1="142.24" y1="99.06" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO4" gate="C" pin="4"/>
<wire x1="243.84" y1="139.7" x2="236.22" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="259.08" y1="218.44" x2="259.08" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_2"/>
<wire x1="142.24" y1="96.52" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO1" gate="C" pin="4"/>
<wire x1="243.84" y1="200.66" x2="236.22" y2="200.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="243.84" y1="218.44" x2="243.84" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_3"/>
<wire x1="142.24" y1="93.98" x2="152.4" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO2" gate="C" pin="4"/>
<wire x1="243.84" y1="180.34" x2="236.22" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="248.92" y1="218.44" x2="248.92" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_4"/>
<wire x1="142.24" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO5" gate="C" pin="4"/>
<wire x1="243.84" y1="119.38" x2="236.22" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="264.16" y1="218.44" x2="264.16" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_5"/>
<wire x1="142.24" y1="88.9" x2="152.4" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO6" gate="C" pin="4"/>
<wire x1="243.84" y1="99.06" x2="236.22" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="269.24" y1="218.44" x2="269.24" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_6"/>
<wire x1="142.24" y1="86.36" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO7" gate="C" pin="4"/>
<wire x1="243.84" y1="78.74" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="274.32" y1="218.44" x2="274.32" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO1_7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO1_7"/>
<wire x1="142.24" y1="83.82" x2="152.4" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO8" gate="C" pin="4"/>
<wire x1="243.84" y1="58.42" x2="236.22" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="279.4" y1="218.44" x2="279.4" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_1"/>
<wire x1="142.24" y1="78.74" x2="152.4" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN1_C"/>
<wire x1="185.42" y1="88.9" x2="152.4" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_2"/>
<wire x1="142.24" y1="76.2" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN1_B"/>
<wire x1="185.42" y1="101.6" x2="152.4" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_3"/>
<wire x1="142.24" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN1_A"/>
<wire x1="185.42" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_4"/>
<wire x1="142.24" y1="71.12" x2="152.4" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN2_A"/>
<wire x1="185.42" y1="111.76" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_5"/>
<wire x1="142.24" y1="68.58" x2="152.4" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN2_B"/>
<wire x1="185.42" y1="99.06" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_6"/>
<wire x1="142.24" y1="66.04" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN2_C"/>
<wire x1="185.42" y1="86.36" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO2_7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO2_7"/>
<wire x1="142.24" y1="63.5" x2="152.4" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="IN2_D"/>
<wire x1="185.42" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_A2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="RS_A"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="185.42" y1="116.84" x2="180.34" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_B2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="RS_B"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="185.42" y1="104.14" x2="180.34" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_C2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="RS_C"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="185.42" y1="91.44" x2="180.34" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_D2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="RS_D"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="185.42" y1="78.74" x2="180.34" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+1V" class="0">
<segment>
<pinref part="+1V@2" gate="G$1" pin="+1V"/>
<pinref part="U5" gate="G$1" pin="VREF_A"/>
<wire x1="185.42" y1="109.22" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<wire x1="182.88" y1="109.22" x2="182.88" y2="106.68" width="0.1524" layer="91"/>
<wire x1="182.88" y1="106.68" x2="180.34" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VREF_B"/>
<wire x1="185.42" y1="96.52" x2="182.88" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VREF_C"/>
<wire x1="185.42" y1="83.82" x2="182.88" y2="83.82" width="0.1524" layer="91"/>
<wire x1="182.88" y1="83.82" x2="182.88" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VREF_D"/>
<wire x1="185.42" y1="71.12" x2="182.88" y2="71.12" width="0.1524" layer="91"/>
<wire x1="182.88" y1="71.12" x2="182.88" y2="83.82" width="0.1524" layer="91"/>
<junction x="182.88" y="83.82"/>
<wire x1="182.88" y1="96.52" x2="182.88" y2="106.68" width="0.1524" layer="91"/>
<junction x="182.88" y="96.52"/>
<junction x="182.88" y="106.68"/>
</segment>
<segment>
<pinref part="+1V@3" gate="G$1" pin="+1V"/>
<pinref part="U6" gate="G$1" pin="VREF_A"/>
<wire x1="185.42" y1="27.94" x2="182.88" y2="27.94" width="0.1524" layer="91"/>
<wire x1="182.88" y1="27.94" x2="182.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="182.88" y1="25.4" x2="180.34" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VREF_B"/>
<wire x1="185.42" y1="15.24" x2="182.88" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VREF_C"/>
<wire x1="185.42" y1="2.54" x2="182.88" y2="2.54" width="0.1524" layer="91"/>
<wire x1="182.88" y1="2.54" x2="182.88" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VREF_D"/>
<wire x1="185.42" y1="-10.16" x2="182.88" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-10.16" x2="182.88" y2="2.54" width="0.1524" layer="91"/>
<junction x="182.88" y="2.54"/>
<wire x1="182.88" y1="15.24" x2="182.88" y2="25.4" width="0.1524" layer="91"/>
<junction x="182.88" y="15.24"/>
<junction x="182.88" y="25.4"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="60.96" y1="172.72" x2="60.96" y2="170.18" width="0.1524" layer="91"/>
<pinref part="+1V@4" gate="G$1" pin="+1V"/>
<wire x1="60.96" y1="170.18" x2="60.96" y2="167.64" width="0.1524" layer="91"/>
<wire x1="73.66" y1="190.5" x2="73.66" y2="170.18" width="0.1524" layer="91"/>
<wire x1="73.66" y1="170.18" x2="60.96" y2="170.18" width="0.1524" layer="91"/>
<junction x="60.96" y="170.18"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VREF_A"/>
<wire x1="185.42" y1="190.5" x2="182.88" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VREF_B"/>
<wire x1="185.42" y1="177.8" x2="182.88" y2="177.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VREF_C"/>
<wire x1="185.42" y1="165.1" x2="182.88" y2="165.1" width="0.1524" layer="91"/>
<wire x1="182.88" y1="165.1" x2="182.88" y2="177.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VREF_D"/>
<wire x1="185.42" y1="152.4" x2="182.88" y2="152.4" width="0.1524" layer="91"/>
<wire x1="182.88" y1="152.4" x2="182.88" y2="165.1" width="0.1524" layer="91"/>
<junction x="182.88" y="165.1"/>
<wire x1="182.88" y1="190.5" x2="182.88" y2="187.96" width="0.1524" layer="91"/>
<junction x="182.88" y="177.8"/>
<pinref part="+1V@1" gate="G$1" pin="+1V"/>
<wire x1="182.88" y1="187.96" x2="182.88" y2="177.8" width="0.1524" layer="91"/>
<wire x1="180.34" y1="187.96" x2="182.88" y2="187.96" width="0.1524" layer="91"/>
<junction x="182.88" y="187.96"/>
</segment>
</net>
<net name="+1V1" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="60.96" y1="182.88" x2="60.96" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OSCM3" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OSCM"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-22.86" x2="167.64" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OSCM2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OSCM"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="185.42" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OSCM1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OSCM"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="185.42" y1="139.7" x2="167.64" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_A1-" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_A-"/>
<wire x1="215.9" y1="185.42" x2="231.14" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO1" gate="C" pin="1"/>
<wire x1="243.84" y1="208.28" x2="231.14" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_A1+" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_A+"/>
<wire x1="215.9" y1="182.88" x2="231.14" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO1" gate="C" pin="2"/>
<wire x1="243.84" y1="205.74" x2="231.14" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_B1-" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_B-"/>
<wire x1="215.9" y1="177.8" x2="231.14" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO2" gate="C" pin="1"/>
<wire x1="243.84" y1="187.96" x2="231.14" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_B1+" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_B+"/>
<wire x1="215.9" y1="175.26" x2="231.14" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO2" gate="C" pin="2"/>
<wire x1="243.84" y1="185.42" x2="231.14" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_C1-" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_C-"/>
<wire x1="215.9" y1="170.18" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO3" gate="C" pin="1"/>
<wire x1="243.84" y1="167.64" x2="231.14" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_C1+" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_C+"/>
<wire x1="215.9" y1="167.64" x2="231.14" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO3" gate="C" pin="2"/>
<wire x1="243.84" y1="165.1" x2="231.14" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_D1-" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_D-"/>
<wire x1="215.9" y1="162.56" x2="231.14" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO4" gate="C" pin="1"/>
<wire x1="243.84" y1="147.32" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_D1+" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT_D+"/>
<wire x1="215.9" y1="160.02" x2="231.14" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO4" gate="C" pin="2"/>
<wire x1="243.84" y1="144.78" x2="231.14" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_A2-" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_A-"/>
<wire x1="215.9" y1="104.14" x2="231.14" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO5" gate="C" pin="1"/>
<wire x1="243.84" y1="127" x2="231.14" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_A2+" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_A+"/>
<wire x1="215.9" y1="101.6" x2="231.14" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO5" gate="C" pin="2"/>
<wire x1="243.84" y1="124.46" x2="231.14" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_A3-" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_A-"/>
<wire x1="215.9" y1="22.86" x2="231.14" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO9" gate="C" pin="1"/>
<wire x1="243.84" y1="45.72" x2="231.14" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_B2-" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_B-"/>
<wire x1="215.9" y1="96.52" x2="231.14" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO6" gate="C" pin="1"/>
<wire x1="243.84" y1="106.68" x2="231.14" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_B2+" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_B+"/>
<wire x1="215.9" y1="93.98" x2="231.14" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO6" gate="C" pin="2"/>
<wire x1="243.84" y1="104.14" x2="231.14" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_C2-" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_C-"/>
<wire x1="215.9" y1="88.9" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO7" gate="C" pin="1"/>
<wire x1="243.84" y1="86.36" x2="231.14" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_C2+" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_C+"/>
<wire x1="215.9" y1="86.36" x2="231.14" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO7" gate="C" pin="2"/>
<wire x1="243.84" y1="83.82" x2="231.14" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_D2-" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_D-"/>
<wire x1="215.9" y1="81.28" x2="231.14" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO8" gate="C" pin="1"/>
<wire x1="243.84" y1="66.04" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_D2+" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT_D+"/>
<wire x1="215.9" y1="78.74" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO8" gate="C" pin="2"/>
<wire x1="243.84" y1="63.5" x2="231.14" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_A3+" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_A+"/>
<wire x1="215.9" y1="20.32" x2="231.14" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO9" gate="C" pin="2"/>
<wire x1="243.84" y1="43.18" x2="231.14" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_B3-" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_B-"/>
<wire x1="215.9" y1="15.24" x2="231.14" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO10" gate="C" pin="1"/>
<wire x1="243.84" y1="25.4" x2="231.14" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_B3+" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_B+"/>
<wire x1="215.9" y1="12.7" x2="231.14" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO10" gate="C" pin="2"/>
<wire x1="243.84" y1="22.86" x2="231.14" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_C3-" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_C-"/>
<wire x1="215.9" y1="7.62" x2="231.14" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO11" gate="C" pin="1"/>
<wire x1="243.84" y1="5.08" x2="231.14" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_C3+" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_C+"/>
<wire x1="215.9" y1="5.08" x2="231.14" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO11" gate="C" pin="2"/>
<wire x1="243.84" y1="2.54" x2="231.14" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_D3-" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_D-"/>
<wire x1="215.9" y1="0" x2="231.14" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO12" gate="C" pin="1"/>
<wire x1="243.84" y1="-15.24" x2="231.14" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT_D3+" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="OUT_D+"/>
<wire x1="215.9" y1="-2.54" x2="231.14" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO12" gate="C" pin="2"/>
<wire x1="243.84" y1="-17.78" x2="231.14" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_3"/>
<wire x1="142.24" y1="33.02" x2="152.4" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN1_A"/>
<wire x1="185.42" y1="33.02" x2="152.4" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="71.12" y1="124.46" x2="73.66" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SW1" gate="A" pin="IN1"/>
<wire x1="73.66" y1="137.16" x2="73.66" y2="124.46" width="0.1524" layer="91"/>
<wire x1="73.66" y1="124.46" x2="73.66" y2="119.38" width="0.1524" layer="91"/>
<junction x="73.66" y="124.46"/>
<pinref part="U3" gate="G$1" pin="A0"/>
<wire x1="73.66" y1="119.38" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!RESET"/>
<wire x1="81.28" y1="43.18" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="43.18" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<wire x1="78.74" y1="30.48" x2="78.74" y2="10.16" width="0.1524" layer="91"/>
<wire x1="78.74" y1="10.16" x2="73.66" y2="10.16" width="0.1524" layer="91"/>
<pinref part="RST" gate="G$1" pin="2@2"/>
<junction x="71.12" y="10.16"/>
<pinref part="RST" gate="G$1" pin="2@1"/>
<wire x1="73.66" y1="10.16" x2="71.12" y2="10.16" width="0.1524" layer="91"/>
<wire x1="71.12" y1="10.16" x2="71.12" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<junction x="73.66" y="10.16"/>
<pinref part="J1" gate="A" pin="1"/>
<wire x1="73.66" y1="30.48" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<junction x="78.74" y="30.48"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="SW1" gate="A" pin="IN2"/>
<wire x1="76.2" y1="129.54" x2="76.2" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="71.12" y1="129.54" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
<wire x1="76.2" y1="129.54" x2="76.2" y2="114.3" width="0.1524" layer="91"/>
<junction x="76.2" y="129.54"/>
<pinref part="U3" gate="G$1" pin="A1"/>
<wire x1="76.2" y1="114.3" x2="81.28" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SDA"/>
<wire x1="73.66" y1="91.44" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<wire x1="73.66" y1="91.44" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="73.66" y1="83.82" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
<wire x1="68.58" y1="83.82" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<junction x="73.66" y="83.82"/>
<pinref part="U2" gate="A" pin="SDA"/>
<wire x1="73.66" y1="58.42" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SCL"/>
<wire x1="71.12" y1="96.52" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="71.12" y1="55.88" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="71.12" y1="78.74" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<wire x1="68.58" y1="78.74" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<junction x="71.12" y="78.74"/>
<pinref part="U2" gate="A" pin="SCL"/>
<wire x1="71.12" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_5"/>
<wire x1="142.24" y1="27.94" x2="152.4" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN2_B"/>
<wire x1="185.42" y1="17.78" x2="152.4" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_6"/>
<wire x1="142.24" y1="25.4" x2="152.4" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN2_C"/>
<wire x1="185.42" y1="5.08" x2="152.4" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_7"/>
<wire x1="142.24" y1="22.86" x2="152.4" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN2_D"/>
<wire x1="185.42" y1="-7.62" x2="152.4" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V_HF_1" class="0">
<segment>
<pinref part="Q1" gate="A" pin="D"/>
<pinref part="IO1" gate="C" pin="5"/>
<wire x1="223.52" y1="198.12" x2="243.84" y2="198.12" width="0.1524" layer="91"/>
<wire x1="243.84" y1="198.12" x2="248.92" y2="198.12" width="0.1524" layer="91"/>
<wire x1="248.92" y1="198.12" x2="248.92" y2="177.8" width="0.1524" layer="91"/>
<junction x="243.84" y="198.12"/>
<pinref part="IO2" gate="C" pin="5"/>
<wire x1="248.92" y1="177.8" x2="243.84" y2="177.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="177.8" x2="248.92" y2="157.48" width="0.1524" layer="91"/>
<junction x="248.92" y="177.8"/>
<pinref part="IO3" gate="C" pin="5"/>
<wire x1="248.92" y1="157.48" x2="243.84" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IO4" gate="C" pin="5"/>
<wire x1="248.92" y1="157.48" x2="248.92" y2="137.16" width="0.1524" layer="91"/>
<wire x1="248.92" y1="137.16" x2="243.84" y2="137.16" width="0.1524" layer="91"/>
<junction x="248.92" y="157.48"/>
</segment>
</net>
<net name="+5V_HF_3" class="0">
<segment>
<pinref part="Q3" gate="A" pin="D"/>
<pinref part="IO9" gate="C" pin="5"/>
<wire x1="223.52" y1="35.56" x2="243.84" y2="35.56" width="0.1524" layer="91"/>
<wire x1="243.84" y1="35.56" x2="248.92" y2="35.56" width="0.1524" layer="91"/>
<junction x="243.84" y="35.56"/>
<pinref part="IO10" gate="C" pin="5"/>
<wire x1="248.92" y1="35.56" x2="248.92" y2="15.24" width="0.1524" layer="91"/>
<wire x1="248.92" y1="15.24" x2="243.84" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IO11" gate="C" pin="5"/>
<wire x1="248.92" y1="15.24" x2="248.92" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-5.08" x2="243.84" y2="-5.08" width="0.1524" layer="91"/>
<junction x="248.92" y="15.24"/>
<pinref part="IO12" gate="C" pin="5"/>
<wire x1="248.92" y1="-5.08" x2="248.92" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-25.4" x2="243.84" y2="-25.4" width="0.1524" layer="91"/>
<junction x="248.92" y="-5.08"/>
</segment>
</net>
<net name="+5V_HF_2" class="0">
<segment>
<pinref part="Q2" gate="A" pin="D"/>
<pinref part="IO5" gate="C" pin="5"/>
<wire x1="223.52" y1="116.84" x2="243.84" y2="116.84" width="0.1524" layer="91"/>
<wire x1="243.84" y1="116.84" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
<junction x="243.84" y="116.84"/>
<pinref part="IO6" gate="C" pin="5"/>
<wire x1="248.92" y1="116.84" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="248.92" y1="96.52" x2="243.84" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IO7" gate="C" pin="5"/>
<wire x1="248.92" y1="96.52" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<wire x1="248.92" y1="76.2" x2="243.84" y2="76.2" width="0.1524" layer="91"/>
<junction x="248.92" y="96.52"/>
<pinref part="IO8" gate="C" pin="5"/>
<wire x1="248.92" y1="76.2" x2="248.92" y2="55.88" width="0.1524" layer="91"/>
<wire x1="248.92" y1="55.88" x2="243.84" y2="55.88" width="0.1524" layer="91"/>
<junction x="248.92" y="76.2"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="C3" gate="C" pin="1"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="53.34" y1="33.02" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VDDA_SEL"/>
<pinref part="U2" gate="A" pin="VDD(B)"/>
<wire x1="43.18" y1="63.5" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="43.18" y1="68.58" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
<junction x="43.18" y="68.58"/>
</segment>
<segment>
<pinref part="U1" gate="G1" pin="VIN"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="25.4" y1="109.22" x2="17.78" y2="109.22" width="0.1524" layer="91"/>
<wire x1="17.78" y1="109.22" x2="15.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="15.24" y1="109.22" x2="15.24" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U1" gate="G1" pin="EN"/>
<wire x1="25.4" y1="101.6" x2="25.4" y2="109.22" width="0.1524" layer="91"/>
<junction x="25.4" y="109.22"/>
<pinref part="C1" gate="C" pin="1"/>
<junction x="17.78" y="109.22"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="4"/>
<wire x1="7.62" y1="76.2" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<label x="12.7" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="12"/>
<wire x1="7.62" y1="53.34" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<label x="12.7" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C16" gate="C" pin="1"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="45.72" y1="91.44" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="INT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!INT"/>
<wire x1="81.28" y1="-17.78" x2="134.62" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-17.78" x2="142.24" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-17.78" x2="142.24" y2="15.24" width="0.1524" layer="91"/>
<pinref part="J1" gate="A" pin="3"/>
<wire x1="73.66" y1="25.4" x2="81.28" y2="25.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="25.4" x2="81.28" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<junction x="134.62" y="-17.78"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="SW1" gate="A" pin="IN3"/>
<wire x1="78.74" y1="134.62" x2="71.12" y2="134.62" width="0.1524" layer="91"/>
<wire x1="78.74" y1="134.62" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
<wire x1="78.74" y1="134.62" x2="78.74" y2="109.22" width="0.1524" layer="91"/>
<junction x="78.74" y="134.62"/>
<pinref part="U3" gate="G$1" pin="A2"/>
<wire x1="78.74" y1="109.22" x2="81.28" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO3_0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_0"/>
<wire x1="142.24" y1="60.96" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="A" pin="G"/>
<wire x1="218.44" y1="205.74" x2="218.44" y2="193.04" width="0.1524" layer="91"/>
<wire x1="218.44" y1="193.04" x2="236.22" y2="193.04" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="218.44" y1="205.74" x2="218.44" y2="215.9" width="0.1524" layer="91"/>
<junction x="218.44" y="205.74"/>
</segment>
</net>
<net name="IO3_1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_1"/>
<wire x1="142.24" y1="58.42" x2="152.4" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="A" pin="G"/>
<wire x1="218.44" y1="124.46" x2="218.44" y2="111.76" width="0.1524" layer="91"/>
<wire x1="218.44" y1="111.76" x2="236.22" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="218.44" y1="124.46" x2="218.44" y2="134.62" width="0.1524" layer="91"/>
<junction x="218.44" y="124.46"/>
</segment>
</net>
<net name="IO3_2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_2"/>
<wire x1="142.24" y1="55.88" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q3" gate="A" pin="G"/>
<wire x1="218.44" y1="43.18" x2="218.44" y2="30.48" width="0.1524" layer="91"/>
<wire x1="218.44" y1="30.48" x2="236.22" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="218.44" y1="43.18" x2="218.44" y2="53.34" width="0.1524" layer="91"/>
<junction x="218.44" y="43.18"/>
</segment>
</net>
<net name="IO3_3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_3"/>
<wire x1="142.24" y1="53.34" x2="152.4" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO3_4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_4"/>
<wire x1="142.24" y1="50.8" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO9" gate="C" pin="4"/>
<wire x1="243.84" y1="38.1" x2="236.22" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="284.48" y1="218.44" x2="284.48" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO3_5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_5"/>
<wire x1="142.24" y1="48.26" x2="152.4" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO10" gate="C" pin="4"/>
<wire x1="243.84" y1="17.78" x2="236.22" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="289.56" y1="218.44" x2="289.56" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO3_6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_6"/>
<wire x1="142.24" y1="45.72" x2="152.4" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO11" gate="C" pin="4"/>
<wire x1="243.84" y1="-2.54" x2="236.22" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="294.64" y1="218.44" x2="294.64" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO3_7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO3_7"/>
<wire x1="142.24" y1="43.18" x2="152.4" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO12" gate="C" pin="4"/>
<wire x1="243.84" y1="-22.86" x2="236.22" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="299.72" y1="218.44" x2="299.72" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_0"/>
<wire x1="142.24" y1="40.64" x2="152.4" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN1_D"/>
<wire x1="185.42" y1="-5.08" x2="152.4" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_1"/>
<wire x1="142.24" y1="38.1" x2="152.4" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN1_C"/>
<wire x1="185.42" y1="7.62" x2="152.4" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_2"/>
<wire x1="142.24" y1="35.56" x2="152.4" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN1_B"/>
<wire x1="185.42" y1="20.32" x2="152.4" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="INTRST" class="0">
<segment>
<pinref part="J1" gate="A" pin="2"/>
<wire x1="71.12" y1="27.94" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="INT"/>
<wire x1="71.12" y1="53.34" x2="68.58" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SW" class="0">
<segment>
<pinref part="U1" gate="G1" pin="SW"/>
<pinref part="L1" gate="L" pin="1"/>
<wire x1="45.72" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSDAM" class="0">
<segment>
<pinref part="I2C" gate="A" pin="1"/>
<wire x1="20.32" y1="83.82" x2="7.62" y2="83.82" width="0.1524" layer="91"/>
<label x="10.16" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="9"/>
<wire x1="20.32" y1="60.96" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<label x="10.16" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="DSDAM"/>
<wire x1="43.18" y1="55.88" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSDAP" class="0">
<segment>
<pinref part="I2C" gate="A" pin="2"/>
<wire x1="20.32" y1="81.28" x2="7.62" y2="81.28" width="0.1524" layer="91"/>
<label x="10.16" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="10"/>
<wire x1="20.32" y1="58.42" x2="7.62" y2="58.42" width="0.1524" layer="91"/>
<label x="10.16" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="DSDAP"/>
<wire x1="43.18" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSCLM" class="0">
<segment>
<pinref part="I2C" gate="A" pin="6"/>
<wire x1="20.32" y1="71.12" x2="7.62" y2="71.12" width="0.1524" layer="91"/>
<label x="10.16" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="14"/>
<wire x1="20.32" y1="48.26" x2="7.62" y2="48.26" width="0.1524" layer="91"/>
<label x="10.16" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="DSCLM"/>
<wire x1="43.18" y1="48.26" x2="38.1" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSCLP" class="0">
<segment>
<pinref part="I2C" gate="A" pin="3"/>
<wire x1="20.32" y1="78.74" x2="7.62" y2="78.74" width="0.1524" layer="91"/>
<label x="10.16" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="11"/>
<wire x1="20.32" y1="55.88" x2="7.62" y2="55.88" width="0.1524" layer="91"/>
<label x="10.16" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="DSCLP"/>
<wire x1="43.18" y1="50.8" x2="38.1" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DINTM" class="0">
<segment>
<pinref part="U2" gate="A" pin="DINTM"/>
<wire x1="43.18" y1="40.64" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="7"/>
<wire x1="20.32" y1="68.58" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
<label x="10.16" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="15"/>
<wire x1="20.32" y1="45.72" x2="7.62" y2="45.72" width="0.1524" layer="91"/>
<label x="10.16" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="DINTP" class="0">
<segment>
<pinref part="U2" gate="A" pin="DINTP"/>
<wire x1="43.18" y1="43.18" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="8"/>
<wire x1="20.32" y1="66.04" x2="7.62" y2="66.04" width="0.1524" layer="91"/>
<label x="10.16" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C" gate="A" pin="16"/>
<wire x1="20.32" y1="43.18" x2="7.62" y2="43.18" width="0.1524" layer="91"/>
<label x="10.16" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V_ATX" class="0">
<segment>
<pinref part="Q2" gate="A" pin="S"/>
<wire x1="236.22" y1="132.08" x2="228.6" y2="132.08" width="0.1524" layer="91"/>
<wire x1="228.6" y1="132.08" x2="223.52" y2="132.08" width="0.1524" layer="91"/>
<wire x1="223.52" y1="132.08" x2="223.52" y2="127" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="228.6" y1="132.08" x2="228.6" y2="134.62" width="0.1524" layer="91"/>
<junction x="228.6" y="132.08"/>
<label x="228.6" y="134.62" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IO5" gate="C" pin="6"/>
<wire x1="243.84" y1="114.3" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO6" gate="C" pin="6"/>
<wire x1="243.84" y1="93.98" x2="236.22" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO7" gate="C" pin="6"/>
<wire x1="243.84" y1="73.66" x2="236.22" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO8" gate="C" pin="6"/>
<wire x1="243.84" y1="53.34" x2="236.22" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="A" pin="S"/>
<wire x1="236.22" y1="213.36" x2="233.68" y2="213.36" width="0.1524" layer="91"/>
<wire x1="233.68" y1="213.36" x2="228.6" y2="213.36" width="0.1524" layer="91"/>
<wire x1="228.6" y1="213.36" x2="223.52" y2="213.36" width="0.1524" layer="91"/>
<wire x1="223.52" y1="213.36" x2="223.52" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="228.6" y1="213.36" x2="228.6" y2="215.9" width="0.1524" layer="91"/>
<junction x="228.6" y="213.36"/>
<label x="228.6" y="215.9" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="233.68" y1="213.36" x2="233.68" y2="228.6" width="0.1524" layer="91"/>
<wire x1="233.68" y1="228.6" x2="243.84" y2="228.6" width="0.1524" layer="91"/>
<junction x="233.68" y="213.36"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="243.84" y1="228.6" x2="248.92" y2="228.6" width="0.1524" layer="91"/>
<junction x="243.84" y="228.6"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="248.92" y1="228.6" x2="254" y2="228.6" width="0.1524" layer="91"/>
<junction x="248.92" y="228.6"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="254" y1="228.6" x2="259.08" y2="228.6" width="0.1524" layer="91"/>
<junction x="254" y="228.6"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="259.08" y1="228.6" x2="264.16" y2="228.6" width="0.1524" layer="91"/>
<junction x="259.08" y="228.6"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="264.16" y1="228.6" x2="269.24" y2="228.6" width="0.1524" layer="91"/>
<junction x="264.16" y="228.6"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="269.24" y1="228.6" x2="274.32" y2="228.6" width="0.1524" layer="91"/>
<junction x="269.24" y="228.6"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="274.32" y1="228.6" x2="279.4" y2="228.6" width="0.1524" layer="91"/>
<junction x="274.32" y="228.6"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="279.4" y1="228.6" x2="284.48" y2="228.6" width="0.1524" layer="91"/>
<junction x="279.4" y="228.6"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="284.48" y1="228.6" x2="289.56" y2="228.6" width="0.1524" layer="91"/>
<junction x="284.48" y="228.6"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="289.56" y1="228.6" x2="294.64" y2="228.6" width="0.1524" layer="91"/>
<junction x="289.56" y="228.6"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="294.64" y1="228.6" x2="299.72" y2="228.6" width="0.1524" layer="91"/>
<junction x="294.64" y="228.6"/>
</segment>
<segment>
<pinref part="IO1" gate="C" pin="6"/>
<wire x1="243.84" y1="195.58" x2="236.22" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO2" gate="C" pin="6"/>
<wire x1="243.84" y1="175.26" x2="236.22" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO3" gate="C" pin="6"/>
<wire x1="243.84" y1="154.94" x2="236.22" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO4" gate="C" pin="6"/>
<wire x1="243.84" y1="134.62" x2="236.22" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q3" gate="A" pin="S"/>
<wire x1="236.22" y1="50.8" x2="228.6" y2="50.8" width="0.1524" layer="91"/>
<wire x1="228.6" y1="50.8" x2="223.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="223.52" y1="50.8" x2="223.52" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="228.6" y1="50.8" x2="228.6" y2="53.34" width="0.1524" layer="91"/>
<junction x="228.6" y="50.8"/>
<label x="228.6" y="53.34" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IO9" gate="C" pin="6"/>
<wire x1="243.84" y1="33.02" x2="236.22" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO10" gate="C" pin="6"/>
<wire x1="243.84" y1="12.7" x2="236.22" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO11" gate="C" pin="6"/>
<wire x1="243.84" y1="-7.62" x2="236.22" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IO12" gate="C" pin="6"/>
<wire x1="243.84" y1="-27.94" x2="236.22" y2="-27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F2" gate="A" pin="2"/>
<wire x1="142.24" y1="195.58" x2="142.24" y2="200.66" width="0.1524" layer="91"/>
<label x="142.24" y="200.66" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="MODE2_1" class="0">
<segment>
<pinref part="C7" gate="C" pin="1"/>
<pinref part="U4" gate="G$1" pin="+5V"/>
<wire x1="215.9" y1="205.74" x2="215.9" y2="208.28" width="0.1524" layer="91"/>
<junction x="215.9" y="208.28"/>
<pinref part="U4" gate="G$1" pin="MODE2"/>
<wire x1="185.42" y1="203.2" x2="172.72" y2="203.2" width="0.1524" layer="91"/>
<wire x1="172.72" y1="203.2" x2="172.72" y2="218.44" width="0.1524" layer="91"/>
<wire x1="172.72" y1="218.44" x2="215.9" y2="218.44" width="0.1524" layer="91"/>
<wire x1="215.9" y1="218.44" x2="215.9" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MODE2_2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="+5V"/>
<pinref part="U5" gate="G$1" pin="MODE2"/>
<wire x1="185.42" y1="121.92" x2="172.72" y2="121.92" width="0.1524" layer="91"/>
<wire x1="172.72" y1="121.92" x2="172.72" y2="134.62" width="0.1524" layer="91"/>
<wire x1="172.72" y1="134.62" x2="215.9" y2="134.62" width="0.1524" layer="91"/>
<wire x1="215.9" y1="134.62" x2="215.9" y2="127" width="0.1524" layer="91"/>
<junction x="215.9" y="127"/>
<pinref part="C10" gate="C" pin="1"/>
<wire x1="215.9" y1="124.46" x2="215.9" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MODE2_3" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="+5V"/>
<pinref part="U6" gate="G$1" pin="MODE2"/>
<wire x1="185.42" y1="40.64" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<wire x1="172.72" y1="40.64" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<wire x1="172.72" y1="53.34" x2="215.9" y2="53.34" width="0.1524" layer="91"/>
<wire x1="215.9" y1="53.34" x2="215.9" y2="45.72" width="0.1524" layer="91"/>
<junction x="215.9" y="45.72"/>
<pinref part="C13" gate="C" pin="1"/>
<wire x1="215.9" y1="43.18" x2="215.9" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO4_4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IO4_4"/>
<wire x1="142.24" y1="30.48" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="IN2_A"/>
<wire x1="185.42" y1="30.48" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ATX12V" class="0">
<segment>
<wire x1="134.62" y1="172.72" x2="134.62" y2="180.34" width="0.1524" layer="91"/>
<pinref part="PWR" gate="-1" pin="S"/>
<wire x1="137.16" y1="172.72" x2="134.62" y2="172.72" width="0.1524" layer="91"/>
<pinref part="F1" gate="A" pin="1"/>
</segment>
</net>
<net name="ATX5V" class="0">
<segment>
<pinref part="PWR" gate="-2" pin="S"/>
<wire x1="137.16" y1="170.18" x2="129.54" y2="170.18" width="0.1524" layer="91"/>
<wire x1="129.54" y1="170.18" x2="129.54" y2="195.58" width="0.1524" layer="91"/>
<pinref part="F2" gate="A" pin="1"/>
<wire x1="129.54" y1="195.58" x2="132.08" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!PIDET" class="0">
<segment>
<pinref part="U2" gate="A" pin="!PIDET"/>
<wire x1="68.58" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!READY" class="0">
<segment>
<pinref part="U2" gate="A" pin="!READY"/>
<wire x1="68.58" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,81.28,68.58,U3,VDD,+3V3,,,"/>
<approved hash="104,1,81.28,60.96,U3,VSS,GND,,,"/>
<approved hash="104,1,81.28,66.04,U3,VDD,+3V3,,,"/>
<approved hash="104,1,81.28,58.42,U3,VSS,GND,,,"/>
<approved hash="104,1,81.28,55.88,U3,VSS,GND,,,"/>
<approved hash="104,1,81.28,53.34,U3,VSS,GND,,,"/>
<approved hash="104,1,81.28,50.8,U3,VSS,GND,,,"/>
<approved hash="104,1,81.28,48.26,U3,VSS,GND,,,"/>
<approved hash="104,1,200.66,213.36,U4,VM,+12V,,,"/>
<approved hash="104,1,200.66,132.08,U5,VM,+12V,,,"/>
<approved hash="104,1,200.66,50.8,U6,VM,+12V,,,"/>
<approved hash="104,1,68.58,68.58,U2,VDD(A),+3V3,,,"/>
<approved hash="104,1,68.58,40.64,U2,VSS,GND,,,"/>
<approved hash="104,1,43.18,68.58,U2,VDD(B),+5V,,,"/>
<approved hash="104,1,25.4,109.22,U1,VIN,+5V,,,"/>
<approved hash="106,1,68.58,48.26,!PIDET,,,,,"/>
<approved hash="106,1,68.58,45.72,!READY,,,,,"/>
<approved hash="106,1,142.24,53.34,IO3_3,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>

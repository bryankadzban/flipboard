<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="9" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="connector">
<description>&lt;b&gt;Various connectors&lt;/b&gt;&lt;p&gt;
 &lt;author&gt;Created by bryan@kadzban.net&lt;/author&gt;</description>
<packages>
<package name="1X03-4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;, 100 mil, 1 blank space between pins</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<wire x1="-2.54" y1="0.635" x2="-3.175" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.127" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.127" layer="21"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<pad name="3" x="3.81" y="0" drill="1" shape="octagon" rot="R90"/>
</package>
<package name="1X03-22">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<text x="-2.54" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
</package>
<package name="SHERLOCK-3-RA">
<description>Molex Sherlock family 3-pin board connector; right-angle</description>
<pad name="1" x="-2" y="0" drill="0.8" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.8" rot="R180"/>
<pad name="3" x="2" y="0" drill="0.8" rot="R180"/>
<wire x1="4.15" y1="-2" x2="3.15" y2="-2" width="0.127" layer="21"/>
<wire x1="-3.15" y1="-2" x2="-4.15" y2="-2" width="0.127" layer="21"/>
<wire x1="4.15" y1="-2" x2="4.15" y2="8.5" width="0.127" layer="21"/>
<wire x1="4.15" y1="8.5" x2="2.9" y2="8.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="8" x2="2.4" y2="7.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="7.5" x2="-2.4" y2="7.5" width="0.127" layer="21"/>
<wire x1="-2.4" y1="7.5" x2="-2.4" y2="8" width="0.127" layer="21"/>
<wire x1="-2.9" y1="8.5" x2="-4.15" y2="8.5" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-2" x2="-4.15" y2="8.5" width="0.127" layer="21"/>
<wire x1="4.15" y1="-2" x2="-4.15" y2="-2" width="0.127" layer="39"/>
<wire x1="-4.85" y1="0" x2="-4.85" y2="6.8" width="0.127" layer="39"/>
<wire x1="-4.15" y1="8.5" x2="4.15" y2="8.5" width="0.127" layer="39"/>
<wire x1="4.85" y1="6.8" x2="4.85" y2="0" width="0.127" layer="39"/>
<wire x1="3.6" y1="1.5" x2="3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3" y1="1.5" x2="3.3" y2="1" width="0.127" layer="21"/>
<wire x1="3.3" y1="1" x2="3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.15" y1="-2" x2="-3.15" y2="0" width="0.127" layer="21"/>
<wire x1="-3.15" y1="0" x2="-2.8" y2="0" width="0.127" layer="21"/>
<wire x1="-2.8" y1="0" x2="-1.2" y2="0" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0" x2="-0.8" y2="0" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="0.8" y1="0" x2="1.2" y2="0" width="0.127" layer="21"/>
<wire x1="1.2" y1="0" x2="2.8" y2="0" width="0.127" layer="51"/>
<wire x1="2.8" y1="0" x2="3.15" y2="0" width="0.127" layer="21"/>
<wire x1="3.15" y1="0" x2="3.15" y2="-2" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-2" x2="-4.15" y2="0" width="0.127" layer="39"/>
<wire x1="-4.15" y1="0" x2="-4.85" y2="0" width="0.127" layer="39"/>
<wire x1="4.15" y1="-2" x2="4.15" y2="0" width="0.127" layer="39"/>
<wire x1="4.15" y1="0" x2="4.85" y2="0" width="0.127" layer="39"/>
<wire x1="-2.4" y1="8" x2="-2.9" y2="8.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="8" x2="2.9" y2="8.5" width="0.127" layer="21"/>
<wire x1="-4.85" y1="6.8" x2="-4.15" y2="6.8" width="0.127" layer="39"/>
<wire x1="-4.15" y1="6.8" x2="-4.15" y2="8.5" width="0.127" layer="39"/>
<wire x1="4.15" y1="6.8" x2="4.85" y2="6.8" width="0.127" layer="39"/>
<wire x1="4.15" y1="6.8" x2="4.15" y2="8.5" width="0.127" layer="39"/>
</package>
<package name="1X03-SM">
<description>&lt;b&gt;3 WIRE HEADER&lt;/b&gt;, 70mil, 0 blank spaces between pins</description>
<pad name="1" x="-1.778" y="0" drill="0.8" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.8" rot="R90"/>
<text x="-2.6162" y="0.8128" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<pad name="3" x="1.778" y="0" drill="0.8" rot="R90"/>
<text x="-4" y="-0.3" size="0.5" layer="21" rot="SR0">+5V</text>
<text x="2.8" y="-0.3" size="0.5" layer="21" rot="SR0">GND</text>
</package>
<package name="1X03-SM-3.3V">
<description>&lt;b&gt;3 WIRE HEADER&lt;/b&gt;, 70mil, 0 blank spaces between pins</description>
<pad name="1" x="-1.778" y="0" drill="0.8" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.8" rot="R90"/>
<text x="-2.6162" y="0.8128" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<pad name="3" x="1.778" y="0" drill="0.8" rot="R90"/>
<text x="-4.6" y="-0.3" size="0.5" layer="21" rot="SR0">+3.3V</text>
<text x="2.8" y="-0.3" size="0.5" layer="21" rot="SR0">GND</text>
</package>
<package name="TO-92">
<description>https://pdfserv.maximintegrated.com/package_dwgs/21-0248.PDF</description>
<pad name="2" x="0" y="0" drill="0.75" shape="octagon"/>
<pad name="1" x="-1.524" y="0" drill="0.75" shape="octagon"/>
<pad name="3" x="1.524" y="0" drill="0.75" shape="octagon"/>
<wire x1="-2.413" y1="0" x2="2.413" y2="0" width="0.127" layer="21" curve="-180"/>
<wire x1="-2.286" y1="-1.524" x2="2.286" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-2.30631875" y1="-1.524" x2="-2.413" y2="0" width="0.127" layer="21"/>
<wire x1="2.30631875" y1="-1.524" x2="2.413" y2="0" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="1X03">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="MAX31820">
<description>Maxim MAX31820 one-wire temperature sensor, active power</description>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="+3V3" x="0" y="10.16" visible="pin" length="short" direction="pwr" rot="R270"/>
<pin name="GND" x="0" y="-10.16" visible="pin" length="short" direction="pwr" rot="R90"/>
<pin name="DQ" x="7.62" y="0" visible="pin" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1X03" prefix="JP">
<description>&lt;b&gt;3 wire-connection pads&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="1X03" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03-4">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SQ" package="1X03-22">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SHERLOCK" package="SHERLOCK-3-RA">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SM" package="1X03-SM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SM-3V3" package="1X03-SM-3.3V">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAX31820">
<description>Maxim MAX31820 temperature sensor, with non-parasite power</description>
<gates>
<gate name="A" symbol="MAX31820" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO-92">
<connects>
<connect gate="A" pin="+3V3" pad="3"/>
<connect gate="A" pin="DQ" pad="2"/>
<connect gate="A" pin="GND" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IN" library="connector" deviceset="1X03" device="-SM-3V3"/>
<part name="OUT" library="connector" deviceset="1X03" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="U$1" library="connector" deviceset="MAX31820" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IN" gate="A" x="7.62" y="55.88"/>
<instance part="OUT" gate="A" x="63.5" y="55.88"/>
<instance part="+3V1" gate="G$1" x="5.08" y="71.12"/>
<instance part="+3V2" gate="G$1" x="60.96" y="71.12"/>
<instance part="GND1" gate="1" x="5.08" y="38.1"/>
<instance part="GND2" gate="1" x="60.96" y="38.1"/>
<instance part="U$1" gate="A" x="30.48" y="66.04" rot="R270"/>
<instance part="GND3" gate="1" x="17.78" y="60.96"/>
<instance part="+3V3" gate="G$1" x="43.18" y="71.12"/>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="IN" gate="A" pin="1"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="5.08" y1="58.42" x2="5.08" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OUT" gate="A" pin="1"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="60.96" y1="58.42" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="A" pin="+3V3"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="40.64" y1="66.04" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<wire x1="43.18" y1="66.04" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IN" gate="A" pin="3"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="5.08" y1="53.34" x2="5.08" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OUT" gate="A" pin="3"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="60.96" y1="53.34" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="A" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="20.32" y1="66.04" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<wire x1="17.78" y1="66.04" x2="17.78" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ" class="0">
<segment>
<pinref part="IN" gate="A" pin="2"/>
<pinref part="OUT" gate="A" pin="2"/>
<wire x1="5.08" y1="55.88" x2="30.48" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="A" pin="DQ"/>
<wire x1="30.48" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
<wire x1="30.48" y1="58.42" x2="30.48" y2="55.88" width="0.1524" layer="91"/>
<junction x="30.48" y="55.88"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>

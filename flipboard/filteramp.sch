<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="LOGO" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<description>&lt;b&gt;Various connectors&lt;/b&gt;&lt;p&gt;
 &lt;author&gt;Created by bryan@kadzban.net&lt;/author&gt;</description>
<packages>
<package name="C0306">
<description>&lt;b&gt;Ceramic Chip Capacitor 0306&lt;/b&gt;&lt;p&gt;
Metric Code Size 0816</description>
<wire x1="-0.4064" y1="0.8001" x2="0.4064" y2="0.8001" width="0.1016" layer="51"/>
<wire x1="0.4064" y1="-0.8001" x2="-0.4064" y2="-0.8001" width="0.1016" layer="51"/>
<smd name="1" x="-0.675" y="0" dx="1.05" dy="1.6" layer="1"/>
<smd name="2" x="0.675" y="0" dx="1.05" dy="1.6" layer="1"/>
<text x="-1.4" y="1.05" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.4" y="-2.05" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4572" y1="-0.8001" x2="-0.2794" y2="0.8001" layer="51"/>
<rectangle x1="0.2794" y1="-0.8001" x2="0.4572" y2="0.8001" layer="51"/>
<wire x1="-0.4" y1="-0.8" x2="0.4" y2="-0.8" width="0.127" layer="39"/>
<wire x1="0.4" y1="-0.8" x2="0.4" y2="0.8" width="0.127" layer="39"/>
<wire x1="0.4" y1="0.8" x2="-0.4" y2="0.8" width="0.127" layer="39"/>
<wire x1="-0.4" y1="0.8" x2="-0.4" y2="-0.8" width="0.127" layer="39"/>
</package>
<package name="C0805">
<description>&lt;b&gt;Ceramic Chip Capacitor 0805&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.575" x2="0.925" y2="0.575" width="0.1" layer="51"/>
<wire x1="0.925" y1="-0.575" x2="-0.925" y2="-0.575" width="0.1016" layer="51"/>
<smd name="1" x="-1.15" y="0" dx="1.3" dy="1.25" layer="1"/>
<smd name="2" x="1.15" y="0" dx="1.3" dy="1.25" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.625" x2="-0.5" y2="0.625" layer="51"/>
<rectangle x1="0.5" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<wire x1="-0.95" y1="0.575" x2="0.95" y2="0.575" width="0.127" layer="39"/>
<wire x1="0.95" y1="0.575" x2="0.95" y2="-0.575" width="0.127" layer="39"/>
<wire x1="0.95" y1="-0.575" x2="-0.95" y2="-0.575" width="0.127" layer="39"/>
<wire x1="-0.95" y1="-0.575" x2="-0.95" y2="0.575" width="0.127" layer="39"/>
</package>
<package name="C-UCM">
<description>&lt;b&gt;UCM series electrolytic capacitor 5x5.8mm&lt;/b&gt;&lt;p&gt;
Source: 
http://nichicon-us.com/english/products/pdfs/e-ucm.pdf / http://products.nichicon.co.jp/en/pdf/XJA043/e-ch_ref.pdf</description>
<smd name="NEG" x="-2.2" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="POS" x="2.2" y="0" dx="3" dy="1.6" layer="1"/>
<text x="-2.5" y="2.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3.9" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-2.75" y1="2.75" x2="2.15" y2="2.75" width="0.127" layer="21"/>
<wire x1="2.15" y1="2.75" x2="2.75" y2="2.15" width="0.127" layer="21"/>
<wire x1="2.75" y1="2.15" x2="2.75" y2="1" width="0.127" layer="21"/>
<wire x1="2.75" y1="-1" x2="2.75" y2="-2.15" width="0.127" layer="21"/>
<wire x1="2.75" y1="-2.15" x2="2.15" y2="-2.75" width="0.127" layer="21"/>
<wire x1="2.15" y1="-2.75" x2="-2.75" y2="-2.75" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-2.75" x2="-2.75" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.75" y1="2.75" x2="-2.75" y2="1" width="0.127" layer="21"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-0.873" y1="0.483" x2="0.873" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.873" y1="0.483" x2="0.873" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.873" y1="-0.483" x2="-0.873" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.873" y1="-0.483" x2="-0.873" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="SHERLOCK-3-RA">
<description>Molex Sherlock family 3-pin board connector; right-angle</description>
<pad name="1" x="-2" y="0" drill="0.8" rot="R180"/>
<pad name="2" x="0" y="0" drill="0.8" rot="R180"/>
<pad name="3" x="2" y="0" drill="0.8" rot="R180"/>
<wire x1="4.15" y1="-2" x2="3.15" y2="-2" width="0.127" layer="21"/>
<wire x1="-3.15" y1="-2" x2="-4.15" y2="-2" width="0.127" layer="21"/>
<wire x1="4.15" y1="-2" x2="4.15" y2="8.5" width="0.127" layer="21"/>
<wire x1="4.15" y1="8.5" x2="2.9" y2="8.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="8" x2="2.4" y2="7.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="7.5" x2="-2.4" y2="7.5" width="0.127" layer="21"/>
<wire x1="-2.4" y1="7.5" x2="-2.4" y2="8" width="0.127" layer="21"/>
<wire x1="-2.9" y1="8.5" x2="-4.15" y2="8.5" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-2" x2="-4.15" y2="8.5" width="0.127" layer="21"/>
<wire x1="4.15" y1="-2" x2="-4.15" y2="-2" width="0.127" layer="39"/>
<wire x1="-4.85" y1="0" x2="-4.85" y2="6.8" width="0.127" layer="39"/>
<wire x1="-4.15" y1="8.5" x2="4.15" y2="8.5" width="0.127" layer="39"/>
<wire x1="4.85" y1="6.8" x2="4.85" y2="0" width="0.127" layer="39"/>
<wire x1="3.6" y1="1.5" x2="3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3" y1="1.5" x2="3.3" y2="1" width="0.127" layer="21"/>
<wire x1="3.3" y1="1" x2="3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.15" y1="-2" x2="-3.15" y2="0" width="0.127" layer="21"/>
<wire x1="-3.15" y1="0" x2="-2.8" y2="0" width="0.127" layer="21"/>
<wire x1="-2.8" y1="0" x2="-1.2" y2="0" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0" x2="-0.8" y2="0" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="0.8" y1="0" x2="1.2" y2="0" width="0.127" layer="21"/>
<wire x1="1.2" y1="0" x2="2.8" y2="0" width="0.127" layer="51"/>
<wire x1="2.8" y1="0" x2="3.15" y2="0" width="0.127" layer="21"/>
<wire x1="3.15" y1="0" x2="3.15" y2="-2" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-2" x2="-4.15" y2="0" width="0.127" layer="39"/>
<wire x1="-4.15" y1="0" x2="-4.85" y2="0" width="0.127" layer="39"/>
<wire x1="4.15" y1="-2" x2="4.15" y2="0" width="0.127" layer="39"/>
<wire x1="4.15" y1="0" x2="4.85" y2="0" width="0.127" layer="39"/>
<wire x1="-2.4" y1="8" x2="-2.9" y2="8.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="8" x2="2.9" y2="8.5" width="0.127" layer="21"/>
<wire x1="-4.85" y1="6.8" x2="-4.15" y2="6.8" width="0.127" layer="39"/>
<wire x1="-4.15" y1="6.8" x2="-4.15" y2="8.5" width="0.127" layer="39"/>
<wire x1="4.15" y1="6.8" x2="4.85" y2="6.8" width="0.127" layer="39"/>
<wire x1="4.15" y1="6.8" x2="4.15" y2="8.5" width="0.127" layer="39"/>
</package>
<package name="VSON8">
<description>&lt;b&gt;VSON&lt;/b&gt; 3 x 3 mm,  Pitch 0.65mm&lt;p&gt;
Source: http://www.ti.com/lit/ds/symlink/tpa2005d1.pdf</description>
<wire x1="1.575" y1="1.2625" x2="1.575" y2="1.575" width="0.127" layer="21"/>
<wire x1="1.575" y1="1.575" x2="0.55" y2="1.575" width="0.127" layer="21"/>
<wire x1="0.55" y1="1.575" x2="0.1" y2="1.575" width="0.127" layer="51"/>
<wire x1="0.1" y1="1.575" x2="-0.1" y2="1.575" width="0.127" layer="21"/>
<wire x1="-0.1" y1="1.575" x2="-0.55" y2="1.575" width="0.127" layer="51"/>
<wire x1="-0.55" y1="1.575" x2="-1.575" y2="1.575" width="0.127" layer="21"/>
<wire x1="-1.575" y1="1.575" x2="-1.575" y2="1.2625" width="0.127" layer="21"/>
<wire x1="-1.575" y1="1.2625" x2="-1.575" y2="-1.2625" width="0.127" layer="51"/>
<wire x1="-1.575" y1="-1.2625" x2="-1.575" y2="-1.575" width="0.127" layer="21"/>
<wire x1="-1.575" y1="-1.575" x2="-0.55" y2="-1.575" width="0.127" layer="21"/>
<wire x1="-0.55" y1="-1.575" x2="-0.1" y2="-1.575" width="0.127" layer="51"/>
<wire x1="-0.1" y1="-1.575" x2="0.1" y2="-1.575" width="0.127" layer="21"/>
<wire x1="0.1" y1="-1.575" x2="0.55" y2="-1.575" width="0.127" layer="51"/>
<wire x1="0.55" y1="-1.575" x2="1.575" y2="-1.575" width="0.127" layer="21"/>
<wire x1="1.575" y1="-1.575" x2="1.575" y2="-1.2625" width="0.127" layer="21"/>
<wire x1="1.575" y1="-1.2625" x2="1.575" y2="1.2625" width="0.127" layer="51"/>
<smd name="1" x="-1.65" y="0.975" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="2" x="-1.65" y="0.325" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="3" x="-1.65" y="-0.325" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="4" x="-1.65" y="-0.975" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="8" x="1.65" y="0.975" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="7" x="1.65" y="0.325" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="6" x="1.65" y="-0.325" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="5" x="1.65" y="-0.975" dx="1.45" dy="0.37" layer="1" stop="no"/>
<smd name="EXP" x="0" y="0" dx="1.545" dy="1.85" layer="1" stop="no"/>
<text x="-2.2" y="2.1" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2" y="-3.4" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.7725" y1="-0.925" x2="0.7725" y2="0.925" layer="29"/>
<rectangle x1="-2.4" y1="0.755" x2="-0.9" y2="1.195" layer="29"/>
<rectangle x1="-2.4" y1="0.105" x2="-0.9" y2="0.545" layer="29"/>
<rectangle x1="-2.4" y1="-0.545" x2="-0.9" y2="-0.105" layer="29"/>
<rectangle x1="-2.4" y1="-1.195" x2="-0.9" y2="-0.755" layer="29"/>
<rectangle x1="-1.425" y1="1.2" x2="-1.2" y2="1.425" layer="21"/>
<rectangle x1="0.9" y1="0.755" x2="2.4" y2="1.195" layer="29"/>
<rectangle x1="0.9" y1="0.105" x2="2.4" y2="0.545" layer="29"/>
<rectangle x1="0.9" y1="-0.545" x2="2.4" y2="-0.105" layer="29"/>
<rectangle x1="0.9" y1="-1.195" x2="2.4" y2="-0.755" layer="29"/>
<rectangle x1="-0.465" y1="0.925" x2="-0.185" y2="1.95" layer="29"/>
<rectangle x1="0.185" y1="0.925" x2="0.465" y2="1.95" layer="29"/>
<rectangle x1="-0.465" y1="-1.95" x2="-0.185" y2="-0.925" layer="29"/>
<rectangle x1="0.185" y1="-1.95" x2="0.465" y2="-0.925" layer="29"/>
<rectangle x1="-0.465" y1="0.925" x2="-0.185" y2="1.95" layer="31"/>
<rectangle x1="0.185" y1="0.925" x2="0.465" y2="1.95" layer="31"/>
<rectangle x1="-0.465" y1="-1.95" x2="-0.185" y2="-0.925" layer="31"/>
<rectangle x1="0.185" y1="-1.95" x2="0.465" y2="-0.925" layer="31"/>
<wire x1="-1.575" y1="0.6875" x2="-1.575" y2="0.6125" width="0.127" layer="21"/>
<wire x1="-1.575" y1="0.0375" x2="-1.575" y2="-0.0375" width="0.127" layer="21"/>
<wire x1="-1.575" y1="-0.6125" x2="-1.575" y2="-0.6875" width="0.127" layer="21"/>
<wire x1="1.575" y1="0.6875" x2="1.575" y2="0.6125" width="0.127" layer="21"/>
<wire x1="1.575" y1="0.0375" x2="1.575" y2="-0.0375" width="0.127" layer="21"/>
<wire x1="1.575" y1="-0.6125" x2="1.575" y2="-0.6875" width="0.127" layer="21"/>
<polygon width="0.1524" layer="1">
<vertex x="-0.6963" y="0.8488"/>
<vertex x="-0.6963" y="-0.8488"/>
<vertex x="-0.3888" y="-0.8488"/>
<vertex x="-0.3888" y="-1.8738"/>
<vertex x="-0.2612" y="-1.8738"/>
<vertex x="-0.2612" y="-0.8488"/>
<vertex x="0.2612" y="-0.8488"/>
<vertex x="0.2612" y="-1.8738"/>
<vertex x="0.3888" y="-1.8738"/>
<vertex x="0.3888" y="-0.8488"/>
<vertex x="0.6963" y="-0.8488"/>
<vertex x="0.6963" y="0.8488"/>
<vertex x="0.3888" y="0.8488"/>
<vertex x="0.3888" y="1.8738"/>
<vertex x="0.2612" y="1.8738"/>
<vertex x="0.2612" y="0.8488"/>
<vertex x="-0.2612" y="0.8488"/>
<vertex x="-0.2612" y="1.8738"/>
<vertex x="-0.3888" y="1.8738"/>
<vertex x="-0.3888" y="0.8488"/>
</polygon>
</package>
<package name="1X02">
<description>&lt;b&gt;2 WIRE HEADER&lt;/b&gt;, 70mil, 0 blank spaces between pins</description>
<pad name="1" x="-0.889" y="0" drill="0.7" rot="R90"/>
<pad name="2" x="0.889" y="0" drill="0.7" rot="R90"/>
<text x="-2.6162" y="0.8128" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.663" y="-0.635" size="1.27" layer="21">+</text>
<text x="1.674" y="-0.635" size="1.27" layer="21">-</text>
</package>
<package name="1X02-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;, 100mil, 0 blank spaces between pins</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02-3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;, 100 mil, 1 blank space between pins</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.127" layer="21"/>
</package>
<package name="1X02-4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;, 100 mil, 2 blank spaces between pins</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="2" x="3.81" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<wire x1="-2.54" y1="0.635" x2="-3.175" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.127" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.127" layer="21"/>
</package>
<package name="C0603">
<description>&lt;b&gt;Ceramic Chip Capacitor 0603&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.975" y="0" dx="1.25" dy="1.08" layer="1"/>
<smd name="2" x="0.975" y="0" dx="1.25" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="39"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="39"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="39"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="39"/>
</package>
<package name="5MMX3">
<description>2.5mm-pitch 3-pin 1mm-drill through-hole</description>
<pad name="2" x="0" y="0" drill="1" shape="octagon"/>
<pad name="3" x="2.5" y="0" drill="1" shape="octagon"/>
<pad name="1" x="-2.5" y="0" drill="1" shape="octagon"/>
<wire x1="-3.5" y1="1" x2="3.5" y2="1" width="0.127" layer="21"/>
<wire x1="3.5" y1="1" x2="3.5" y2="-1" width="0.127" layer="21"/>
<wire x1="3.5" y1="-1" x2="-3.5" y2="-1" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-1" x2="-3.5" y2="1" width="0.127" layer="21"/>
<circle x="-1.5" y="0.7" radius="0.1" width="0.2" layer="21"/>
</package>
<package name="USOP-8">
<description>8-pin uSOP package; 0.65mm pitch&lt;br&gt;
Source: http://datasheets.maximintegrated.com/en/ds/MAX7418-MAX7425.pdf</description>
<smd name="1" x="-2.5" y="0.975" dx="1.865" dy="0.36" layer="1"/>
<smd name="2" x="-2.5" y="0.325" dx="1.865" dy="0.36" layer="1"/>
<smd name="3" x="-2.5" y="-0.325" dx="1.865" dy="0.36" layer="1"/>
<smd name="4" x="-2.5" y="-0.975" dx="1.865" dy="0.36" layer="1"/>
<smd name="5" x="2.5" y="-0.975" dx="1.865" dy="0.36" layer="1"/>
<smd name="6" x="2.5" y="-0.325" dx="1.865" dy="0.36" layer="1"/>
<smd name="7" x="2.5" y="0.325" dx="1.865" dy="0.36" layer="1"/>
<smd name="8" x="2.5" y="0.975" dx="1.865" dy="0.36" layer="1"/>
<wire x1="-1.55" y1="-1.5" x2="1.55" y2="-1.5" width="0.1524" layer="21"/>
<wire x1="1.55" y1="-1.5" x2="1.55" y2="-1.33" width="0.1524" layer="21"/>
<wire x1="1.55" y1="-1.33" x2="1.55" y2="1.33" width="0.1524" layer="51"/>
<wire x1="1.55" y1="1.33" x2="1.55" y2="1.5" width="0.1524" layer="21"/>
<wire x1="1.55" y1="1.5" x2="-1.55" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-1.55" y1="1.5" x2="-1.55" y2="1.33" width="0.1524" layer="21"/>
<text x="-0.9514" y="1.615" size="0.4" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-1.188" y="-2.0994" size="0.4" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<circle x="-0.95" y="0.9" radius="0.22360625" width="0.127" layer="21"/>
<wire x1="-1.55" y1="1.33" x2="-1.55" y2="-1.33" width="0.1524" layer="51"/>
<wire x1="-1.55" y1="-1.33" x2="-1.55" y2="-1.5" width="0.1524" layer="21"/>
</package>
<package name="SOT353">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;br&gt;
0.65mm pitch</description>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.1524" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.1524" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.1524" layer="21"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.1524" layer="51"/>
<wire x1="-0.2224" y1="0.675" x2="0.2224" y2="0.675" width="0.1524" layer="21"/>
<smd name="1" x="-0.65" y="-1.4" dx="0.4" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.4" dx="0.4" dy="1.4" layer="1"/>
<smd name="3" x="0.65" y="-1.4" dx="0.4" dy="1.4" layer="1"/>
<smd name="4" x="0.65" y="1.4" dx="0.4" dy="1.4" layer="1"/>
<smd name="5" x="-0.65" y="1.4" dx="0.4" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.1" y1="0.675" x2="-1.1" y2="-0.675" width="0.127" layer="39"/>
<wire x1="-1.1" y1="-0.675" x2="1.1" y2="-0.675" width="0.127" layer="39"/>
<wire x1="1.1" y1="-0.675" x2="1.1" y2="0.675" width="0.127" layer="39"/>
<wire x1="1.1" y1="0.675" x2="-1.1" y2="0.675" width="0.127" layer="39"/>
<wire x1="0.8" y1="0.75" x2="0.8" y2="1.4" width="0.127" layer="39"/>
<wire x1="0.8" y1="1.4" x2="0.5" y2="1.4" width="0.127" layer="39"/>
<wire x1="0.5" y1="1.4" x2="0.5" y2="0.75" width="0.127" layer="39"/>
<wire x1="0.5" y1="0.75" x2="0.8" y2="0.75" width="0.127" layer="39"/>
<wire x1="-0.5" y1="0.75" x2="-0.5" y2="1.4" width="0.127" layer="39"/>
<wire x1="-0.8" y1="0.75" x2="-0.8" y2="1.4" width="0.127" layer="39"/>
<wire x1="-0.5" y1="1.4" x2="-0.8" y2="1.4" width="0.127" layer="39"/>
<wire x1="-0.8" y1="0.75" x2="-0.5" y2="0.75" width="0.127" layer="39"/>
<wire x1="-0.8" y1="-0.75" x2="-0.5" y2="-0.75" width="0.127" layer="39"/>
<wire x1="-0.5" y1="-1.4" x2="-0.8" y2="-1.4" width="0.127" layer="39"/>
<wire x1="-0.5" y1="-1.4" x2="-0.5" y2="-0.75" width="0.127" layer="39"/>
<wire x1="-0.8" y1="-1.4" x2="-0.8" y2="-0.75" width="0.127" layer="39"/>
<wire x1="-0.15" y1="-1.4" x2="-0.15" y2="-0.75" width="0.127" layer="39"/>
<wire x1="0.15" y1="-1.4" x2="0.15" y2="-0.75" width="0.127" layer="39"/>
<wire x1="0.15" y1="-1.4" x2="-0.15" y2="-1.4" width="0.127" layer="39"/>
<wire x1="0.15" y1="-0.75" x2="-0.15" y2="-0.75" width="0.127" layer="39"/>
<wire x1="0.5" y1="-1.4" x2="0.5" y2="-0.75" width="0.127" layer="39"/>
<wire x1="0.8" y1="-1.4" x2="0.8" y2="-0.75" width="0.127" layer="39"/>
<wire x1="0.8" y1="-1.4" x2="0.5" y2="-1.4" width="0.127" layer="39"/>
<wire x1="0.8" y1="-0.75" x2="0.5" y2="-0.75" width="0.127" layer="39"/>
</package>
<package name="1X03-4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;, 100 mil, 1 blank space between pins</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<wire x1="-2.54" y1="0.635" x2="-3.175" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.127" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.127" layer="21"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<pad name="3" x="3.81" y="0" drill="1" shape="octagon" rot="R90"/>
</package>
<package name="1X03-22">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<text x="-2.54" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
</package>
<package name="C-UCM-6MM">
<description>&lt;b&gt;UCM series electrolytic capacitor 6.3x7.7mm&lt;/b&gt;&lt;p&gt;
Source: 
http://nichicon-us.com/english/products/pdfs/e-ucm.pdf / http://products.nichicon.co.jp/en/pdf/XJA043/e-ch_ref.pdf</description>
<smd name="NEG" x="-2.7" y="0" dx="3.8" dy="1.6" layer="1"/>
<smd name="POS" x="2.7" y="0" dx="3.8" dy="1.6" layer="1"/>
<text x="-2.5" y="3.575" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.5" y="-4.6" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-3.4" y1="3.4" x2="2.35" y2="3.4" width="0.127" layer="21"/>
<wire x1="2.35" y1="3.4" x2="3.4" y2="2.35" width="0.127" layer="21"/>
<wire x1="3.4" y1="2.35" x2="3.4" y2="1" width="0.127" layer="21"/>
<wire x1="3.4" y1="-1" x2="3.4" y2="-2.35" width="0.127" layer="21"/>
<wire x1="3.4" y1="-2.35" x2="2.35" y2="-3.4" width="0.127" layer="21"/>
<wire x1="2.35" y1="-3.4" x2="-3.4" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="C-UCM-8MM">
<description>&lt;b&gt;UCM series electrolytic capacitor 8x6.2mm&lt;/b&gt;&lt;p&gt;
Source: 
http://nichicon-us.com/english/products/pdfs/e-ucm.pdf / http://products.nichicon.co.jp/en/pdf/XJA043/e-ch_ref.pdf</description>
<smd name="NEG" x="-2.2" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="POS" x="2.2" y="0" dx="3" dy="1.6" layer="1"/>
<text x="-2.5" y="2.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3.9" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-4.25" y1="4.25" x2="2.15" y2="4.25" width="0.127" layer="21"/>
<wire x1="2.15" y1="4.25" x2="4.25" y2="2.15" width="0.127" layer="21"/>
<wire x1="4.25" y1="2.15" x2="4.25" y2="1" width="0.127" layer="21"/>
<wire x1="4.25" y1="-1" x2="4.25" y2="-2.15" width="0.127" layer="21"/>
<wire x1="4.25" y1="-2.15" x2="2.15" y2="-4.25" width="0.127" layer="21"/>
<wire x1="2.15" y1="-4.25" x2="-4.25" y2="-4.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-4.25" x2="-4.25" y2="-1" width="0.127" layer="21"/>
<wire x1="-4.25" y1="4.25" x2="-4.25" y2="1" width="0.127" layer="21"/>
</package>
<package name="1X03-SM">
<description>&lt;b&gt;3 WIRE HEADER&lt;/b&gt;, 70mil, 0 blank spaces between pins</description>
<pad name="1" x="-1.778" y="0" drill="0.8" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.8" rot="R90"/>
<text x="-2.6162" y="0.8128" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<pad name="3" x="1.778" y="0" drill="0.8" rot="R90"/>
<text x="-4" y="-0.3" size="0.5" layer="21" rot="SR0">+5V</text>
<text x="2.8" y="-0.3" size="0.5" layer="21" rot="SR0">GND</text>
</package>
<package name="C0508">
<description>&lt;b&gt;Ceramic Chip Capacitor 0306&lt;/b&gt;&lt;p&gt;
Metric Code Size 0816</description>
<wire x1="-0.5742" y1="1" x2="0.5742" y2="1" width="0.1016" layer="51"/>
<wire x1="0.5742" y1="-1" x2="-0.5742" y2="-1" width="0.1016" layer="51"/>
<smd name="1" x="-0.8875" y="0" dx="1.075" dy="1.8" layer="1"/>
<smd name="2" x="0.8875" y="0" dx="1.075" dy="1.8" layer="1"/>
<text x="-1.4" y="1.05" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.4" y="-2.05" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.625" y1="-1" x2="-0.4472" y2="1" layer="51"/>
<rectangle x1="0.4472" y1="-1" x2="0.625" y2="1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="1X03">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="TPA2005D1">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="!SDN" x="-10.16" y="5.08" visible="pin" length="short" direction="in"/>
<pin name="IN+" x="-10.16" y="-2.54" visible="pin" length="short" direction="in"/>
<pin name="IN-" x="-10.16" y="-5.08" visible="pin" length="short" direction="in"/>
<pin name="VDD" x="10.16" y="-2.54" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="VSS@1" x="10.16" y="2.54" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="VO-" x="10.16" y="5.08" visible="pin" length="short" direction="out" rot="R180"/>
<pin name="VO+" x="10.16" y="-5.08" visible="pin" length="short" direction="out" rot="R180"/>
<pin name="NC" x="-10.16" y="2.54" visible="pin" length="short" direction="nc"/>
<pin name="VSS@2" x="0" y="-10.16" visible="pin" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="1X02">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="POT">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="2.667" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.778" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="3" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="2.54"/>
<vertex x="2.667" y="3.81"/>
<vertex x="1.778" y="2.921"/>
</polygon>
</symbol>
<symbol name="MAX7420">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<pin name="!SDN" x="-10.16" y="7.62" visible="pin" length="short" direction="in"/>
<pin name="OS" x="-10.16" y="0" visible="pin" length="short" direction="in"/>
<pin name="IN" x="-10.16" y="-5.08" visible="pin" length="short" direction="in"/>
<pin name="VDD" x="10.16" y="7.62" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="VSS" x="10.16" y="-7.62" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="OUT" x="10.16" y="0" visible="pin" length="short" direction="out" rot="R180"/>
<pin name="CLK" x="-10.16" y="-7.62" visible="pin" length="short" direction="in"/>
<pin name="COM" x="-10.16" y="2.54" visible="pin" length="short" direction="in"/>
</symbol>
<symbol name="FAN4931">
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<pin name="IN+" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="IN-" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="V-" x="2.54" y="-7.62" length="short" direction="pwr" rot="R90"/>
<pin name="V+" x="2.54" y="7.62" length="short" direction="pwr" rot="R270"/>
<pin name="OUT" x="15.24" y="0" length="short" direction="out" rot="R180"/>
<text x="-10.16" y="12.7" size="2.54" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-US" prefix="C" uservalue="yes">
<description>Capacitor, US symbol</description>
<gates>
<gate name="C" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="0306" package="C0306">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="C0508">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-UCM" package="C-UCM">
<connects>
<connect gate="C" pin="1" pad="POS"/>
<connect gate="C" pin="2" pad="NEG"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-UCM-6MM" package="C-UCM-6MM">
<connects>
<connect gate="C" pin="1" pad="POS"/>
<connect gate="C" pin="2" pad="NEG"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-UCM-8MM" package="C-UCM-8MM">
<connects>
<connect gate="C" pin="1" pad="POS"/>
<connect gate="C" pin="2" pad="NEG"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-US" prefix="R" uservalue="yes">
<description>RESISTOR, American symbol</description>
<gates>
<gate name="A" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="-0603" package="R0603">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1X03" prefix="JP">
<description>&lt;b&gt;3 wire-connection pads&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="1X03" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03-4">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SQ" package="1X03-22">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SHERLOCK" package="SHERLOCK-3-RA">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SM" package="1X03-SM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPA2005D1" prefix="U">
<description>1.4W (@5V) monoaural filter-free class-D audio power amplifier</description>
<gates>
<gate name="A" symbol="TPA2005D1" x="0" y="0"/>
</gates>
<devices>
<device name="-DRB" package="VSON8">
<connects>
<connect gate="A" pin="!SDN" pad="1"/>
<connect gate="A" pin="IN+" pad="3"/>
<connect gate="A" pin="IN-" pad="4"/>
<connect gate="A" pin="NC" pad="2"/>
<connect gate="A" pin="VDD" pad="6"/>
<connect gate="A" pin="VO+" pad="5"/>
<connect gate="A" pin="VO-" pad="8"/>
<connect gate="A" pin="VSS@1" pad="7"/>
<connect gate="A" pin="VSS@2" pad="EXP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1X02" prefix="JP">
<description>&lt;b&gt;2 wire-connection pads&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="1X02" x="0" y="0"/>
</gates>
<devices>
<device name="-SM" package="1X02">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LG" package="1X02-2">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LG3" package="1X02-3">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LG4" package="1X02-4">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PTV09" prefix="R">
<description>Potentiometer</description>
<gates>
<gate name="A" symbol="POT" x="0" y="0"/>
</gates>
<devices>
<device name="A-1" package="5MMX3">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAX7420" prefix="U">
<description>MAX7420 5th-order low-pass switched-capacitor Butterworth filter</description>
<gates>
<gate name="A" symbol="MAX7420" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USOP-8">
<connects>
<connect gate="A" pin="!SDN" pad="7"/>
<connect gate="A" pin="CLK" pad="8"/>
<connect gate="A" pin="COM" pad="1"/>
<connect gate="A" pin="IN" pad="2"/>
<connect gate="A" pin="OS" pad="6"/>
<connect gate="A" pin="OUT" pad="5"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FAN4931" prefix="U">
<description>Rail-to-rail I/O opamp</description>
<gates>
<gate name="A" symbol="FAN4931" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT353">
<connects>
<connect gate="A" pin="IN+" pad="1"/>
<connect gate="A" pin="IN-" pad="3"/>
<connect gate="A" pin="OUT" pad="4"/>
<connect gate="A" pin="V+" pad="5"/>
<connect gate="A" pin="V-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.1524" drill="0.3302">
<clearance class="0" value="0.1524"/>
</class>
</classes>
<parts>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="DAC" library="connector" deviceset="1X03" device="-SM" value="1X03-SM"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="U3" library="connector" deviceset="TPA2005D1" device="-DRB"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="connector" deviceset="R-US" device="-0603" value="150k"/>
<part name="R3" library="connector" deviceset="R-US" device="-0603" value="150k"/>
<part name="C7" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="SPKR" library="connector" deviceset="1X02" device="-SM" value="1X02-SM"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="C3" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="C4" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="connector" deviceset="C-US" device="0603" value="43pF"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="POT1" library="connector" deviceset="PTV09" device="A-1"/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="P+5" library="supply1" deviceset="+5V" device=""/>
<part name="U2" library="connector" deviceset="MAX7420" device=""/>
<part name="U1" library="connector" deviceset="FAN4931" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="P+6" library="supply1" deviceset="+5V" device=""/>
<part name="R1" library="connector" deviceset="R-US" device="-0603" value="17k"/>
<part name="R2" library="connector" deviceset="R-US" device="-0603" value="33k"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="C1" library="connector" deviceset="C-US" device="0306" value="0.1uF"/>
<part name="P+7" library="supply1" deviceset="+5V" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="GND12" gate="1" x="66.04" y="12.7"/>
<instance part="DAC" gate="A" x="-15.24" y="33.02"/>
<instance part="GND1" gate="1" x="-17.78" y="22.86"/>
<instance part="U3" gate="A" x="99.06" y="27.94"/>
<instance part="GND2" gate="1" x="99.06" y="12.7"/>
<instance part="R4" gate="A" x="81.28" y="25.4"/>
<instance part="R3" gate="A" x="81.28" y="17.78"/>
<instance part="C7" gate="C" x="119.38" y="22.86"/>
<instance part="SPKR" gate="A" x="129.54" y="25.4" rot="R180"/>
<instance part="GND4" gate="1" x="111.76" y="27.94"/>
<instance part="C3" gate="C" x="53.34" y="48.26"/>
<instance part="GND5" gate="1" x="53.34" y="40.64"/>
<instance part="C4" gate="C" x="40.64" y="0"/>
<instance part="GND6" gate="1" x="40.64" y="-7.62"/>
<instance part="C2" gate="C" x="35.56" y="12.7"/>
<instance part="GND7" gate="1" x="35.56" y="5.08"/>
<instance part="POT1" gate="A" x="30.48" y="27.94" rot="R90"/>
<instance part="P+1" gate="1" x="88.9" y="38.1"/>
<instance part="P+2" gate="1" x="53.34" y="53.34"/>
<instance part="P+3" gate="1" x="-17.78" y="45.72"/>
<instance part="P+4" gate="1" x="66.04" y="43.18"/>
<instance part="P+5" gate="1" x="119.38" y="30.48"/>
<instance part="U2" gate="A" x="53.34" y="25.4"/>
<instance part="U1" gate="A" x="10.16" y="33.02"/>
<instance part="GND9" gate="1" x="12.7" y="20.32"/>
<instance part="P+6" gate="1" x="12.7" y="45.72"/>
<instance part="R1" gate="A" x="12.7" y="12.7" rot="R180"/>
<instance part="R2" gate="A" x="0" y="7.62" rot="R270"/>
<instance part="GND10" gate="1" x="0" y="0"/>
<instance part="C1" gate="C" x="20.32" y="48.26"/>
<instance part="P+7" gate="1" x="20.32" y="53.34"/>
<instance part="GND8" gate="1" x="20.32" y="40.64"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U3" gate="A" pin="VSS@2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="99.06" y1="17.78" x2="99.06" y2="15.24" width="0.1524" layer="91"/>
<wire x1="119.38" y1="15.24" x2="99.06" y2="15.24" width="0.1524" layer="91"/>
<junction x="99.06" y="15.24"/>
<pinref part="C7" gate="C" pin="2"/>
<wire x1="119.38" y1="17.78" x2="119.38" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="VSS@1"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="109.22" y1="30.48" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="66.04" y1="15.24" x2="66.04" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VSS"/>
<wire x1="66.04" y1="17.78" x2="63.5" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="C" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="C" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="C" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="V-"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="12.7" y1="22.86" x2="12.7" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="A" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="C" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="DAC" gate="A" pin="3"/>
<wire x1="-17.78" y1="25.4" x2="-17.78" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
</net>
<net name="FILTERED" class="0">
<segment>
<pinref part="U2" gate="A" pin="OUT"/>
<pinref part="R4" gate="A" pin="1"/>
<wire x1="63.5" y1="25.4" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ATTFILTERED" class="0">
<segment>
<pinref part="U3" gate="A" pin="IN+"/>
<pinref part="R4" gate="A" pin="2"/>
<wire x1="86.36" y1="25.4" x2="88.9" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ATT2.5V" class="0">
<segment>
<pinref part="U3" gate="A" pin="IN-"/>
<pinref part="R3" gate="A" pin="2"/>
<wire x1="86.36" y1="17.78" x2="88.9" y2="17.78" width="0.1524" layer="91"/>
<wire x1="88.9" y1="17.78" x2="88.9" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VO-" class="0">
<segment>
<pinref part="U3" gate="A" pin="VO-"/>
<wire x1="109.22" y1="33.02" x2="132.08" y2="33.02" width="0.1524" layer="91"/>
<pinref part="SPKR" gate="A" pin="2"/>
<wire x1="132.08" y1="33.02" x2="132.08" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VO+" class="0">
<segment>
<pinref part="U3" gate="A" pin="VO+"/>
<wire x1="109.22" y1="22.86" x2="111.76" y2="22.86" width="0.1524" layer="91"/>
<wire x1="111.76" y1="22.86" x2="111.76" y2="12.7" width="0.1524" layer="91"/>
<wire x1="111.76" y1="12.7" x2="132.08" y2="12.7" width="0.1524" layer="91"/>
<pinref part="SPKR" gate="A" pin="1"/>
<wire x1="132.08" y1="12.7" x2="132.08" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="C2" gate="C" pin="1"/>
<wire x1="35.56" y1="17.78" x2="35.56" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CLK"/>
<wire x1="35.56" y1="17.78" x2="43.18" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DOWN" class="0">
<segment>
<pinref part="POT1" gate="A" pin="2"/>
<wire x1="43.18" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<wire x1="35.56" y1="20.32" x2="35.56" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="IN"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U3" gate="A" pin="!SDN"/>
<wire x1="88.9" y1="35.56" x2="88.9" y2="33.02" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="C3" gate="C" pin="1"/>
<pinref part="P+2" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="DAC" gate="A" pin="1"/>
<wire x1="-17.78" y1="35.56" x2="-17.78" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VDD"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="66.04" y1="33.02" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="38.1" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="33.02" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="!SDN"/>
<wire x1="66.04" y1="38.1" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
<wire x1="43.18" y1="38.1" x2="43.18" y2="33.02" width="0.1524" layer="91"/>
<junction x="66.04" y="38.1"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="VDD"/>
<wire x1="109.22" y1="25.4" x2="119.38" y2="25.4" width="0.1524" layer="91"/>
<wire x1="119.38" y1="25.4" x2="119.38" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C7" gate="C" pin="1"/>
<junction x="119.38" y="25.4"/>
<pinref part="P+5" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="V+"/>
<pinref part="P+6" gate="1" pin="+5V"/>
<wire x1="12.7" y1="43.18" x2="12.7" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="C" pin="1"/>
<pinref part="P+7" gate="1" pin="+5V"/>
</segment>
</net>
<net name="AUDIO" class="0">
<segment>
<pinref part="U1" gate="A" pin="IN+"/>
<wire x1="-5.08" y1="33.02" x2="-5.08" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="38.1" x2="0" y2="38.1" width="0.1524" layer="91"/>
<pinref part="DAC" gate="A" pin="2"/>
<wire x1="-5.08" y1="33.02" x2="-17.78" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AMPLIFIED" class="0">
<segment>
<pinref part="POT1" gate="A" pin="3"/>
<pinref part="U1" gate="A" pin="OUT"/>
<wire x1="30.48" y1="33.02" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="25.4" y1="33.02" x2="25.4" y2="12.7" width="0.1524" layer="91"/>
<junction x="25.4" y="33.02"/>
<wire x1="25.4" y1="12.7" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R1" gate="A" pin="1"/>
</segment>
</net>
<net name="FB" class="0">
<segment>
<pinref part="U1" gate="A" pin="IN-"/>
<wire x1="0" y1="27.94" x2="0" y2="12.7" width="0.1524" layer="91"/>
<wire x1="0" y1="12.7" x2="7.62" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R2" gate="A" pin="1"/>
<junction x="0" y="12.7"/>
<pinref part="R1" gate="A" pin="2"/>
</segment>
</net>
<net name="+2.5V" class="0">
<segment>
<pinref part="R3" gate="A" pin="1"/>
<wire x1="73.66" y1="17.78" x2="76.2" y2="17.78" width="0.1524" layer="91"/>
<wire x1="73.66" y1="17.78" x2="73.66" y2="5.08" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="COM"/>
<pinref part="C4" gate="C" pin="1"/>
<wire x1="43.18" y1="27.94" x2="40.64" y2="27.94" width="0.1524" layer="91"/>
<wire x1="40.64" y1="27.94" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="OS"/>
<wire x1="40.64" y1="25.4" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<wire x1="40.64" y1="22.86" x2="40.64" y2="5.08" width="0.1524" layer="91"/>
<wire x1="40.64" y1="5.08" x2="40.64" y2="2.54" width="0.1524" layer="91"/>
<wire x1="43.18" y1="25.4" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<junction x="40.64" y="25.4"/>
<pinref part="POT1" gate="A" pin="1"/>
<wire x1="30.48" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<junction x="40.64" y="22.86"/>
<wire x1="73.66" y1="5.08" x2="40.64" y2="5.08" width="0.1524" layer="91"/>
<junction x="40.64" y="5.08"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,109.22,25.4,U3,VDD,+5V,,,"/>
<approved hash="104,1,109.22,30.48,U3,VSS,GND,,,"/>
<approved hash="104,1,99.06,17.78,U3,VSS,GND,,,"/>
<approved hash="104,1,63.5,33.02,U2,VDD,+5V,,,"/>
<approved hash="104,1,63.5,17.78,U2,VSS,GND,,,"/>
<approved hash="104,1,12.7,25.4,U1,V-,GND,,,"/>
<approved hash="104,1,12.7,40.64,U1,V+,+5V,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
